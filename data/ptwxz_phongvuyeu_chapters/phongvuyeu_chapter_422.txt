








    当龙界内部陷入了水生火热的困境的时候，在一墙之隔的拜尔王都，这座历经了无数岁月的古都却陷入了沉寂之中，或者，应该说一种人为的肃清。◇↓

    和往昔的沉默不同，在东线败战之后，王都的皇家骑士大肆行动，对整个城市进行了戒严和宵禁，那些趁机生事的皇子一个个被拖到的街头斩首示众，似乎已经转职老好人的“毒蝎帝王”奥罗斯十三世再度让世人见识他的狠毒。

    “海伦特会成为新一代的帝王。”

    当东线战败，风雨摇摆之中的时候，老皇帝却顶着所有压力，向国人和大领主们重申了自己的意见。

    当面反对？杀。

    背后议论？杀。

    皇子勾结封地领主？杀。

    仅仅只是在自己的府邸向仆人和亲友抱怨？一样，杀。

    平日总是挂着微笑的老皇帝，那个在自己被刺杀的时候都能笑的出来的和善老者，此时只是在自己的书房品着茶，笑着说出一个个“杀”，而同样疯狂的皇家骑士也沉默着执行一个个杀戮任务，即使那是把不满十岁的孩子绞死在城墙之上，即使那会把骑士荣誉和骑士精神都放在脚下踩的粉碎。

    “对海伦特继位有任何质疑者，不管身份、不问年龄、不论功绩，杀无赦。”

    这就是老皇帝的意志，皇家骑士们默默的执行了这一意志，即使拜尔人无法理解老皇帝突然而然的疯狂行径，无法直视那突然而来的杀戮。尤其是最近由于和皇子勾结被屠杀的一干二净的大领主质子。已经让边境的大贵族陷入了暴怒之中。

    眼看这样的下去。圣战联军、亡灵帝国不打过来，拜尔就会先行内乱，往昔那个睿智的老皇帝到底在做什么？他在毁灭自己一生岁月打造的帝国吗？

    连站在海伦特身边，那一贯中立的龙骑士都无法理解老皇帝的突然疯狂，但碍于骑士精神首要的忠诚，却无法阻止，但谁都知道，这并不正常。也不能长久。

    恐怕，这样下去就算海伦特成功继位，只要老皇帝一倒，那疯狂杀戮、压迫造成的反弹和报复，也会让其惨淡收场。

    而奇迹之手的陨落，在让世人为之惊叹、悲伤、无奈的同时，却帮了海伦特和拜尔一个大忙。

    越发成为正在恶化的毒瘤的圣战联军被拦住了，圣光之神无可抵御的意志被停下了，即使只有短短的一瞬，埃斯特拉达用自己的牺牲获得了凡人的胜利。拜尔也失去了近在眉睫的利剑威胁。

    “……这样老皇帝应该会冷静了吧。”

    而有些事情，却超出了所有人的预期。老皇帝不仅没有停下来，反而变本加厉。

    趁着危机哄抬物价、囤积居奇者？杀。

    和分封大贵族关系密切，有亲缘关系者？杀。

    什么都没做，但位高权重，有可能对未来的海伦特继位造成影响者？杀。

    慈眉善目的看着那些义子彼此厮杀了二十年的老皇帝，却依旧带着微笑说这一个个“杀”字，看着帝都血流成河。

    即使事件的另外一个当事人海伦特跑过去和他争吵之后，他依旧没有什么改变，倒是海伦特本人就此闭门不出。

    “……未来的拜尔要想存活下去，只能由海伦特继承皇位，至于他是怎么上来的，一点都不重要。至于我自己……呵呵，等着瞧吧。”

    整个国内，只要少数几个人，能够走近奥罗斯的书房，听着他缓缓述说自己的想法。

    而此时，老皇帝正在听着密探首领和骑士长的汇报。

    “……从我们探子收集到的情报来看，由于无法摸清您现在的目的，卡费公爵、伯兰伯爵都暂缓了整合私兵的行动，似乎是担心当了出头鸟，会被您当做首要目标，但有些事情和我们过去预测的没什么区别，只要您……您‘离开’了，他们肯定会造反，现在也只是暂缓而已。”

    “呵，那些老家伙。算了，国内的日常贸易情况如何。物价还在两倍左右吗。”

    “很不错，那些奸商死了一批后，现在上来的安稳了不少，已经从最高峰的二十五倍掉到了一倍三。再加上您的高压政策和海论特殿下的亲笔信许诺，他们大部分已经暗中向殿下效忠吗，只要拔掉了爪牙，本来就没什么地位的商人又能做出什么，至少短时间内没有问题。”

    “我那些不争气的其他小子？”

    “有可能制造威胁的已经全部处死，剩余的都宣布和拜尔王室解除关系，所有的皇子都在监控之中。但与此相对应的，就是您的名声……民间已经有人称呼您为白龙。”

    “白龙？”

    “……食子的白龙。”

    奥罗斯点头却没有追问，甚至看都没有看情报，直接低头喝茶，对于已经抛弃一切的他，这样的污言对他来说又算得了什么，接下来他即将做的，将会更加过份，说不准，自己将成为拜尔历史上的头号暴君和蠢货，但为了“大计”着想，外人的无法理解和污蔑又算得了什么。

    老者只是缓缓的闭上眼，并没有再度询问什么，其他人都知机的退下，而当书房再度陷入平静，密室之中的三个人却走了出来，从某种意义上来说，这三位才是现在整个拜尔的核心层。

    南方教会的教皇埃莫耶一世，也曾经担任过拜尔的宫廷总管，是奥罗斯生死与共的兄弟，也是他最信任的对象，而他手中掌握的，却是整个南方教派和拜尔的宗教势力。

    “……辛苦了。”

    “呵呵，已经比预期的好了，教会那边还是麻烦你了。有奇迹之手的牺牲。

    此时的老教皇埃莫耶也是满脸疲惫。之前南方教派给予重望的出道战就遭受当头痛击，而若不是埃斯特拉达的牺牲，稳住了人心，恐怕南方教派会出大问题。

    另外一个披着紫色紫罗兰斗篷的老法师，称呼“愚者”，他也是拜尔魔法师工会的会长，但往日异常低调，很少有人看到他的真正面容。而此时他也露出了真面容，却是和没什么特殊的银发老者，但若是让圣堂教会的异端审判者看到了，恐怕所有的疯狗会倾巢出动。

    但若是罗兰在这里，恐怕就会和他很有交流余地了，是的，他就是色雷斯，疯法师色雷斯，那个“知道的太多还说出口”的愚者。

    “法师工会那边，还有和法师之国的沟通。恐怕都要麻烦您老多费心了，我们让您违背誓言复出。还……”

    “我知道的，不用多说了，有些事情是命中注定的逃不过去的，既然你们都豁出去赌一把，老夫又怎么可能退缩。只是……可惜了，当年的埃斯特拉达逼得的我发誓归隐的时候，也没想过会有这一天吧。”

    被当做疯子的老法师满头整齐油亮的银发，华丽的法袍更是法师中难得整齐笔挺，而此时，想到故友最终落到这样的下场，色雷斯却摇了摇头，满脸不忍。

    “可惜，可惜，太可惜了。”

    最终，他也只能做出这样的评价。

    “导师，节哀顺变，我们都是一路人，从某种意义上来说，他只是先走了一步而已，这应该值得高兴……”

    或许根本没有多少人知道，奥罗斯年轻的时候曾经在云中塔求学，甚至还在有心人的帮助下，成为了当时隐居的**师色雷斯的弟子。

    “……奥罗斯陛下。”

    而最后一个人走近了，却根本不是拜尔人，而是……

    “时间到了。”

    莉莉丝.米兰的手臂之上，那颗绿宝石原本璀璨夺目的光华已经开始消散，裂纹逐渐在其上扩大，很快，这枚价值万金的双子猫眼绿就会成为一钱不值的残渣。

    而这用宝石学加工的特殊魔法宝石一对双生，其中一颗毁灭而另外一颗也随着毁灭，这种因果上联系能够突破一切的封锁，是一种异常罕见而珍贵的传讯装置，成品至少需要组建半个骑士团的海量金币，而能够用上这样的一次性传讯装置，足以说明眼前事件的严峻性。

    而当眼前的绿宝石变成废渣，只有一个可能性，在龙界中的罗兰捏碎了另外一块宝石，整个“大计”将由眼前的奥罗斯启动。

    而此时的莉莉丝.米兰，却不仅仅是罗兰和律法教会的代言人，那熟悉的华丽宫廷女装和皇家徽记，无疑说明了一个事实——她已经回归拜尔皇室！

    “莉莉丝阿……姐姐，麻烦你了，请您在海伦特真正成熟之前，作为拜尔王室当前唯二的纯正皇室，请您暂任其摄政王的职务。”

    莉莉丝.米兰……不，应该说莉莉丝长公主兼摄政王点了点头，若是可以的话，她是一点都不想惹上这身麻烦，但眼前的人却用自己的理念和牺牲，说服了她，让她觉得，在这个时间点，若自己做不出点什么，恐怕也对不起自己已经逝去的亲人。

    环顾四周，老皇帝叹了口气，他知道，恐怕这是自己最后一天看到这所书房和友人。

    “……没想到，忍了一辈子，最后还是要忍。你们说，我不是一个合格的皇帝。”

    “当然，我的陛下。没有您就没有现在的拜尔，或许黑夜将会很漫长，或许将由无数的疯狗出来撕咬您的尸骸，但乌云是无法永远遮住阳光的，最终您睿智的名声将会重新在这片土地上传播。”

    老教皇，过去的老管家微微鞠躬，眯着的老眼中却带着泪花。

    “奥罗斯，何必为那些愚者的看法困扰，老夫我始终以你为傲。”

    并不擅长言辞的色雷斯顿了顿，用肯定的语气点了点头。

    “我知道你也不会在意这些虚名，但我的确觉得你很了不起，比我还了不起。”

    能够得到导师和老友的肯定，老皇帝欣慰的笑了，或许世人将无法理解自己的选择，但人生有一知己已经足够，自己有这两位亲人的理解，不就已经足够了。

    “……足够了，为了凡人的世界，就由我去毁掉所有的龙界之门吧。”

    龙界之门？是的，通往龙界的大门，若通往一个异位面的所有的大门、次元坐标全部失去了，那个异位面也将在主位面眼中失去踪迹，而毁掉所有的龙界之门，也意味着龙族将失去自己的三大聚集之地，也意味着整个主位面将失去和那个异位面的联系。

    这在普通的位面是根本做不到的，毕竟零零散散的联系不知道有多少，而偏偏龙族为了把持住这个位面，也保持住了所有的次元之门和传送点，而那三个传送点全部在拜尔国内！更是被龙骑士和龙族把守……是的，依文莉就是看守之一，也是最强的看守，而显然，龙族选错了人。

    而重新建立一个异位面的联系，需要大量的时间，至少需要两三年以上，偏偏泰坦复苏在即，龙族一旦失去了援军，等于那些居于龙界的龙族将全部面临死地，这绝对超出了整个龙族的预期，巨龙的怒火必然降临，而选择海伦特作为下任皇帝，或许理由远比预期的简单，只是因为半龙血脉的他有可能被龙族接受，保住这个国家。

    至于这个做出如此无法理解的罪行的人，自然会成为龙族的报复对象，甚至由于“失陷其中的罗兰和绝对绅士联盟佣兵团”，北方诸国也将和拜尔翻脸，而最危险的，却是龙族在被背叛的怒火下，若他们不甘于仅仅用奥罗斯泄恨，那么，整个拜尔都有可能迎来毁灭的末日。

    而冒着即将毁灭的危险，做下如此恶行，拜尔和奥罗斯却得不到任何好处，相反，原本作为盟友的龙族会变成死敌，原本的盟友北方诸国，至少在明面上会变成“死敌”，那么，他到底为了什么。仅仅为了让龙界沦为泰坦的狩猎场，让拜尔摆脱这个定时炸弹吗？这是何等的愚蠢和短视，奥罗斯将成为有史以来最愚蠢的皇帝。

    “愚者吗？呵，还真是相称，导师，或许我以后会成为‘愚王’。”

    “不，别人以为我是智者，我只是想提醒自己，我只是一个自以为聪明的蠢货。但你，却是看的太透，看的太远，才即将被愚夫视作愚蠢，这反而是你睿智的证明。”

    或许，对于有心人来说，让一个愚蠢的阴谋失败的败在阳光之下，让自己被吊死在绞刑架上，只是为了隐藏一个更加隐蔽的阴谋。

    “凡人的世界啊，可惜，我没有亲眼看到他的可能。”

    呢喃中的奥罗斯，不由得想起了前段时间和罗兰的深谈，当时，老皇帝是这么开口的。

    “罗兰，实际上，我一直很喜欢你在南方教派的教义上的那句话‘这世界属于凡人，那些高高在上的诸神只是一些虚伪的狂徒’.......”

    当一方摆明立场，打开心扉之后，双方的交流就容易的很多了，就算是罗兰本人，也很惊讶对方的能力和目标，并当即与其缔结了血脉盟约。

    而在彻夜密谈之后，奥罗斯将准备了十多年的谋划交给了罗兰，而罗兰却在他的计划之上，再度完善了它，最终，才有了所谓的“大计”。

    “大计吗？不愧是拜尔三百年来最强的阴谋家，真是敢想。不过若这个计划真的成功，恐怕称其为千年以来最睿智的君王也没错。”

    这是当时罗兰对奥罗斯给出的评价，而此时，千年最睿智的君王不知道有没有，但一步一步把自己的名声毁掉，把脖子递上断头台的奥罗斯，却毫不犹豫，毫不后悔，毫无怯意的把自己放置在绞刑架上。

    “……一切，为了凡人的世界，一切，为了真正的……天堂地狱！”(未完待续请搜索飄天文學，小说更好更新更快!







  



