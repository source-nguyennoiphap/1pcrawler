








    “恶魔（de摸n）和魔鬼(devil)？那只是同一个词语的两种说法吧。～”

    大部分的主位面居民是无法分辨两者的差距的，毕竟对他们来说遇到这两种存在的机会都很少，而若是真遇到了，区分两者也没多大区别，反正都是死亡结局，灵魂还会被带到下位面。

    但实际上两者完全是不同的生物，从性情到习惯都差距极其巨大，且互为死敌，在大部分情况下，若他们狭路相逢，开战绝对是第一选择。

    “那些狡猾的魔鬼？他们的脑浆中都是阴谋和计算，而只有脆弱的家伙才需要这些东西，在恶魔大爷面前，他们脆弱的身躯和搞笑的邪法一无是处。”

    “恶魔？你是说那些用红二头肌思考的蠢蛋吗？世间中这些愚蠢的生物简直是浪费空间，他们唯一能够自傲的，就是那比我们稍微多了一点的数量。嗯，或许还要加上那些和他们丑陋的外表极其相称的蛮力。”

    从某种意义上，虽然他们同为深渊的住户，但两者所处的“深渊地狱”和“无底深渊”完全是两回事，只看数量的话，恶魔可不是只多一点点，恶魔最少是魔鬼的十倍，但由于恶魔们大部分忙于内战，这数量优势也没有实际意义。

    魔鬼般的狡猾，虽然其中稍有多余的含义，但的确是一句赞美，这也说明了魔鬼最明显的特征——狡猾。

    “睿智被视作狡猾，短视被视作果断，真正的勇敢被视作鲁莽。胆怯却被视作镇定。凡人的看法。别太在意——卡米尔提亚斯。”

    魔鬼的来源在普通人眼中是秘密，但对于古老者来说，这些从“初代人类”转换身份的堕落者，并不是什么奥秘。

    而作为曾经的秩序阵营，即使堕落了深渊，他们依旧保持自己作为初代秩序生物的习性，尤其是第一代人类中最高老的四位强者依旧君临整个魔鬼世界，就更他们保留了不少过往的习俗和阶位。

    作为混沌之子。他们却极度重视阶级和秩序，虽然族内内斗不断，但在必要的时候也能够相互协作，而对魔鬼来说，什么样的能力应该站在什么的位置是一种天性，若能力和地位不符的话，一次成功的阴谋被视作荣耀，毕竟，下克上成功，也多半也代表着这个魔鬼的能力应该处于更高的层次。

    从某种意义上。他们的社会结构和现代社会的私人公司有很多相似之处，每个人都看似为公司服务。但实际上却是为自己服务，削尖脑袋往上爬最简单的途径就是让上级犯下大错和打小报告，所以魔鬼的社会虽然表面上不像恶魔那样血淋淋，但也在耗费心力的同时，异常残酷，而偏偏魔鬼们以这种“心斗”为乐。

    而值得一提的，就是那些经营的不错的私人公司也有相对稳定的高层，和那些总是轮替的恶魔王爵、贵族不同，魔鬼九君主很少发生更替，其中不少更是从初代人类时期走到现在的老家伙。

    他们中的任何一位，都是可以让任何帝王和真神为之头痛的存在，他们策划的阴谋让无数的王国化作泡影，让古老神明陨落成真神的残骸，有的人无法理解这些魔鬼的目标，但或许是因为，这些魔鬼大佬们策划阴谋只是因为日常太过无聊，他们所想获得的，只有阴谋得逞的愉悦和乐趣。

    而如今，这些让神明头痛的老家伙却全部走到我的面前，并俯身行礼，向我低下了从未向任何神明低下的头颅。

    “小罗兰，按照‘那个存在’的意思，从现在起，我们将听从您的命令，请放心的把我们当做工具使用。而只要您能够完成吾族的夙愿，我们将尊你为……”

    “咳。我知道了。”

    “谎言之王”卡米尔提亚斯的话语说到一半就被我阻止了，毕竟我们周边还有那些白龙存在，尤其是太古白龙萨默，他此时正饶有兴趣的看着我们，但看似纯真的双瞳中却有掩饰不住的惊讶。

    卡米尔提亚斯，魔鬼九君王中最年长的存在，因此谎言之王也被人称为魔鬼之王，这样的存在，却被艾耶亲切的称为小提亚斯。

    只看外表的话，谁都不会认为眼前彬彬有礼的中年绅士会是魔鬼，他留着八字胡，带着单片眼镜和绅士帽，手臂下还夹着一本书册，稍显谨慎的目光中却带着些许诚挚，一眼望去，就仿若一个专注于学术研究的贵族学者。

    但被他欺骗的英雄、平民、贵族、王者，若是把名字列出一个名单来，大概会比词典还要厚，最离谱的，就是在凡人的世界查不到他的实力和擅长的东西，也就是他根本没有出手的记录，那么，要么就是见到他真实实力的家伙全死了，要么就是他根本不用出手就能整死任何人，前者还在理解范畴，后者就更难对付了，而偏偏从我在艾耶那里搞到的资料来看，他应该是完完全全的不需要出手。

    “凡人，你的谋划打动了我，即使你失败了，我也会给你在我身侧留一个位置的。但我期望不会有用到的那一天。”

    “叛逆之王”马尔辛也是男性的魔鬼，但和化身人类形态的谎言之王不同，这位以煽动叛乱和下克上为人生乐趣的魔鬼君王，却依旧保持着魔鬼的形态。

    至于是什么形态？我抬头只能看到他的膝盖，黑黝黝的雾气让人看不清他的面容和体型，整个人如龙卷风中的黑影怪物，但那碧绿的双瞳却闪烁着邪光，雄厚的声音在整个深谷回荡，回音都让我不住耳鸣，还是很有帝王风范的。

    “别这么粗鲁，马尔辛。吓得了新人多不好，亲爱的罗兰。可以和伊伊姐姐讲一下你过去的故事吗？虽然听‘他’说过一些。但我还是想听你说说。”

    伊弥密思雅。“诱惑之王”，所有欲魔的女王，或许这些善于欺骗和诱惑的女魔在名声上并没有恶魔的魅魔那么响亮，但并不是她们能力不及，而是当她们在主位面做事的时候，习惯把自己做的那些事情推到外形和能力颇为相似的魅魔身上。

    仿若知音姐姐的声音在耳边回荡，虽然看起来是个温柔可爱的女子，但一想到这位仅仅用声音就能让人想到床的女魔。有近三位数的情人和同等数量的麻烦，我就觉得她是魔鬼中最麻烦的，毕竟其他的魔鬼大君不会在这个关键时刻给我找麻烦，这位只要稍微和我走近一点，就代表着无尽的麻烦。

    这位艳名远扬的女王足足有三米多高，却没有一丝臃肿，黄金比例的魔鬼曲线无疑是诱人的资本，清纯可人的面容上是天使一般的纯洁，完美的诠释了天使面孔、魔鬼身材的含义。

    诱人的气息已经到面前，我点了点头。却直接后退两步，以直白的意思表示拉开差距。

    这倒不是我有什么洁癖。而是背后女仆的刺人眼光很让人头大。

    是的，女仆，依旧是女仆。

    我想象过，放弃一切选择力量的伊莉莎化身恶魔后，会变成什么样子，而最保守的估计，都是百多米的巨大妖魔。

    这倒不是我对恶魔有什么偏见，而是恶魔这种彻底的混沌生物从骨子里都透着混乱的气息，魔鬼们有金字塔一样的社会结构，下层可以通过自己的“努力”做出成绩，获得上级的赏识和“提升”，那怕那需要几百年。

    而恶魔们获得更高的地位和更强的力量，就只有两条路——杀和吃。

    杀光一切能够看到的，将战斗中的经验化作成长的力量，若这点还可以理解的话，恶魔们那吞噬同族就很麻烦了，任何一个恶魔都可以吞噬那些弱者和失败者，用他们的营养和长处强化自身，获得更强的变异

    这种能够自我进化的种族能力看似很给力，实际上的确很给力，但却让这个种族从没有停止过内斗，甚至大部分恶魔都把同族视作头号敌，他们生命中九成的时间再和恶魔内战。

    但也因为这种种族天赋，她们可以说在高端战力之中，恶魔是最好量产的，恶魔领主只要有足够的高阶恶魔厮杀吞噬就能够产生，只要有足够的中阶恶魔，高阶恶魔也不会少。

    而这样的成长历程，充满了无数不可测的变异和突然情况，没有一个恶魔拥有完全一样的力量和**，毕竟他们自己都说不清自己是怎么“吃”到这一步的，顺带一提，伊莉莎那吞噬掠夺的灵魂徽印，也和她深渊之子的身份、恶魔血脉有着千丝万缕的联系。

    而正是由于这极度的弱肉强食的种族天性，恶魔们从小打到到，从最弱的小恶魔一只打到恶魔领主，他们极其善战且渴望战斗，同阶的恶魔在各个种族中也是战力极其出色的。

    有一个笑话，若把差不多同等阶位的魔鬼、天使、恶魔放在一起，只有胜者才能出来，打赢的绝对是恶魔，活的出来的绝对是魔鬼，至于天使…….可以问问那两个混蛋鸡翅膀好吃不。

    事实上，当这个无聊的笑话传播开来，也的确有更无聊的魔鬼大佬去做了试试，而结果却在百次实验后，七成以上出来的都是恶魔，二成以上是天使，毕竟所谓的魔鬼的智慧遇到了根本不讲理的恶魔，就如秀才遇到兵，恶魔往往会先敲死魔鬼再去和天使斗，恶魔赢了还有很高几率完成双杀，但魔鬼就算险胜了恶魔，也多半立马死在天使手上。

    咳，言归正传，恶魔由于这种把弱肉强食铭刻在骨子里的吞噬能力，他们的进化都是奔着更强、更壮、更凶上去的，结果是目标虽然达到了，他们的**能够承担自己的作战要求和能力，但多半会带上更丑的负效果，除了魅魔等性质特殊的恶魔稍微能看点，大部分深渊恶魔大佬都是各类恐怖怪物造型，那千奇百怪的能力更是不打不知道，等知道了也多半进冥河了。

    而我预想中的恶魔奶女侯爵，怎么都要个一百多米高，四条腿或六条腿，浑身都燃烧着绿色的炼狱魔火，说话就是雷鸣，移动就是地震，走过的路就是火山，摔倒了就制造地裂…….

    “真没想到，居然还是这幅样子。”

    此时的伊莉莎带着金丝眼睛，那熟悉的银发下是精灵一般的尖耳，那黑白相间的女仆装依旧是过去的那身款式，只是似乎我设计的裙子稍微长了两分…….咳，和过去相比，失去了尾巴和爪子这样的炼狱化特征，我的女仆长比过去更像人类了，至少我在她身上找不出一丝恶魔的痕迹。

    当然，这反而说明她肯定进行了肉身变化……我还不傻，肯定不会问出口。

    而刚刚从位面之门中走了出来，我还没来得及打招呼，她就默默的以这个姿态站在了我背后，虽然恶魔女侯爵的友情站街给足了我面子，她的那帮恶魔手下看到主上这模样就像是活见鬼和鬼撞墙，居然有一个狂战魔直接用爪子给了自己狠狠一下，当场爆掉了半颗眼珠。

    尤其是看到魔鬼大佬们看见伊丽莎时，那提防和满心戒备的眼神，让我的虚荣心也微微上涨，但从那背后那越发冰冷的熟悉眼神来看，我好像又不知不觉的得罪了她。

    “……我不是百米高、四条腿真是抱歉了，不能一脚踩死你真是遗憾，我倒是认识一个符合你要求的火山领主，人家据说有两千多顿，三百多米高，蹄子还带岩浆。要我介绍给你吗？哼，真不愧是一家人，你和悲风的兴趣越来越像了。”

    啊，这熟悉的毒舌，还真是让人怀念啊，但我要严肃声明，我和悲风毫无干系，他这样的男人世界上有一个已经够了，再多一个天灾榜都放不下了，我明明是正常人。

    “哼，树（艾米拉）、史莱姆猫、还有自己的半恶魔亲戚（蕾妮），你口味比悲风还重。”

    “比悲风口味还重……”

    这沉重的一击让我弯下了腰，让我开始怀疑自己的人生，但考虑到眼前还有重要的客人在，可不是胡闹的时候，至少我面容上还是满脸严肃，没有一丝变化。

    “绝望主君了？”

    “他的化身在执行任务，恐怕会晚一点了。那个任务，你也是知道的。”

    我点了点头，和我们有约的四位魔鬼大佬来了三个，还都是真身降临，诚意满满，连另外一个也按照计划中的执行事先约定的任务，看来，我们可以直接进入下一步了。

    “通知奥罗斯和艾耶，我们的大计也到了启动……”

    我的话语还没说完，天空却突然黯淡下来了，巨大的黑龙在天空中探出了头，炙热的龙息吹走了云彩，无数的巨龙从天而降，看来，他们终于发现情况不对，质问上门了。

    “……看来，要先应付一下眼前的小麻烦了。”(未完待续请搜索飄天文學，小说更好更新更快!







  



