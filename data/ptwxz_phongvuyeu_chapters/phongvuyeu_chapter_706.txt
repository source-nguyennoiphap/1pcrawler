








    安文村只是一个由于美食银熏鱼稍有名气的小村，这样的小村在整个司璐威尔无计其数，此时，雪花缓缓落下，原本宁静的小村落却不那么平静。

    只能用来遮拦野兽的栅栏挡不住真正的猛兽，趁着午夜，灰色的狼骑兵进入了营地，杀戮的的盛宴在荒野小村中展开。

    小镇的守卫和守夜人第一时间被抹掉，在训练有素的狼骑兵突袭下，最多只挑战过食人魔的民兵连敲响警铃的机会都没有。

    午夜二时，三十骑狼骑兵进村，午夜二时十五分，民兵队长、村长、土绅都从温暖的被窝中拖了出了，他们的无头尸体在村口喂狼，人头被用来挖掘抵抗者的意志。

    二时四十分，全村五百人都被拖到了村口的小广场，女性和超过一刃的孩子被拖到一旁，然后屠杀开始，可能作为兵力的青年男性中没有幸免者。

    有人尝试抵抗，但在真正的职业战士面前，平民的抵抗毫无意义，更不要说狼骑兵本就是强战兽人中精锐。

    高效率的屠杀后，凌晨三时狼骑兵已经离开，临走的时候随手放了一把火，带着鲜血的狼足在雪地中留下一个个血脚印，满身鲜血的骑兵依旧默默无语，但背后却只有熊熊燃烧的废墟和火光，那撕心裂肺的哭声让人发噩梦。

    或许一开始他们还有所犹豫，但这已经是一周内他们毁灭的第四个村落，从进入到离开不到一个小时，这已经成为一种高效率的屠杀。至于屠杀的良心谴责？他们已经麻木了。

    这就是战争。既不神圣也不正义。唯一需要考虑的，只有通往胜利的途径。

    分散成小队的狼骑兵的任务，是在人类王国的腹地流窜，制造更多的混乱和恐慌，牵扯对方的兵力和精力，而虽然手段极其恶劣，他们也的确做到了。

    兽人并不是没有底限的禽兽，屠杀平民的恶徒更让每一个强者发自心底的鄙视。人民崇拜敢于向巨龙冲锋的无畏骑士，却绝对不会从心底尊敬一个弑杀平民取乐的屠夫。

    “父亲，这次和人类作战，您有多少战果？杀死了多少人类将军？”

    “不，我只是屠杀好几千平民。”

    一想起回去后自己孩子会然后询问自己，作为狼骑小队的队长阿克力虽然是部落中出名的勇者，就不知道如何回答。

    此时，他灵敏的嗅觉中依旧满是刺鼻的鲜血，梦到孩子的质问是他最可怕的噩梦，他能感觉到内心的疲惫不堪。

    在兽人的概念中。和强敌力战而亡是一种至高无上的的荣耀，而对弱者的屠杀却无意说明了那个强者内心的软弱。这样的战士在部落会遭人鄙视，死后更会被战神拒之门外。

    “这是上级的命令，这是为了吾族返回故土的夙愿，这是为了吾族的大义！”

    正义啊，多少罪恶假汝之名而行！光鲜的旗帜后面站着的往往是罪恶的刽子手。

    他也知道这是自欺自人，但至少有这样的借口，他才能昧着良心，继续屠杀下去。

    “队伍越来越沉默了，看来要想点办法鼓舞一下士气了。”

    只有最没有底限的恶徒才会炫耀自己屠杀平民的“战绩”，从这些骑兵紧皱的眉和僵硬的表情来看，他们恐怕也也还没有到那一步。

    但他们依旧这么做了，义无反顾的做了，毕竟，这是战争，万恶的战争，美妙的战争，可以让一切罪恶披上神圣外衣的战争。

    “嗷嗷嗷！”

    狼嚎在前面响起，那是作为侦骑的鹰族兽人的回报，这三声狼吼无疑说明了前方有敌人挡路，但却实力不强，可以直接冲锋。

    而很快，越过这片山林。那个“敌人”就到了眼前，那是一个头上满是雪花的金发年轻人，看外貌相当年轻，但那一手一把的双手巨剑，却异常打眼。

    “唉，又是一个独自追上来的年轻人，有勇无谋，可惜了，这年龄大概和我的爱库尔一样大......。”

    看着这个和自己孩子一般大的年轻剑士，阿克力稍微停顿了一秒，接着，却是拇指划过脖子的手势，那是见敌必杀的指令。

    “刚才那个村子和爱库尔一样大的年轻人更多，那么，为了爱库尔有一个美好的未来，你就去陪他们吧.......奇怪，这把剑有点眼熟？好像在那里看到过。”

    多余的念头已经没有必要，即使只有一个对手，狼骑兵依旧摆出了自己最拿手的波浪冲锋阵型。

    坐骑最壮硕的数位狼骑负责接触肉搏，作为最强者的阿克力等人在二线准备突袭，他们背后的投网和标枪早已经准备就绪。

    强大的狮鹫骑士都有可能在一个照面被狼骑兵们秒杀，在骑兵眼中，这个不知天高地厚的年轻人已经是死人。

    面对狼骑兵的绝命突袭，那个年轻人却突然笑了，笑容中满是残酷的韵味，这熟悉的笑容，让阿克力终于想起了在那里见过那把剑。

    “哦！那不就是小镇雕像上的那个人，剑气外放？是剑圣！......那把剑？难道是罗兰圣剑？圣骑士罗兰？”

    这却是阿克力最后的念头了，雪白的剑光在飞雪中闪烁，飞扬的剑气开始夺人性命，只是脖子上一凉，狼骑兵队长的视角直接倾斜，从狼背滑到了雪地中。

    最后映入眼帘的，却是那个银甲剑圣挥动双手剑，一刀一个的屠杀自己的骑兵的模样。

    此刻，狼骑兵那毫无抵抗余力的模样，就仿若无力的小鸡遭遇了屠夫，和自己半个小时前在那个雕像前做出的恶行没有任何区别。

    “杀人者人恒杀之吗，还真是让人讽刺啊.......”

    仅仅一个照面，喀拉部落的勇者就失去了自己头颅。作为屠夫的他再也不用担心怎么回去向自己的孩子交待了。

    “血债血偿。去死！”

    不知是谁。最先喊出了这一句，而很快，一切都恢复平静了，只是地面上又多出一些无头尸体。

    是的，血债血偿，但到底是从那边开始欠的？是从这场战争开始算，还是从二十年前的战争开始算，或是从三百多年前的大战开始算？恐怕。这注定将是一笔永远都算不清楚的糊涂账了。

    ----------

    击杀了最后一个狼骑兵后，我也很有些疲倦，于是就把剑插入雪中，在战场上歇息起来。

    在等候艾琳贝拉清理逃窜的散兵游勇的时候，我还想起一些其他的事情可以做。

    从那个狼骑兵怀中掏出一个羊皮袋子，意料之中，除了补给品之外，更有附近村落的地图，而上面那一个个红叉，只是说明狼骑兵已经去过。我已经不用赶去了。

    入目的都是尸体和鲜血，我却莫名的有点渴。于是打开对方留下的水袋，狠狠的干了一口，那甜的腻人的劣质马奶酒，莫名的，却全部吐了出来。

    “呸，又苦又涩，和当年一样。”

    在我抱怨的时候，海洛依丝已经完成了对狼骑兵小队路线的解读，于是，我下个目的地已经确定。

    “蓝湖村吗？希望这次能够赶到前面，我可不想再看到那样的废墟了。”

    想起刚才安文村看到的惨状，我加快了动作，飞快的整理完战场，确定没有遗落的情报，直接跃上了艾琳贝拉的马鞍上，赶下了下一个目标。

    这已经是一周内我除掉的第十二个狼骑兵小队了，同伴们都在报告自己的收获，独自行动的我算是收获少的，但比起整个战场糜烂的战局，恐怕却依旧只是杯水车薪。

    唇亡齿寒的道理谁都懂，司璐威尔一旦完蛋，接下来倒霉的绝对只是北地诸国，所以，大雪仅仅只是稍微减弱，各国的援军和援助就上路了。

    我们是来的算是最快的，但恐怕也是人数最少的，一百亡灵骑士，三百菜鸟骑士，恐怕在以万为单位的主战场上起不到什么作用。

    考虑到东岚的现状和冬天要面对的兽潮，能够出兵就已经让其他国家吃惊了，带队的是我本人，更让多余的杂音直接闭嘴，嗯，至少在我面前闭嘴了。

    当然，数量是一回事，质量又是一回事，我的亡灵骑士战力可靠，那些菜鸟们都是从各个新建军团中挑出的骨干，所缺乏的只是经验。

    而对我个人来说，也的确对安图恩主战场兴趣缺缺，其他各国援军都冲着各个兽人主力部队去的，我却把注意力投向了这些散兵游勇。

    兽人的分兵战术已经起到了效用，司璐威尔王国各地的领主都在忙于清剿自己领地上的游骑兵，对于王城的求援自然无视了，而那些稍有余暇的大领主，则被另外三波兽人大军牵制了。

    轻骑兵（不仅是狼骑兵）游猎牵制并不是什么罕见的战术，但配上兽人精锐的小顾部队，在战术层面上却极其有效，更极其残酷。

    对于一个普通的村落来说，袭击者是剑圣还是青铜阶的狼骑兵根本没有区别，反正都是被屠杀灭村的结局。

    追杀这些游骑兵也是典型的吃力不讨好，他们机动性太强，神出鬼没，很难追上不提，就算追上了，凭借高出一线的个体战力，谁输谁赢还不知道。

    而当兽人主力没被剿灭之前，各国正在集结的战力也没有余暇去管别国的小村落，而我却打算管。

    “管那些贵族去死，既然我们来了，能够插手帮忙，就帮下吧。”

    我的人的确少，但亡灵骑士们个体实力都不低，独自面对一个狼骑兵小队也是轻轻松松，而菜鸟们正好打打下手积累经验。

    于是，我就干脆把战团解散，让其以一带三的形式小组自由行动，反过来狩猎游骑兵小队。

    而情理之中意料之外的，却是这样的行动反而收获了极大的民望，毕竟现在原本应该承担守护职责的领地贵族和王室无所作为，而我的亡灵骑士却是最好的标志。

    “先王罗兰带着他的军团来拯救岚之民了，那个伪王菲姆尔在无视了罗兰陛下的警告后，再度无视了国民的苦难，只顾着自己的权位和安全，缩在王都之中。”

    从某种意义上，司璐威尔王室之前的公告正在不断展示什么叫做愚蠢和作死，“不计前嫌”的我却随着反狩猎的行动声望日益增高，亡灵骑士们开始受到热情的支持，干劲更足了。

    而接着，让所有人吃惊乃至不可思议一幕发生了。

    那个菲姆尔看到这种情况，明明安图恩的战时依旧吃紧，却通过教会来警告我不得乘火打劫收买人心，还再度宣告我并不是先王罗兰，只是一个蛊惑人心的冒牌货。

    显然在那个“陛下”的眼中，兽人大军只是迟早要被驱逐的外患，而我和我的骑士们确实所谋甚大的“野心家”。

    当时我差点转身就走，但接着一想，反而被气乐了。

    “行，你说我收买人心，我就真的来收买人心吧。骑士们，拿出我们的战旗和军徽来，我们是堂堂正正救援的援军，又什么好遮遮拦拦的。”

    当然，从某种意义上我分的很清楚，民众是民众，王室是王室，但更重要的，却是有些事一旦插手就很难收手，难道就因为安图恩的那个蠢货，我就看着那些村民去死。

    而当这些消息传了出来，司璐威尔王室的声望更是一落千丈，每天都有无数的难民选择逃向原本根本不会选择的东岚。

    据说听到这个消息，那个菲姆尔当时就摔了王室百年传家宝的七彩虹玉酒杯，然后就很后悔了，还很伤心，得知这个消息后，而我为了安慰他，还通过教会给他发了个信息。

    “七彩虹玉酒杯？那个不是我当时烧玻璃烧出来的副产品吗？放心吧，别看它外表不错，实际上就是琉璃玻璃，根本不值钱。其实那也不是酒杯，只是烧的太过失败的小痰盂，还装过蛐蛐.........那个杯子背面应该有罗兰造的暗铭，若不是摔的太碎，可以找找。”

    好吧，据说还真被他拼了起来，但这次被摔的就不止一个酒杯了。(未完待续。。)

    ps：又是一次幸运e,太惨，停电加上被吃掉的千多字的稿子,硬是更晚了.....

    最近实在太忙，欠的三次万字更新这个月搞定.......

    那啥，有票票的书友支持下吧.....







  



