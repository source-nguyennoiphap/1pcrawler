








    杀戮是什么？大概，就是以停止对手生命为目的的行动。

    而我眼前的一群，恐怕是此世最纯粹的杀戮兵器。

    还算不错的面庞挂着和善的微笑，但双瞳中却没有丝毫笑意，明明没有一丝杀意，但却行着杀戮的行为。

    “真是，这种笑着杀人的变态，和谁学的。”

    【…能不能稍微有点自觉，他们都是谁的复制体，是谁越是危险笑的越开心。】

    “…….卡文斯。”

    【…….你就装吧…….】

    或许，是知道机会不多了，和蠢笨把一切弄砸的女神大人有一搭没一搭的聊着，居然意外的让人珍惜。

    战场？很顺利，顺利的不用投入更多的目光。

    此时的卡文斯，就如被围猎的巨兽，即使依旧狂傲愤怒，依旧威势逼人，但身上已经插满了冰矛、冰剑，仅仅只是放血量，就足够生物步入终结。

    这群微笑着的罗兰，没有怜悯，没有愤怒，甚至连敌意都缺乏，但每一次攻击，却是那么的危险。

    以身化龙，奥义绝杀，以伤换伤，即使付出性命，只要留下一道无法愈合的伤疤就是划算的。

    连作为创造者的我都无法想象，当“我”豁出性命的时候，居然能够如此危险。

    陷入了绝境的卡文斯很强，强的让人绝望，我惊讶的发现他的个体实力隐隐约约还在索福克里之上。

    但在1342位罗兰面前，即使仅仅只有不到一半实力的罗兰面前，卡文斯的强大依旧毫无意义。

    就如武士时代的剑圣，面对钢铁巨兽组成的洪流时，遭遇的不是能够正面交锋的对手，而是力量级别差距的碾压。

    而对卡文斯来说更不公平的，是他一旦攻击“罗兰”，那反弹的伤害一样要命，而“罗兰”们，却毫不犹豫的步入死亡。

    当个体的实力差距没有被拉开的情况下，当卡文斯被绑住了手脚后，赢得，自然是人多的一方。

    “嗷，罗兰！你敢……”

    “我不敢。”

    我都能够猜到这个单细胞对我喊叫什么，无非是“和我正面交手”之类的废话，言语上的挑衅对我还有意义吗…….而且，现在的我，恐怕也只剩下看的力气了，或许，连这个“看”都很勉强。

    视野已经模糊一片，只有对生命力感知了解眼前发生的一切，似乎，随着灵魂和肉身的崩溃，听觉也已经失去了存在的意义。

    “滚开！”

    似乎卡文斯爆走了，气息猛地升高了不少。

    整个世界被点燃，混沌的领域正在试图破坏空间，只要逃离了这里，我的确没有气力再布置一场。

    但还没有等他撕开封锁，“罗兰们”就布置了冰雪的结界，重新稳定了漏洞。

    而虽然已经看不清了，但我可以感觉到，卡文斯已经被逐渐削弱，再削弱，再再削弱，他的生命力，就如摇摇欲坠的火苗，随时都可能熄灭。

    “呵呵，还真是凄惨。”

    蠢女神给我传递了一份景象，我那无法无天的弟弟，现在被冰雪的长矛森林刺再半空中，那些“罗兰”正在完成最后的收割。

    惨叫在耳边响起，魔神的鲜血不断滴落在雪地中，恶魔王子的灵魂之火摇摇欲坠，但却始终无法别熄灭。

    “为啥每次最后都要用命来拼？”

    过往的记忆如影片般在脑海中出现，这走马灯式的死前回忆已经是第几次了？难道我的对手就不能稍微弱一点吗。

    *已经失去了意识，卡文斯的惨叫依旧继续，恐怕这样子下去，我会倒在他之前，幸好…….

    在可以感知的头顶上，无眠者的虚影再度出现，当那枚法槌落下的时候，最后一次审判也到了。

    “终于，结束了…….”

    当银色的闪光充满了视野，我笑了，这次，是发自内心满足的笑了。

    然后，就是一片熟悉的漆黑……

    “…….这次，可以好好睡一觉了。”

    “睡觉？结束？你在做什么美梦！”

    那熟悉的怒吼在耳边响起，难道卡文斯已经冲破了我的牢笼？

    我猛地惊醒，却发现此时，已经不再深渊之池的最深处。、

    那是一条无边无际的河流，那熟悉的灰黑色河水之中，无数的灵魂正在浮浮沉沉，而我面前，那个暴怒的家伙，好像很有些眼熟。

    “…….卡文斯？”

    我很惊讶，因为在我面前的，是一样脆弱的灵魂。

    “哈。”

    我终于了解了此时的情况，看来，不管是谁先死的，至少现在，我们都已经挂掉了，那么，一切就结束了。

    我笑了，至少还有转生的可能，这已经比预期的最坏情况好太多了。

    “结束？休想？你就在这里躺着，吸着手指看着我怎么打开大门吧。”

    卡文斯的话语很有些荒诞，他都这样了还能做什么，但下一刻，我就理解了。

    他的灵魂双瞳失去了情绪的色彩，和周围那些没有自我的死灵没有什么区别，这种情况，他的意识还能去那？

    “………意识转移了？还有血脉后代？”

    卡文斯之前的威胁在我心头想起，“我们都知道的血脉后代”，该死！

    “蕾妮！？”(未完待续。)







  



