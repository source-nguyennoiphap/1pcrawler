








    “……女神，不知道您的好感度排第几？”

    “……噗，也就是说好感度零了？不，应该是没出现在好感列表上，这难道是没把你当女人的另外一种解释？”

    脑袋里面仿若聊天室的冷嘲热讽做恍若未闻，这群家伙看我笑话已经不是一天两天，天空中正在聚集死亡之力的幽灵才是眼前的重点……

    “嚓！痛痛痛！”

    随着那藤蔓的收紧，浑身的骨头却都不断作响，思考什么的成为奢望。

    好吧，我承认背后那个正虎视眈眈的盯着我的疯女人才是重点，女神大人和猫大姐，有空闲聊先救救我吧。

    魔女的魔性美貌一如往昔，带着些许自然卷曲的绿色长发随风飘起，血红的双瞳如冰晶一般美丽……前提是忽视那藤蔓长发已经在撕裂我的皮肤，而那个如水晶般透彻的双瞳中是毫不掩饰的杀意。

    “那个，能不能放松一点……”

    娇艳的红唇微微的划出一个弧度，轻轻的在我耳边吹气。

    “不，行，哦。”

    香甜的气息弄在耳后痒痒的，舌尖居然还暧昧在耳间轻轻舔了下，很是冰冷，似乎有些暧昧，当然，前提是忽略那越来越紧的藤蔓。骨头越来越大声的惨叫。

    “谁知道放开你后，又会……”

    暧昧的话语甜丝丝的，就像在心头饶痒痒。但下一霎，浑身上下都响起了同样的脆响。

    “咔嚓！”

    毫不留情的骨头脆响。让我很是心焦，而跟更要命的却是居然感觉不到痛感，恐怕这只能说明那些该死的触须已经开始接管神经系统。

    而当我还在努力命令脚拇指移动的时候，背后的轻言细语却突然转换为严厉的质问。

    “……说，跑到那个贼猫那里去！”

    耳边又恢复了知觉，居然在轻舔舔，咬，真咬了！

    “痛！”

    喂喂喂。你说就说，动嘴做什么，你在咀嚼的那半个血淋淋的耳朵，难道是我的……虽然很感谢你瞬间帮我又捏了一个出来安上去，也知道你掌握了逆天的生命创造的权限，但能不能不要这么惊悚，会做噩梦的啊！

    “绝境啊！”

    这个时候，自己的小命已经在危险的边缘，那个天空上的麻烦已经不算什么了。

    “一年多前还没这么凶残这么黑……”

    “被一个男人爽婚了一次还是两次还是很有区别的。对吗？亲爱的。”

    现在的肉身是对方创造的，离得这么近。连身体都无法控制了，居然连想法都有点外泄了。

    “不，不。我没有读心，这点底线我还是有的，只是你这个时候会想什么我还是能够猜到的。”

    但这样的绝境还是难不倒我，我早预料到可能会有今天的情况，所以已经准备好了杀手锏，即使她现在痛恨我入骨，但当我说出以下那段话，她就会再度打开心房…

    “曾经有一份真挚的感情放在我的面前……”

    “……我却没有好好的珍惜，等到失去后。我才后悔莫急！人世间最痛苦的事莫过于此。如果老天能再给我一次机会的话，我会对那女孩说三个字：“我爱你！”如果非要加上一个期限的话。我希望是一万年！”

    “你怎么会知道……”

    “虽然听听你说一下甜言蜜语也不错，但是经典戏剧《罗密欧和至尊宝》的经典台词就算了吧。哦，我都忘记那本书还是你弄出来的，这么说还真应该听听。看看原作者是怎么演绎那段让无数少女痛哭流涕的感情戏的。”

    “罗密欧和至尊宝？”

    看来貌似我又不小心忘记了什么，但从这玩意的名字来看，估计我当时还在报复社会的叛逆期，不会是性取向错误的诡异玩意吧。

    “痛哭流涕？你？”

    “不，我可不是什么得到你的爱情比更重要的小丫头，若我真是那个只会哭和后悔的罗密欧的话，早就把那个负心的至尊宝打碎浑身骨头拖回家了。”

    “……呵呵，还真有真实性的威胁感。那个，能把藤蔓稍微松点吗。”

    天无觉人之路，事情要往好的想，但在我的人身安全遭到威胁的另外一面，至少某些方面却自然好转。

    我相信她不会看着我被其他人干掉……至少不会她报仇之前，我还是暂时安全的……为啥我说这话这么别扭。

    天空之上，虚妄的苍白幽灵在聚集这一团团的死亡之力，这块土地上死亡的亡灵已经太多，足够的灵魂的残片不断浮上天空，被其吸收，让她的颜色变得真实了很多。

    我已经看出了恒在做什么，她在用自己的能力召唤死亡之力和亡灵的残片，补足自己的缺失。

    不管她是用什么方式扛过那场大爆炸的，绝对付出了巨大的代价，按照常理，此时我们应该痛打落水狗，一窝蜂上，但从天上的战况来看的话，恐怕要攻击这个状态的恒都不太容易。

    艾米拉并不是一个人来的，能够赶在某些人前来，只是因为那天空中的一只巨型植物战舰。

    那颗原始的神木战舰有些当年在梦境之森的初代舰影子，但却庞大的多，它看起来只是原始的树木，周遭还有绿色的花纹和分叉的树枝，但从那些部件的高能反应和漫天飞舞的舰载机来看，绝对不是游览用的船舰。

    飞翔的除了那些看起来很幼小可爱的木灵之外，那些仿照各类飞翔猛禽的魔化植物外壳也并罕见，但最让人恐惧的还是那铺天盖地的数量。

    无数的诡异植物、动物在天空中盘旋，从地上仰望，天上全是密密麻麻的黑点，而那艘巨舰的舰身宽度就已经超越了城墙的巨型铁门。

    我认识这艘船，“母亲”号皇家方舟，木灵们为了庆祝“母亲”和“父亲”新婚送上来的新婚赠礼……好吧，让我继续无视背后的怒视吧。

    其上至少有三千多的木灵存在，还至少有两位七美德镇守，得到我的消息，就直接开着一族重宝级的战舰出来，我应该感到荣耀吗。

    就是死亡军团还在，面对这艘战舰的猛攻，也应该很快就会全灭，但天空上的局势却不是如此。

    在无数的爆炸声和攻击之中，恒正在悠悠然的吸收着死亡之力。

    她并没有躲避，这巨大的数量也躲避不掉，所有的攻击都直接穿透其的身躯，却在她身体的另外一边出现，有的木灵凑近了她，居然可以和其重叠，仿若她不在这个次元。

    我见过这个状态的恒，当时是用化身的她处于存在和不存在之间的模糊状态，虽然无法干涉周遭，周遭也无法干涉她。

    而现在的恒显然又更进一步，周遭的舰队无法攻击她，她却在干涉周遭，她在吸收灵魂和死亡之力恢己身。

    我当时是用修订世界规则，让其夺取的热量最终回归己身的方法战胜她的，但此时她并没有放出心像世界夺取能量，过去战胜她的方法显然又无法再用。

    但我并不是全无办法，既然已经知道对方拥有这样的能力还有可能遭遇，我又怎么可能不做好应对准备，但前提是……

    “艾米拉，能够放开我吗？我有办法料理那个家伙。”

    “不行，对了，亲爱的，你是喜欢油炸还是水煮？”

    “……呵呵，你当我听不出来你打算用我选择的方法料理我？不过，既然你让我选择死法，我可以推荐一种吗……”

    “老死？这笑话早就在你《祝英台和朱丽叶》中用了怎么表情如此微妙？”

    听着这强烈激起吐糟欲的名字，我越发无奈了。

    “没有，我只是好奇我当时到底有多仇恨社会。那个，期望我希望死的稍微有点尊严，最后还是处男有点让人丢人，你可以帮我找几十个魅魔吗，我选择乐死……啊啊啊啊，怎么痛觉这个时候恢复了！好痛啊！”(未完待续)

    ...







  



