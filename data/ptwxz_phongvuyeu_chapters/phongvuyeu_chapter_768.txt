








    “你说的圣遗物就是这个？”

    天还没亮，某人的曾曾曾孙女（？）就一脚把某人踢下了床。

    “你造反啊，天没亮闹个什么。”

    昨晚由于某种原因，我昨晚半宿没睡，现在刚刚迷糊就被弄醒，当即就火冒三丈了。

    “这就是你给的圣遗物？你自己看！”

    圣遗物？听起来很高档大气，其实也就是灌注了神力的特殊奇物，比如灌满神血的杯子、刺穿某神心脏的木枪、某神用过的裹尸布，而若是某真神的圣遗物，往往是那个真神登神前使用过的物品，若让其信徒接触，可以加深与真神的链接，迅速提升神力，是各个教会不可多得的宝物。

    而我这里自然有律法之神的圣遗物一大堆，经系统鉴定后，能用的大部分留在硫磺山城供认供奉使用，但有几件却带在身上，现在蕾妮就职公正骑士，随手丢了一件给她，居然一大早就抱怨。

    “这不是很好嘛？圣枕头啊，比卡丘图案的，很可爱啊。啊，你不知道这只著名的电老鼠啊，我给你讲讲，这是我设计的卡通图案，你看，两颊红红的，身子黄黄的，是不是很可爱啊……”

    “谁管你家的电老鼠！口水啊，口水！你家的圣遗物背后全是你的口水，睡到半夜碰到一脸，恶心死了！”

    【无眠者的枕头：圣遗物，由于无眠者长期使用，这个枕头沾染了他的气息。睡眠时接触它，可以加深和真神无眠者的联系。提高律法之力的修行速度】

    接过圣枕头，翻过来，背面还真有口水，犹豫了一秒，就背面朝下，不管不顾的直接睡去。

    “哼，你不用我用，离了这枕头。我半宿都没有睡好。”

    是的，之所以失眠，并不是因为某人等待了半个晚上却迟迟没来的夜袭，而是因为失去了惯用的枕头。

    “喂，起床了！晨练的时间也到了，你还有什么其他的圣遗物。”

    “你因为真神用过的圣遗物是大白菜啊，不过。我这里还真有几件。圣红领巾你是肯定不会要了，圣牙刷要吗？”

    “不要！”

    “圣衣要不，喂喂，别高兴的太早，我就知道你想歪了，不是那些西域装甲型的。就是圣浴衣的简称。粉红色的哦。”

    “罗兰大哥，好歹你也是传说中的英雄，我是听你的英雄故事长大的，能不能靠谱点。我一个女孩子穿着男式浴衣到处走像样吗？”

    “行，我这里有一件圣遗物。和无眠者的联系保证是诸圣遗物之冠，而且穿了他既不多余又不打眼。绝对好用。”

    “这么好怎么不早拿出了？在那？”

    “自己拿吧。”

    于是，我掀开了被子，继续呼呼大睡。

    “在哪？罗兰大哥，能够告诉我吗？”有求于人的时候就低声下气，平时就趾高气扬，这公主脾性还真好掌握，不过显然欠调教。

    “这不就是了。圣内裤，自己拿啊，可以穿在里面，是不是既不多余又不打眼。而且和无眠者贴身接触，保证联系密切。”

    “你这个老色.鬼！又对我性骚扰，吃我断罪击啊！”

    好吧，刚刚转职一天，显然她的断罪击练的还不怎么熟练，只是空有口号，于是，在被瞬间收拾且拖下去晨练后，新的一天，是从某为老不尊的老祖先调教小孙女开始的。

    “居然敢偷袭老夫！今天是两一剑，不完成没晚饭，给我认真练习！”

    “你老夫个什么？休想倚老卖老，凯丽姐和我说了，你现在估计还没我大，叫你罗兰大哥还是看你过去有点名声，否则，呵呵，罗兰小弟，想吃棒棒糖吗？”

    这不良少女显然欠调教。

    “欠打啊！给我练剑，若不能再我练完三万剑前完成，我就罚你今晚没晚饭、没夜宵，你看你凯丽姐姐会不会帮你说情！”

    “别，凯丽姐看似温柔，但实际上一旦涉及到上课就变成鬼婆婆，超级凶悍可怕。”

    “鬼婆婆在你背后……”

    “这么简单的糊弄你以为我信啊，凶婆娘在的话，我早就应该听到脚步声的，她最近体重超标了……谁在揪我的耳朵。”

    “咳，原来我是鬼婆婆和凶婆娘，还劳烦您来关心我的健康问题，看来，公主殿下的礼仪课和卫生课也需要补习了，请结束了剑术训练后再来二楼补习。”

    “混蛋罗兰，你又坑我。”

    “怪我咯？”我无辜摊手，却洋洋得意的笑了。

    ------------

    “我想要把好剑，我是认真的。”

    明明是想找把好兵器，我却拉着那只猫从长计较起来。

    前些天的孤军突袭既是意外，却也早在预期之中，我的确需要找对手来试手，但在品尝到这种新奇战斗方式的强大的同时，也自然发现了弱点，而那天竞技场上的赶鸭子上架，更让我必须正视这个缺点。

    【魔剑：猩红征服者】

    【攻击力：30-50+9】（+9是它本身视作+9级传说武器，即使发挥了武器的下限伤害，也要附加9点无法减免的吸血伤害）

    【血之征服：这把魔剑有五阶形态，他会根据使用者手上的死者数量来确定能够使用哪种形态，每提升一种形态，增加一种特技，最高、最低伤害增加10点，现在处于三级。】

    【饮血者的愤怒：使用后吸取周遭所有生命的血液，造成大量伤害的同时，恢复使用者的体力。】

    【冷血者的毒牙：被击中者将遭受不可遏止的流血伤害，该伤害可以无限叠加。】

    【污血者的诅咒：以剑主为核心。制造一片污浊的领域，所有人将遭受血液沸腾的痛苦。】

    【剑刃处有一串血红的小字：这把魔剑无时无刻在渴望着鲜血和杀戮。不管那鲜血是你的敌人，还是来源与你自己。】

    当时从达索斯哪里接到这把魔剑，我真的在犹豫直接携宝开溜到底有多大的成功率，尤其是我听到了魔剑的诱.惑之后。

    “临时的主人，您手下收割的鲜血和灵魂是眼前蠢货（达索斯）的百倍以上，在近十位剑主中，只有伟大的您可以使用吾的最终形态，那将是现在的百倍强大。但作为代价，您必须在一年内给吾提供一万人的鲜血”

    大部分魔剑都是魔鬼和恶魔的制品，使用魔剑一般都要付出巨大的代价且得不到善终，但等价兑换永远是世间常规，在付出巨大的同时，获得的力量也是极其恐怖的，这猩红征服者在三阶状态就能够和亚神器圣剑罗兰相媲美。若是升级到最终形态会强大到何种地步，简直无法想象，恐怕神器也无非如此了。

    不过，五级魔剑是万人鲜血供养的话，三级恐怕就是百人了，而若使用者无法支付代价。必然就是用自己的鲜血抵扣了，一年百人，三天一个，恐怕达索斯也在忙于血祭。

    当时我就拒绝了，若真转化成最终形态。恐怕光这转化的前提条件就怎么也解释不通了，更不要提之后付出的巨大代价。但作为一个剑手，那魔剑强悍的力量和特效，我又怎么可能不心动。

    “我太缺好武器了。作为一个剑术大师，居然依旧只能拿着两把劣质的黑铁双手剑，实在让人无法忍受。”

    在使用过魔剑和圣剑后，看着蕾妮勉勉强强能够使用那越来越强悍的罗兰圣剑，而自己却在用两把加起来才十个金币不到的铁剑，怎么能够让人不沮丧。

    “喵，你要剑去找铁匠大师，找喵喵我做什么。”

    越来越卖萌装傻的女妖之王，正在舔自己的爪子，已经让人无法直视。

    “别装傻了，你知道我找你为了什么，都是千年的狐狸,玩什么聊斋。告诉我，附近有什么神兵利器没有？”

    “喵，没有，鱼骨头利刃倒是有两把，昨晚吃剩的，你要我就给你。”

    呵，我就知道是这种结果，所以，早就做好了准备工作。

    “好吧，既然没有也没有办法了。”

    “喵，这不像你啊，居然会放弃。”

    “嗯，没有总不可能变出来吧，既然没有，我就自己打造。”

    海洛伊丝有些吃惊了，只知道眼前的人对亡灵造物非常擅长，没听说过会打铁。

    “喵，你还会锻造武器？等等，罗兰.岚，乖徒弟，你打算做什么！”

    但接着，它就懂了，罗兰穿上了那恶魔一般的白大褂，带上了口罩和眼睛，双瞳中满是狂热的火花，而手上的试剂瓶和药瓶中，无数的诡异液体正在流传。

    “你应该听说过那个传说吧，金属史莱姆可以变化成铁棒之类的武器，金属史莱姆王更是可以变化成传说中的神兵。反正你已经是史莱姆了，再改变一点属性，增加一点功能，也算不了什么，对吗？”

    “对个毛线啊！别往老娘体内倒那些东西，镊子也不要！钻子？太大了！你想弄死老娘我啊！”

    好了，在生命的威吓下，海洛伊丝终于没有卖萌的余暇了。

    “我认输，老娘认输了，我告诉你那里有神兵了，把你的钻子拿开！”

    “切！”

    “你切什么切！就这么遗憾吗？你就这么想把老娘插来插去，钻来钻去吗？你这个不肖之徒！”

    好了，面对抱怨不绝入耳的海洛伊丝，我再度举起钻子和试管，她立马就乖了。

    “这附近还真有一把你能用的，你知道苍白的正义吗？传奇圣骑士凯恩曾经用过的圣剑。他是奥兰人，他的后代应该在这座城市，剩下的不用我多说了吧。”

    圣剑吗？我当即喜出望外，这可是预料之外的收获。

    在艾希大陆，圣剑其实就是圣洁、神圣的宝剑的含义，但归于现实的话，往往有两种不同的圣剑，第一种就是代表某个地域、国家、种族、家族的守护之剑，罗兰圣剑无疑就是第一种的代表，而第二种，就是灌入了圣洁灵魂的宝剑，这就往往是圣骑士的专利了。

    这实际上是秩序众神给自己仆从开的后门，当一个圣骑士步入死亡，他又不甘心就此停下打击混沌的步伐，就可以把自己的部分力量和灵魂灌入一把武器之中，留待后人使用。

    这把圣洁的神剑/圣锤就此诞生，他往往限定了使用者必须同为圣骑士，且对混沌生物有着非常强的针对性。

    为啥说是众神留下的后门？因为其他职业这样干都没成功过，除非化身死灵，秩序众神和冥河不会放过任何一个死者的灵魂。

    能够得到圣剑的消息，我自然很满意，于是，当海洛伊丝刚刚松了一口气，我却又举起了钻子。

    “你要做什么！我不是告诉你圣剑的消息了吗？你打算反悔！你这个言而无信的骗子！”

    “首先，从一开始我就没答应你什么，帮你进化是今天的预期目标。其次，我已经勾起的改造瘾头灭不下来，最后，也是最重要的，我是双剑手，一把怎够用。”

    傻猫当即一脸囧像，似乎在检讨这么简单怎么自己次次上当。

    “难道肉体会影响智商是真的。”

    “不，我以前每次坑你不都成了，智商是天生的，你早就该认命。”

    “休想！”

    从愤怒到沮丧，从沮丧到再度激动，海洛伊丝每次被坑的表情都看不腻，或许，这就是我喜欢坑她的原因吧。

    “这是我的地盘，你叫破喉咙都没人救你的，所以，你就从了吧！！”

    “不要，把那白色的油腻液体给我拿开！死变态，为什么会有触手,难道传说中的粉红之书真的在你那。救命啊，有人虐猫了啊！”

    PS：既然好一点了就加更还债吧，两更过万爆发奉上，有票票的书友支持下。







  



