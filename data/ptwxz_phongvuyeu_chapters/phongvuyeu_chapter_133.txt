








    别人国家打成怎么样，我本来是不想管的，也管不了。

    但圣安东里奥的混乱，却注定影响深远。

    至少在荒漠地区寻找遗迹的我，也因此有了不少麻烦。

    记录之中的遗迹已经找到了，在荒漠地区的沙丘围绕之处，我直接胡来开了一个大坑，硬是找到了陷入地下的古代遗迹。

    但很快，我就发现自己找错了目标。

    “蛇神遗迹？”

    虽然写了很多乱七八糟的东西赚稿费，可我并不擅长考古，而眼前的遗迹废墟，却并不需要多少考古知识。

    古代遗迹中最有价值就是坟墓或神庙，它们往往有着不少珍宝和魔法物品，盗墓人……冒险家用魔力探索技巧，就能很容易的找到目标的方向。

    “看来，这次是扑空了。”

    但这次，看到壁画上都是蛇头人身的生物，还有各种巫毒锅、草人、木矛之类的杂物，我就是不怎么深入探索，也知道自己找错了。

    而魔力感知隐隐约约有反应，显然有些魔法道具没有被拿走。

    若是往昔的话，我倒是有很兴趣去玩玩神庙探险之类的，毕竟不少神器、奇珍异宝、古代魔法都是这样挖出来的，盗墓者……探索遗迹始终是最赚的买卖。

    但探索遗迹需要足够的耐心和无限的时间，这些我都没有。

    现在主位面一大堆麻烦事，我可没有闲心在这里闲晃。

    既然已经确定了这个遗迹不是太阳神的地盘，那么，剩下的那个，可能性很大了。

    “在这个时间点，穿越半个圣安东里奥…….”

    光想想，我都觉得头痛，我身上还挂着一个首席通缉犯的头衔，虽然有的人没有当真，但基层的骑士和警卫队可是会较真的。

    一旦身份暴露。恐怕又是麻烦。

    但有些事情，却不得不做，这天下越来越乱了，恐怕诸神下凡的日子就在眼前。若不能尽快实现突破……貌似我在诸神之中的仇人不少吧，永夜时期得罪的神祗不是一个两个，若真到了诸神乱世的时代……

    “…….我不会成为第一个被诸神组团raid的凡人吧，恐怕光这样就能名垂青史了，但这样的荣誉我宁愿不要…….”

    当年得罪的神祗不少。反正仗着他们不可能真身下凡，下来化身又收拾不了我，我可是往死里得罪的。

    干掉他们的凡人教会，等于断绝了他们的信仰，没有陨落算他们厉害，这样的仇丢神祗里面，可比杀父之仇还大。

    至于具体哪些是仇人……我还真是记不得了，横扫的时候谁会记得自己碾压了多少，顺路干掉的真神牧师、教士也不知道多少。

    “低调一点吧…….”

    想来想去，最近还真只能当段时间的缩头乌龟了。至少在几把魔剑到手之前，还是不宜和神级角色死磕。

    嗯，只要那几把魔剑真的能够发挥作用，就是遇到神祗真身，我也没有惧怕的。

    当年牺牲一切，转世重生，若不能变得更强，那我不是犯蠢了吗。

    在理论设计之中，只有有任何一把魔剑存在，至少我可以在神祗面前保命。若是那边最危险的魔剑塑成，弑神应该难度也不大。

    “走了，走了。”

    披上头蓬，转动年龄欺诈首饰。带上假胡须，披上破旧的锁子甲，这次选择伪装的，是中年版的贫困冒险者。

    这样的中年不得志的佣兵，始终占据了冒险者中的大多数，只要小心一点。我不认为会被揭穿。

    而我本身对绝大多数侦查法术免疫，也不需要担心被人堵上。

    现在回想起来的话，若不是我在温斯顿亲王那里泄露了行踪，对方也不可能抓住的我方位，顺势进行布局。

    但一想到漫长的回归路，我就有些头大。

    不是不想驾驭飞翔坐骑在天空中翱翔，那样一天的空程，足足抵得上步行一周了，但圣安东里奥可不是小国，各种空骑来回巡视，被围上了乐子就大了。

    从沙漠中走出来倒是很顺，连走带飞，只花了一天，但接下来的旅程就明显要很艰辛。

    一想到沿路我多半无法进城，和来的路上一般要风餐露宿，我就更是郁闷，很有些直接飞过去的冲动。

    而在路上，冒险混入了一个城市，和情报贩子进行交流，得出的消息，却让人更是沮丧。

    至少在圣安东里奥大部分地区，我的嫌疑犯的帽子甩不掉了，而最扯且最让人不爽的，居然把我和圣堂教会扯到了一起，各方消息都仿若在指责圣堂教会制造了这场动乱，温斯顿亲王就是圣堂教会的狗腿子，而我是他们派出来的凶手。

    我能够理解政治宣传多半是胡扯，但胡扯到到这种地步，也算是难为那些宣传人员了。

    圣堂教会的死敌罗兰居然和圣堂教会合作？为了谋杀千里之外的某位女皇？这要多扯，才能够让人相信这么荒诞的留言。

    而随便到一个城市，都可以看到我的大头贴在悬赏栏、城门处挂了一片，也让我无奈的减少了出入城市的打算。

    找了个杂货铺，补充了食水和难吃的干粮，就再度上路了。

    我考虑了是不是找个商会车队、冒险者小队加入，混入其中总比孤身上路来的隐蔽，但考虑到普通人的“龟速”，无奈只有自己独自上路。

    每到一处，都可以看到紧张训练的民兵、到处游弋的骑士，内战的阴影似乎已经笼罩在这个国家之上。

    而等我快抵达温斯顿的地盘，接近塔罗河的时候，却得到了确定的消息。

    内战，已经在一天前爆发了。

    巴特罗公爵的骑兵团在夜幕的掩护下，袭击了边境城市图尔丁，并杀光了所有的反抗者。

    这个要塞城邦在土地上连名字都很难找到，常驻军不到两千人，这个时候却吸引了包括神祗在内的诸生目光，因为这个行动只代表一件事圣安东里奥内战全面爆发。

    而更让人觉得匪夷所思的，就是在巴特罗公爵的军士之中，出现了巨魔和炎魔的身影，昔日的死敌居然成了盟友。

    想到玛利亚也是死在恶魔手中，若她泉下有灵，恐怕已经气爆了。

    而我翻了翻地图，却发现图尔丁居然离我所在的位置并不算远，犹豫了片刻，我还是选择了绕行。

    但可惜的是，这次，却稍微晚了点，追兵已经找上门了。(未完待续。)







  



