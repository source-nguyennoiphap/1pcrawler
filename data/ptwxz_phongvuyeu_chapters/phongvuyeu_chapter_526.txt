








    一场“木灵超可爱佣兵团”的内战五分钟就结束了，我的亡灵大军中有多出了四个“精英改造亡灵”，……

    别误会，我并没有干掉他们，也没有必要干掉他们。

    我只是对其说了一下，我是在执行木灵特别颁下的任务，以亡灵法师的身份在收集情报……这群家伙就信了！

    是的，他们立马就信了，毫不犹豫的信了，然后当即勒索我说“肯定有不少正义点数吧，作为一个伟光正的圣骑士，绝对不能私吞啊，大家有福同享，我们一起做任务吧”，然后我随口应付了两句，他们就主动加入了混点数的大队。

    “这些腐肉好恶心。”

    浑身被被粘上了粉红色的肉片，加上我刻意准备的化妆技巧，如今的野蛮人阿根已经变成了一只浑身恶臭的憎恶尸怪。

    腐臭的骨干，满是恶臭的腐肉，还有穷的没穿裤子的肥胖下身，肠子已经拖在了外面，接错了的下巴，边走嘴角还在滴血，简直是一只完美的增恶啊！

    “其实，按照我的职业特性，俺应该伪装成一个优雅的血族的！在夜色的掩护下，行走于屋顶和阴影之间，用匕首收割敌对盗贼工会的刺客的性命。”

    “优雅？还有这屋顶交战的剧情怎么这么熟悉，你丫难道是说《刺客》那本传奇小说吗？”

    “嗯，当年，俺就是看了那本书，才选择成为盗贼的！”

    从那不住的点头，我越发觉得坑爹了。我在犹豫是不是要告诉他。那本小说也是我当年混稿费的产物。一路yy和龙傲天，各类逻辑错误实在不少，但由于很爽，却莫名其妙的很受欢迎，真照小说主角的生活方式去当耍帅贼，一年内死一百次都算少的。

    但看着这个憨厚的笑脸，在感叹这家伙居然现在还没挂掉真是命硬的同时，我觉得还是不要毁人三观比较好。嗯，绝对不是觉得若对方因此恼羞成怒就会很麻烦之类的。

    “梦想是成为高来高去的飞贼吗？”

    考虑这家伙的体积和重量，我觉得若他真的如小说主角那样在屋顶上跳跃前进，要考虑的并不是隐蔽性，而是那房屋的结实程度……半夜三更做什么愉快事情的时候，突然一个庞然大物击穿自家天花板，从天而降，震耳欲聋……嗯，我绝对没有尝试过，也没有卡在天花板上！

    一想到这个大块头‘也’被夹在地板和天花板中。突然觉得很带感啊！或许下次可以让他试试，最好带上红色的棉袄和大胡子。被发现的时候还可以“呵呵呵，我是圣诞公公，只有乖孩子才能收到我的礼物。啥？现在是夏天？抱歉，我走错了，那该死的地精指南针太不靠谱了，那只蠢笨的独角兽也不知道提醒我！我居然飞错了半球！”

    咳咳，貌似又走神了，言归正传，这可是我独家珍藏的增恶外貌完整礼包，正好适合强力战士使用，既拉风又实用，那壮硕的腐肉和凸出的眼球，还可以增加对女妖的魅力值哦。

    “……罗兰，等下我们私下好好谈谈，我觉得你对女妖有一些误解。”

    好吧，看来一不小心在当事人面前说了坏话，想起这前女妖之王，越来越娴熟的一喵二舔三萌叫……我是说猫科常用的一扑二咬三扫尾，我是要认真考虑一下应该如何解释了。

    咳，貌似又走神了，言归正传吧。

    在我的亡灵主题皮肤礼包的伪装之下，木灵超可爱小队，嗯，就是刚刚成立，简称木（母）爱小队的那个，如今的小队成员已经完全没有生者的模样了。

    已经提到的阿根穿上了憎恶外套，而牧师贝蒂披上斗篷召唤暗影，伪装其祭祀死神的阴影牧师，帅气的精灵吟游诗人伪装成同样帅气的血族，还给其涂上了一点血色伪装，英勇彪悍的矮人大妈……还是霸气十足的矮人大妈！

    这真的不能挂我化妆功底不足，我实在想不出，要让一个上百公斤却只有一米四不到的水桶体型，却伪装成只有腐肉和骨架的亡灵，到底哪一种比较合适，若是也伪装成憎恶的话，那么只有脑筋不正常的亡灵法师，才会制造这种微型尸怪了。

    “血族？有这种体型的血族，恐怕真血族会哭给我看吧。”

    当然，最后我还是找到点办法，给她套上一个怪脸外壳，伪装成一只体重超标的食尸鬼，虽然依旧有着矮人特有的短粗体型，但若有其他亡灵法师看到，也只会认为是矮人转换的，在转换中出了点问题。

    当然，前提……是那个大妈愿意放下自己的酒桶！有那个食尸鬼身上会背着这么大一个酒桶的！还不时偷偷的舀上两杯解解馋。

    但我知道劝说其放弃这个“行李”简直毫无可能，不是有句谚语吗“让矮人放下自己的酒杯，和让精灵们放弃自己的艺术和骄傲一般毫无可能。”

    “真是可惜了，我这些‘皮肤礼包’可都是杰作，又是不要钱的良心产物，这些家伙太不识货了。哼，迟早要找个抓么坑菜鸟的不良商人好好的坑他们一把。他们只是戴个帽子和眼睛再配上一些乱七八糟的赠品就可以当做新的皮肤礼包卖，那有我良心。”

    “别斤斤计较了，有义务帮手就不错了，别计较这么多。”

    好吧，虽然这群坑货似乎个体战力并不算太强，但至少不用我分心操控，还是可以作为添加的战力，当然，出于某些原因，我也稍微和他们拉开了一点距离。

    “内奸？亡灵还能在生者之中布下内奸！？”

    当我提出刚刚收到的信息，众人的惊诧并不是伪装的，在普通人乃至大部分冒险者的观念之中。亡灵就是一群无法交流的野兽。他们会主动攻击眼前的一切。这样的存在，又怎么会在生者之中布下奸细，或许，应该说就算为这些嗜血的亡灵当内奸，能够获得什么。

    当然，这样的观点也说明了这个冒险团的大部分人还真的只是个菜鸟，西罗帝国在人类王国布置的探子并不少，对于掌握权柄和财富的贵族来说。最大的敌人就是死亡，而要他们凭借修炼进阶延寿简直就等于杀死他们，而亡灵转换仪式大概是最容易达成“不朽”的途径

    就算不满足丑陋的亡灵之躯，也有优雅的血族作为备选，而若是还想在阳光下行走，汲取他人生命力、血祭等无底线的邪恶仪式，同样能够满足那些家伙的要求。

    而不仅仅是那些腐朽的贵族，曾经以为残疾而失去了未来的资深冒险者，被某个关卡卡了十几年的垂垂老者，或是天生属性就是黑暗、亡灵的魔法天才。都有可能从亡灵那里获得很多。

    不仅是西罗帝国如此，当年永夜的时候也是一样。每天都有生者主动来投，其中有修行黑暗、诅咒等非主流的强者，也有仅仅是“黑色反派？这样听起来很酷”的二傻子，若是前者的话，我会很开心的把其编入炮灰阵营，若是能够用活下来来证明自己的能力，也会以等价交换的方式达成他们的愿意，后者的话，一样编入炮灰营，只是在事前会进行亡灵转换……非主流就是转换成食尸鬼也是非主流，至少可以恶心一下对手。

    面对这几个小子的惊诧，我犹豫了一下，组织了一下语言，才说道。

    “能够获得什么？恐怕远比你们想象的多，比如力量和无尽的寿命什么的，只要愿意付出些许代价，远比一步步修行来的快。毕竟在很多时候，若是不计后果的话，邪道远比正道来的快。”

    看着面前若有所思的众人，我也没有多说什么，现实永远是残酷的，比起一味的相信其他冒险者都是自己的同伴，不如省下这“学费”，提前做出必要的防备。

    很快，我就从哪个新的下仆骷髅法师库咯那里得到了我期望的讯息。

    “一个披着斗篷的小个子，带着墨绿色的蛇形匕首，身上有黑暗气息？左脚有点跛，似乎还有伤？”

    我把询问的目光投向了自己的“队友”，毕竟比起初来乍到的我，他们应该比较熟悉这里的冒险者。

    然后有人望天，有人查看自己的武器，就是没有人回话，显然这群家伙也是不可靠的，或者，他们根本就对其他的冒险者没有兴趣，也没有收集其他冒险者的情报。

    无奈的摇了摇头，接过了库咯递过来的了那封信，从某种意义上，这还是真实很让人讽刺的，亡者在生者中安插的间谍，却错把重要的情报交给生者在亡者中安插的间谍。

    黄色的信封很轻，看似普通的信函被红色的蜜蜡封印住了，似乎一撕就能打开，但从其中隐隐约约传来的魔法波动来看，这其中还有一个常见的小机关，若接信人不知道开信的正常方法而随随便便打开的话，这信函就会自毁，而且布置下这个小机关的人也会知道。

    但对我来说，这样的小东西毫无意义。

    轻轻一点，一点点小冰块就封住了封条处，带来永眠的绝对零度之冰，让这机关也永远陷入了长眠。

    撕开信封，信纸上却无聊的数字账本，看来是常见的数字密码，无非要对照各种暗号本进行解读，虽然花点时间大概能够想办法解读，我隐隐约约也学过该如何解读，但……

    “我记得你的暗号解读和基础数学都从没及格过吧。”

    是的，即使在当年的云端之塔，也逃不掉那让人烦躁的数字，工程学和元素魔法、法术模型的构建。

    但……什么叫做基础数学！你丫的基础数学居然有微积分和立体几何，就不知道我前世就是为了不学那讨厌的高数才选择法律专业的吗！

    “……作为一个文科生，数学什么的太讨厌了，异世界居然也有高数就更讨厌了！”

    不过，作为一个各门功课长期处于及格、不及格边缘的不良学生，实际上我却达成了不少同类的梦想。

    “海洛伊丝老师，都靠你了！我只能指望你了。”

    “……你就这个时候会想到我。”

    是的，我虽然基础学科不及格，但我随身带着老爷爷……老奶奶（猫扑抗议中）……老师！

    “……木灵们计划扩展……梦境之森的领域，预期……预期在五月的第二个周日，就是发信的第三天启动计划……”

    海洛伊丝的翻译倒是越来娴熟，显然已经找出了暗号的规律，但我却越听越心凉，对方知道了机密程度很高，显然内奸至少混进了冒险者高层。

    但也正是因为这样的严峻现实，我隐隐约约已经有了头绪，或许，还可以将计就计！(未完待续请搜索飄天文學，小说更好更新更快!

    ps：比卡文更悲剧的，大概卡文的同时断网了，但现在总算恢复了.....







  



