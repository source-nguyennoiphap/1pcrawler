








    那种在战场上一言不发就开大招的，有两种人。

    一种就是主动作死的新手，巴不得所有的目光投向自己，然后就是理所当然的瞬间扑街。

    一种，就是装了逼就跑的老鸟…….我是说有自信镇住全场，敢于表现的实力派。

    圣光的黎明照拂了全场，这是专门为残酷的圣战准备的战略兵器，集范围大、增幅效果大、持续时间长、消耗少等诸多优点，虽然有只能针对亡灵和恶魔的弱点，但相比较它的优势来说，是完全可以接受的，唯一的缺陷，大概……

    “…….就是稍微有点嘲讽吧。”

    首先来到面前的，是一只骨龙和两个小队的石像鬼，拜尔的龙骑士和狮鹫骑士并没有成功拦截下全部的亡灵空军。

    但这种等级的对手，若是以前，我还需要一点时间，现在……

    “圣光啊。”

    低声念诵圣光的赞美词，半空中的黎明之剑散发出更加耀眼的圣光，但这次，却是集中的光束。

    一道、两道、十道、百道，半空中是金色的光束的痕迹，那些亡灵的飞翔单位只到自己被击毁之后，才发现自己已经被攻击。

    虽然我必须维持圣剑，但并不代表我毫无防备。

    光在我手中牵引，每一次点击、移动，一道光束就射向了目标。

    一道道光辉的痕迹见证了攻击的效果，每一道光痕的目标，都是一只高阶飞行亡灵。

    “1至71！预判，瞄准，命中。”

    在黎明圣剑的圣光灯塔（炮塔）模式下，战斗变成了简简单单的点击游戏，攻击速度超越了反应速度，一个个被点名的飞行亡灵直接坠入地上。

    一道道光束形成了光网，天空中不断发生的金色爆炸，是最美丽的火花。在这片致命的火力网下，根本没有通过的空间，

    这恐怕是我经历过的最轻松的战斗，一瞬间就击落了数百高阶亡灵。但虽然战果喜人，我却没有追击到底的打算。

    “耗能实在太狠了……”

    短短半分钟，我就使用了三百十二条光束牵引，结果就是黎明圣剑的圣光直接被消耗三分之一，再来上两次。就可以熄火了。

    黎明圣剑从某种意义上来说，就是我的身体的一部分，一瞬间就挥霍了三分之一以上的圣光之力，我也感觉有些虚。

    但我的目标已经达成了。

    第一波亡灵突袭全部被击杀，而有了这个时间差，人类方也缓过气了。

    荆棘的丛林在我面前布满，一个个食人花战士从中爬了出来，而皇家龙骑兵也跟着龙骑士抵在我的面前。

    缓缓升起的太阳驱散了一切黑暗，对混沌的理解，对圣光的追寻。让我塑造了这把混沌天敌之剑。

    “纯净的圣光啊，请赐予我力量，驱散那片黑暗。”

    半跪下，虔诚的祈祷，以自我催眠的方式加强和圣光的链接、契合，周遭的元素潮汐开始滚动，疯狂的聚集更多的光。

    而半空中的圣光太阳，再度开始充能闪耀。

    “…….这太夸张了吧。这圣光储备和圣光亲和力，简直完爆教皇啊。”

    当年，被称为圣光的私生子是有理由的。那本来就不科学的圣光亲和力，再仲裁者的天使之力的加持之下，进化到了极限。

    “圣光啊。这怎么可能…….”

    “亵渎者。”

    这个时候，就算是最虔诚的圣光之神信徒。也会质疑自己的信仰。

    背后的光之翼被展开，那是战争天使的光之羽翼，无数的光点以肉眼可见的程度在羽翼上聚集，这召集圣光的元素亲和力，是最高阶的天使都望尘莫及的。

    被圣堂教会当做邪恶异端的异教徒，居然是如此强大的圣光使用者。这“虔诚的信仰圣光之主，主会赐予虔诚信徒力量”的教义，变成了笑话。

    “这使用圣光的方式，怎么这么像魔法……”

    “别瞎扯了，你想进异端审判所吗？””

    “不会错的，这就是在用操作元素的方式操纵圣光。”

    “没有祈祷、没有宗教赞美诗，甚至没有链接神力……“

    **师的议论就在耳边，在我近距离演示自己的圣光使用方式之后，从今天开始，他们大概会换个角度来对待圣光。

    而这，也是我希望看到的。

    雨突然停下来了，天空之中的乌云开始扭曲，似乎在畏缩光的存在。

    “罗兰！”

    尖叫的乌云变成了一张巨大的鬼脸，战场在他的阴影之下，而那张面孔，却让我觉得有些熟悉。

    “修普诺斯？”

    资料中可没有这些，我可以从中感觉到极其高深的水元素魔法造诣，看来，这位亡灵大帝还藏了不少东西，之后又有新的谈资了。

    至于修普诺斯的仇怨？恨我的还少了吗。

    和资料中不一样的，却是修普诺斯并没有失去理智的冲过来，当战场上的大雨、黑暗优势被驱散之后，这几万亡灵根本不可能是人类联军的对手。

    最后瞪视了我一眼，乌云边收缩，边向着远方飘去，修普诺斯撤退了。

    高阶亡灵也毫不犹豫的开始撤退，那些低阶炮灰却都甩在战场上没有人管，而且，被逼出了最后潜力的它们，会不惜一切代价掩护智慧亡灵撤退。

    这就是亡灵天灾总是卷土重来的原因，不斩杀那些高阶亡灵和亡灵领主，消灭一些炮灰毫无意义，而亡灵军团百分之九十以上的士兵都只是炮灰。

    从战果来看，人类方取得了辉煌的胜利……但从众人满是严肃的表情来看，没有人会认为这会是一场胜利。

    亡灵方用人类联军十分之一不到的兵力，就造成了大量的损伤，战场上，到处都是伤员的生硬和呼救。

    这当头一棒，好好记的教训了骄傲的人类，亡灵天灾绝对不是好惹的，即使实力占优，这这绝对不是一场秀或郊游，一不小心依旧会全军覆灭。

    而我本人，却也被**师们围住了，我也猜到了他们想问什么。

    于是，我掏出了一本手写稿，这是下个月将由小妖精出版问世的书籍，而它的名字和内容，却注定引起整个世界的震动。

    《是时间走下神坛了论圣光的本质。》

    或许，这将是我最后一次挖圣堂教会的墙角，以后一旦圣光之主走下了神坛，自然也没有必要继续挖了。(未完待续。)







  



