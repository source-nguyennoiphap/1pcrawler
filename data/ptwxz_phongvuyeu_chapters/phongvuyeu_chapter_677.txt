








    “真神陨落了，森林守护神克莱尔拉丁陨落了。半神普雷斯科特大人战死了！”

    北方的精灵王国的守护神就是森林之神，此时他的陨落，给了所有精灵天塌一般的感觉。

    “这绝不可能！神明是不灭的，他们怎么可能陨落！伟大的森林受害者克莱尔拉丁大人怎么可能陨落！”

    这大概是当消息传开后，所有北地精灵的第一感觉，千百年来，精灵真神庇仿他们，让他们远离北地的主旋律——残酷的生存竞争。

    温暖的环境，远离战场的和平，他们已经习惯了神明庇护的生活，把其视作了仿若太阳在东方升起一般的真理，现在太阳突然不再升起，自然惊慌失措到极点。

    “我们完了，北方精灵诸国都完了。

    那些首先失去理智的恐慌散播者被精灵战士直接控制住了，但恐惧和不安早已经蔓延到整个精灵北境，流言可以否认，但有些东西是无法否认的。

    由于森林守护神死去，没有了他的神力的庇护，梦境一般美好的精灵王国走向了现实，回到了这片天地的自然循环之中，大自然开始展示他一视同仁的残酷了！

    此时，森林中小溪开始冻结，大片的树木开始枯死，不属于北地的森林正在走向死亡，而最外缘的防护林在短短数分钟完成了数十年的蜕变，他们转眼之间就掉完了所有的叶子，被神力维持的不朽转瞬走到尽头。枯萎、死亡就在眼前。

    没有森林守护神用森林竖起的隔绝屏障。精灵王国和北地的高低温非自然落差。让自然平衡发起了激烈的反扑。

    在精灵那包含诗意的形容中，暴起的北风化作残暴的季节之神，一个转身，就悄无声息的把与世隔绝的精灵乐园拉回了残酷的北地。

    当寒风猛地灌入原本四季如春的精灵之地的时候，猛地打了个寒颤的精灵这才知道，此时，已经入冬了，这个由暴怒大自然为精灵族特制的残酷冬天可不好过。

    而当精灵们陷入恐慌的同时。他们的“同族”却陷入了莫名的狂喜之中，越战越勇不足以形容他们的激情，在盛大的血祭之中，新生的暗精灵毒素之神在蜘后的蜘蛛神徽旁增加了一个青绿色的毒蛇徽记，从此，黑暗精灵神系不在是孤家寡人的罗丝一人神系，野心勃勃的罗丝已经开始考虑新生精灵神系的排序了。

    在那黑色八爪蜘蛛的旗帜下，黑暗精灵的军团已经开始集结，被抽调了主力的精灵守军根本不是他们的对手，他们眼睁睁看着入侵者在自己的家园建设起来连绵的营地和攻城器械。黑暗精灵根本没有掩饰他们打算常驻此地的打算。

    流苏王国的皇陵已经被挖开，所有的地下世界出口全部敞开。每天都有无数的黑暗精灵从地下世界赶出来，他们就仿若闻到了鲜血美味的嗜血巨鲨一般，迫不及待的品尝越发美味的猎物。

    和地表精灵有世仇的暗精灵并没有如世人预料的展开种族屠杀，但那气定神闲的备战，那视自己为无物的态度，却更让那些那依旧对胜利保有期盼的精灵绝望。

    “他们在等，他们在等我们的远征军团回来。他们有必胜的把握，他们有罗丝的庇护，没有其他精灵神明的参与，我们的军团毫无胜算。崇高的安索罗到底在想什么？为什么放任森林之子被恶毒的蛛后杀死！为什么我们的祈祷得不到一丝回应。”

    精灵诸神那有时间回应那些北地精灵的祷告，人类诸神已经察觉了他们的背叛，他们现在自身难保。

    从某种意义上，精灵族的确缺乏担任秩序侧顶梁柱的担当，和越来越把自己的守护神当吉祥物的人类大国相比，精灵族对于神明的依赖度太高，他们甚至把战争的第一要素视作神明的庇护。

    “不，他们也在等候自己的援军，每一天黑暗精灵的数目都在增多，该死！那群黑皮肤婊子到底有多少？”

    黑暗的地下世界到底有多少黑暗精灵？恐怕就是罗丝自己也不知道，她用自己力量让黑暗精灵有了远超普通精灵的生育能力，却也被戴上了腐化堕落的帽子，被放逐到了地下世界。

    “该死的安索罗！你说我堕.落了？好，我就堕.落给你看，我就让你看看什么是真正的邪恶！”

    从某种意义上，罗丝的自暴自弃导致了整个黑暗精灵真正的堕入黑暗，相互残杀、背叛、欺诈成了一个种族的主旋律，就是地下的魔鬼也不敢说自己比黑暗精灵更善于的欺诈，但至少，这个由上位精灵集体堕.落的种族的确强大，当他们摆脱了繁殖力限制的魔咒后，他们的底蕴远比世人预期的强大。

    没有那一个种族是天生邪恶的，一个人的善恶更多的是由后天的经历决定的，罗丝的剧毒墨水并没有涂黑整个种族，月神庇护下的灰精灵，离群索居的暗精灵游侠，公开不听从罗丝召唤的地下城主，就是黑暗精灵内部也有无数的不同信仰和理念，罗丝早就不能掌控整个黑暗精灵族了。

    但看看现在的暗精灵营地吧，最大的旗帜下的连绵营地的确属于罗丝，月光标记、血刃标记、寒冰双剑徽记等等零落其中，无数大大小小的零散营地却代表着无数的暗精灵社会背叛者，他们加起来所占据的面积远大于罗丝的势力，也从一个侧面证明这个种族的分裂程度之高。

    而此时，这群在地下为了各种理由彼此仇杀的黑皮肤精灵。却为了同一个目标。走到了一起。

    “过去针对先祖的审判毫无公证可言。就算先辈有错，但我们这些后辈也有生活在有阳光的蓝天下的权利，我们也有在舒适大地游弋的自由，我们要用事实证明，我们黑暗精灵，才是所有精灵种族中最优秀者，最应登上王位的上位精灵！”

    在这片旗帜下，原本分歧严重的黑暗精灵族临时团结起来。在罗丝耗费神力召唤的魔力黑雾之下，他们躲避了阳光的灼伤，在黑影中耐心的磨砺自己的匕首，耐心等候那命中注定的同族间的残酷厮杀。

    而提前入侵的黑暗精灵，自然也就和地下世界的其他的势力开始脱离关系，他们现在的目标，只是有自己的一片蓝天。

    “先有北地这块就够了，这里有精灵之泉，有森林，我们可以在这里重建属于我们的王国。这里有我们想要的一切！”

    从某种意义上，只要圣战的战鼓再度敲响。在地下世界磨砺毒牙的黑暗精灵和罗丝的回归只是时间问题，但罗兰和艾耶的邀约信函，却给了她最好的介入时间和理由。

    此时，无数的帐篷扎在城下，黑肤的精灵渴望着复仇的愉悦和血腥的甘酿，以背叛为特性的黑暗精灵们居然在同一个目标下团结一致了。

    而对于常年居住在此的北方精灵来说，这自然就是噩梦了。

    站在首都城下，刚刚归来的国王亚特里恩茫然的看着眼前绝望的景色，这就是他牺牲自己女儿换来的吗？这就是精灵诸神为其承诺的精灵盛世吗？

    “我早该料到，在圣战中首先出头，必然会成炮灰。”

    此时，后悔不已的他不由得想起了那个人对自己做出的评价。

    “你啊，什么都计较得失，算的太多，实际上算的都是些蝇头小利，大势却从没有看清。你这样天天盘算得失，迟早要把自己算进去。”

    “我没错！我绝对没有错！”

    但此时，战局已经容不得他犹豫自责了，其他三国的联军已经对其抱怨不少了，他们中的不少都希望能够解散联军，带着各自的军队回去镇守自己的本国。

    “愚蠢，比无用的人类更加短视，你以为那些黑皮肤的婊子打下了流苏王国，就不会去其他的国家吗？我们流苏那点地那够她们分，她们要的是整个北地精灵国度！”

    一路回归，流苏王国的惨状反而让其他精灵王国的军队的离心更盛，若不能尽快整合整个精灵势力，这场战争不用打就已经输了。

    “给我请两位将军和卡扎路王子赴宴，就说我有私密的话要说......让阿尔文带着月刃精锐做好准备，听我的命令。”

    对着自己满脸震惊的心腹，国王咬牙做出了抹脖子的手势，他已经下定了决心，为了这守护自己的王国，他愿意付出一切。

    --------------

    当这个冬季来临之时，谁也没有料到，牵扯到的势力会如此之多，精灵战场、红枫防线、安图恩元素战场，三个主战场构成了这场圣战最瞩目的开幕战。

    三个战场相互影响，相互交缠，到了这种地步，任何一个战场的失利，都有可能导致雪崩式的溃败。

    “属于背叛者的战争”，是历史后世为其定下的别称，而对于当代的当事人来说，后世的评价从来无关紧要，眼下这种敌我难分的恼人形势，却是最让人头痛的。

    两大阵营之间的相互背叛，同族之间的相互欺诈已经成了日常，今天的敌人，明天的朋友，昨日的战友，明天的死敌。

    这种反复，连崇高的真神都无法幸免，当圣战重启之时，那些身份**的存在，在各位大佬的暗示或威逼下。再也不能保持左右逢源的状态。也必须用实际行动选边站了。

    从某种意义上。也就是这个关键时间点，才让秩序侧大佬之一的艾耶的信函更具说服力，有了让罗丝重新选边站的威力。

    而此时，作为上次圣战最大的失败者之一，没有神明庇护的兽人，却企图在这场战争中翻盘，他们之前的突袭的确做的很出色，但现在。却遇到了一些小麻烦。

    “我们的内线全部被拔掉了吗？怎么可能，我记得总共有二千三百七十六组，其中七成是外表无法分辨的人类混血儿，他们之间都互不认识，怎么可能都被挖出来。不少被深埋的种子都埋了一两百年三代人，我们应该一直没有启动，怎么可能被挖出来。”

    作为占据了主动优势的攻城方，现任的兽人皇埃蒙.血斧很是头痛，从开战至今，情报系统的残缺。是始终困扰兽人的大问题。

    战争首要的就是情报，托司璐威尔王国的“和平通商”政策的福。百多年的相互来往，已经足够早有野心的兽人部落把这个王国渗透成筛子。

    但战争真正开始后，那些埋伏下的“种子”却没有一个发芽。

    那个正在汇报情况的龟族兽人满头大汗，作为情报系统的总管，他也是一个部落的酋长，但在这群愤怒的大酋长面前，他只感觉到自己的生命遭到了威胁。

    也难怪诸位酋长如此愤怒了，养兵千日用兵一时，原本战争才是探子最活跃的时间段，现在本应是他大显身手的时候，但开战至今，他简直是交了白卷。

    “红枫城里面应该有十三组，但.....全部都挖出来了，挂在东城门的那些就是了。而且，我.....”

    “西门的那个女守将你告诉我们是个花瓶，我们集中战力去攻打西门，结果损失惨重，损失的空骑可是我们这么多年的积累，那些魔剑手和战争机械你们居然说不知道？浮空战舰那么大的块头你们告诉我没有看到？一群废物，我养你们做什么！”

    在这个关键时刻，再度听闻情报系统的毫无建树和错误情报，暴怒的兽人王在怒吼中举起了战斧，他打算亲自出手，斩下这群废物的头颅。

    “等下，那些探子是怎么泄漏的？那些有明显我族特征的就不提了，他们只是面上的幌子，但那些收买的人类间谍和混血儿是怎么暴露的，没有道理啊，莫非......”

    和暴怒的鹰王相比，熊人隆德稳重的多，但从他一不小心捏碎手上的玉石来看，他也并不平静，那只比斯巨兽的折损，这永远无法弥补的损失，他已经心痛的数夜睡不安眠了。

    此时，说到最后的“莫非”之时，他却环顾了四周，怀疑的目光在各位酋长脸上停留。

    言语中隐含的意思其实很明显了，这样深埋的情报系统全部被挖出来，只有可能是出了奸细，还是极其高层的奸细。

    “......不，不是内奸。”

    即使斧头已经到了头顶上，被吓的满头大汗，有些该说的话，情报主管还是要说出来。

    “和奸细无关，实际上是我们自己的后勤出了点问题，才导致了所有探子的暴露。”

    “嗯？”

    什么问题能够让所有的情报探子全部暴露？在座的酋长们都满脸不信。

    “那件事若追究起来，大概要从三十年前开始说了，由于生性淳朴，我们的族人在人类世界谋生相当困难，被突然派到异国他乡，那些探子并不擅长经营，不少探子都在短短三个月被骗光了自己身上的财物，在饥寒交迫下返回了高原。”

    众人点了点头，兽人不善经营是众所周知的事实，别说这些大字不识的探子了，不少“睿智”的巫医和萨满都有在人类社会被骗的精光的经历。

    对于依旧保有以物换物的古老习俗的兽人来说，人类世界太过复杂，就算不遇到骗子。也多半很快就会花光所有的盘缠。最终只能无奈返乡。

    “我们一直都不知道反攻日到底是何时。若这么不断派出新的探子，这情报投入简直就是无底洞，我的族人并不富裕，根本支付不起，最终，一个偶然，却让我们获得了传说中的秘宝。”

    “秘宝？”

    “嗯，那是一次偶然的巫医试验。吾族的巫医一不小心把占卜用的草药和烹饪的香料容混了，结果在一系列偶然的巧合下，却做出了举世闻名的秘宝——奥良烤巨魔肉！”

    闻言，酋长们都吞了口口水，他们也都品尝过这种红色的美味烤肉，的确是让人回味无穷。

    “诸位大人也是知道，然后我们以此为基础，开放出了一系列美味烤肉、肉堡，奥良烤肉店也随之闻名于世。哼，那该死的人类居然偷窃我们的配方。搞了一个麦式汉堡店，到处和我们打对战。简直是无耻之尤！”

    “这和我们的探子被挖出来有什么关系？”埃蒙也有些惊讶了，这不是再说情报系统吗？怎么扯到了快餐店大战。

    “您也是知道，我们的探子都不擅长经营，而奥里昂烤肉店却是少数稳赚不赔的买卖，结果.....结果，所有的探子都开了奥里昂烤肉店！然后挖出一个后，却全部被挖出来了！”

    好吧，事实上，由于奥里昂烤肉店实在太赚，甚至有兽人主动去当探子，用开店的方式到人类的花花世界享福

    “我x！这不是找死吗！”

    ”居然蠢到这种地步！谁下的命令！那个白痴做出了这种蠢事。我要把他吊死在旗杆上！”

    兽人王埃蒙的怒吼在帐篷中激烈回荡，尖锐的嗓音让门外的守卫都吓得一哆嗦。

    看到埃蒙如此生气，那个主管更是满头大汗了，

    “是您......”

    那回答的声音不大，但在场的可都不是一般人，众位酋长先是一愣，接着却出现了诡异的沉默，只有大大咧咧的埃蒙依旧追问道。

    “啥？”

    “是您啊，我黑岩部落本就是您血斧部落的下属部落，而整个情报、侦探工作一向由擅长飞翔的鹰族部落总管的，您又是所有鹰人的共主，我一直都是您麾下的忠臣。在您刚刚继位的时候，您说的‘谁知道那些无用的探子什么时候用上，反正不可能是我这辈了，不如抓紧机会多赚点外汇，让所有的族人都做最赚钱的买卖！我们要让奥里昂烤肉店遍布整个世界！我们要成为世界之王！’”

    埃蒙努力的回忆，貌似三四十年前，自己刚刚继位的时候，的确说过类似的话，还作为最明智的发言，被记录在部落宝典中。

    而作为血斧部落酋长，他最得意的并不是武技和战略，而是带领族人发家致富的本事，而正是因为血斧部落的强盛，才会被选做和土元素之神合作的代理人。

    “其实也不能怪您，只是人类太狡猾了。您看，我们现在才知道，原来那些麦式汉堡，居然都是圣安东里奥派出的探子啊。我们被抓的族人，都是那些带着红色假发的小丑举报的！”

    说到苦痛处，那龟人一把鼻涕一把泪。

    “就是就是那些唱着蓝蓝路的吉祥物！他们还用烹饪食材打我们，用恶心的红色假发丢我们。汉堡里面加石头太卑鄙了，兄弟们太惨了啊，我们的吉祥物白胡子兽人老头，拿着巨魔骨头拐杖根本打不过。现场简直是惨不忍睹啊。”

    ‘白胡子兽人老头？吉祥物？这不是找死吗？”

    某位酋长，终于指出了另外一个明显的漏洞，这下，轮到到某位熊人酋长满头大汗了，他也想起二十年前自己刚刚继位的时候做出的糊涂事。

    情报主管瞄了瞄，反正说不出口就是一个死字，说出来说不定还有可能活，他咬牙还是说出来了。

    “那是某位大人纪念他逝去的祖父，提出的额外要求......白胡子象征睿智，拐棍象征不服输的精神，白衣白裙象征萨满文明的步伐，他说，要让人类也能够理解兽人粗犷文化的美丽！”

    好吧。说这话的时候，情报总管一直盯着某个熊人，意思很明显了，这下，谁也别怪谁吧。

    “咳，我建议，再议。其实没有情报也是可以打的。”故作镇定的熊人酋长隆德如此说道。

    “嗯，赞同。我们兽人战力天下第一，根本没有必要学软弱的人类搞什么情报探子。”稍微松了一口气的兽人皇爱蒙如此说道。

    最强的两位大佬一唱一和，其他的酋长还能说些什么，点头吧。

    “通告所有的兽人，从今天起，麦氏汉堡就是我们兽人的死敌！奥里昂烤肉店才是我们的骄傲！从今天起，你就不是我们的情报主管，我们委任你为烤肉经济特别顾问，总管整个兽人联合的烤肉经济！”

    闻言，那个情报总管大喜若望。

    “太好了，这才是我的天职！我可是奥里昂烤肉联合理事会的理事长和特级厨师，交给我绝对没有问题，这比烦人的情报工作好多了。您可真是太会选人用人了！”

    听到这回答，除了被奉承的很高兴，依旧笑的很得意的埃蒙大酋长，众位酋长脸色都有些奇怪。

    “不对吧，从哪里开始就不对了吧，这一开始就委派错了人吧！你让一个厨师去当间谍头子，不出问题才怪吧！现在我们在打战，有必要去搞烤肉店连锁吗？你是不是从一开始就搞错了吧，你用间谍当厨师就算了，别再用厨师去当间谍啊！”(未完待续。。)







  



