








    “吾乃死神艾耶，轮回的监督者，秩序的守护者，自此圣战开启之时，吾宣告，吾等将向混沌侧乃至所有的异界入侵者宣战！”

    简简单单的宣言，却震惊了所有的生命，不，不仅是普通人，甚至连真神、恶魔都为这古老神祗那语气平淡的突然宣言而震惊。````

    这或许只是一次简单的宣战，但其中代表的潜在意义和政治意味，却让所有人必须深思。

    宣战？为什么要宣战？以前历代圣战不是这么稀里糊涂的就打了吗？

    为什么会是死神艾耶宣战？按照秩序侧的现状，即使一定要宣战，也应该是秩序侧的顶梁柱圣光之神来宣战。

    其他的主神还没有发言，为什么会是这么多次圣战都飘然事外的艾耶跳出来，他到底打算做什么，他就不担心圣光之神不满吗？

    对了，他用的还是“吾等”，也就是说除了他还有谁！难道是诸神派系斗争吗？难道秩序侧的真神已经开始分裂了吗？

    仅仅只是第一句宣言，敏锐者已经发现了风雨欲来的眉头，但这其中若是深究下来，都是让人不敢细想的恐惧。

    而我却点了点头，这也是我预期的。

    “战而不宣？斗而不破？表面上和睦相处，私底下各干各的？”

    “是的，我们没必要和圣光之神及他的从神直接闹出什么事情来，我们需要的，只是抢夺圣战的话语权。唯我正义。得道者多助！在一片混乱之中，喉咙最大的才是正义，有发言权的才有可能是正义者。既然必然是邪恶被正义打到的剧本，我们将要想方设法成为代表正义和道德的一方。”

    而这宣战通告，就是抢夺舆论最高点的第一步，从表面了艾耶势力的志向和野心，这看似毫无意义的宣战通告，却是以圣战为借口，竖起艾耶神系的大旗，一个与圣光之神不同的秩序侧势力就此诞生。

    嗯。艾耶方表面上的确依然是秩序侧。但内在……都摆明了分裂对扛了，还秩序个啥啊！就算不算到混沌侧，至少也是中立侧。

    但若仅仅是举起大旗，是绝对不够的。没有人愿意登上随时会被倾覆的危船。

    在圣光体系超强的实力面前。所有人都知道该如何站队。即使由于圣战的原因内战不会短时间内爆发，但当我们摆明了和圣光之神构建的体系不和谐的时候，之后若圣光之神赢得了圣战。大清算简直是肯定的。

    “所以，我们就要借用这次圣战的机会，不仅要把主动权握在手中，更要壮大自己的势力，至少要和圣光体系差距不大。”

    这话说的似乎有些矛盾，船不够大而沉稳吸引不了客人，没有客人也凑不够买大船的钱，但我知道，却有一个办法可以打破这个矛盾。

    “你听说过安利吗？没有？太好了。咳，不对，我们没安利那么过分，我们也不是传x，我们至少是直销。好吧，简单地说，我们所有要做的，只是让客人相信我们，并花钱我们购置船的各式零件，就相当于集资买船吧，等他们有了付出和股份后，等他们和我们利益相共的时候，自然就下不了船了。”

    而具体要如何做？实际上很简单，我这里就有现成的例子——坑爹系统。

    论起拉人入坑，谁比得上这坑了我几百年的坑爹货，我们要做的，只是给每人一个“系统”，然后作为“主神”的中立侧真神就自然有了发言权，然后所有人就会为其而战，然后又付出后回报，逐渐成长，这就是所谓的“龙傲天养成计划。”

    是的，随着战争进行逐渐成长，“历史”上这次圣战也注定了是最后一次圣战，水平线不断上升的敌人最终让仿若开了外挂的玩家都跟不上成长的速度，最终倒在了无尽的征伐途中。

    但我回顾那段“历史”，却觉得是理所当然的，在相当长一段时间内，这些外域的来客，对这个世界并没有归属，他们的从事全部依靠自己的利益来，或许今天为秩序的荣耀而战，明天就为某个大恶魔承诺的华丽史诗长剑背弃了阵营，中途把自己转化成恶魔和亡灵的更数之不尽。

    “没有信念的刀刃毫无意义，没有指引的力量只会伤害自己。”

    而原住民中却是另外一幅摸样，他们有自己的族人和家园，立场倒是坚定了，但出生就决定了立场，牵扯的太深，却很难拔出来，他们受到国家和种族的限制，仅仅只是各种内战都打了不知多久，损失的英才和资源无以计数，等他们知道谁才是真正的敌人的时候，已经晚了。

    而且，在各种条条框框之内，他们的成长速度也受到了限制，无法跟上越来越强的对手，越到后期越是乏力。

    这个真实的世界可没有毫无立场可言的玩家，原住民可是我们唯一的期望，但仅仅是现在的他们又不够的。

    “所以，我们要给予他们剑刃所指的方向，给予他们成长的捷径，给他们跳出自己狭隘立场的可能。”

    这是我在报告提及的战略目标，而只要完成这些，他们也会化作我们的利剑，就算不计算那虔诚的信仰所转化的真神之力，我们手中掌握的力量自然也会不不断壮大。

    匪夷所思？无法操作？仿若系统对我做的一样就够了，虽然世界的确没有第二个系统了，但我们有真神啊，系统做的到的，和艾希世界基础规则相连的真神难道做不到？

    “吾乃命运之神卡提罗，命运的编织者，秩序的守护者，吾宣告，吾将向混沌侧乃至所有的异界入侵者宣战！”

    当那金色的卷轴被打开，卡提罗的宣言在无尽大地回荡。但在摆明了立场之后，才是真正重要的戏肉。

    “吾乃月光女神派翠西亚，黑夜的守护者，秩序的守护者，吾宣告，吾将向混沌侧乃至所有的异界入侵者宣战！”

    又是一个银色的卷轴被撕开，又是一个强大的神祗做出了宣言，柔和的女声也做出了铿锵的战争宣言。

    “吾等将担负守卫监督之职，异位面入侵者和混沌的恶徒，都将被吾等标示！所有为了正义和秩序而战的勇者。将获得秩序诸神的嘉奖！”

    一金、一银两个神力卷轴化作纯粹的神力。混合在一起的两股神力化作神力的禁咒，成为了直抵天际的光柱，而下一刻，即使在依旧是白天的地方。也落下了无尽月光。

    “天黑了！！”

    “银河…..怎么可能！”

    在两位强大神力的神祗的联合禁咒之下。整个世界被夜幕掩盖。美丽的银河出现在所有人面前，在也没有人敢无视这样的宣言了。

    在命运之神的殿堂之上，卡特罗解开了已经被编织好的命运之网。失去控制的命运之网崩坏成一颗颗绳结，绳结再被解散成为一根根命运之线，所有的命运被纠缠在一起，所有的命运变的不可知，在这一刻起，再也没有人能够使用预言类魔法了，所有人的未来都变得不可预测。

    而在失去了对命运之线掌控的那一刻起，卡特罗身上的神力就极度骤减，直接从强大神力降成中等神力，甚至还在下降！

    而在月光女神的神域，同样的事情也在发生，哪里已经是星光汇聚之所。

    而大部分神体都化作了月光、星光的月光女士，此时也是最虚弱的时候了，在短短一刻后，她也成为了中等神力。

    千万年的积累付诸东流，也和圣光之神近乎反目，两位真神付出如此大的代价，自然期望得到的更多，他们失去的神力交织在一起，开始编织笼罩整个世界的星之网，他们将化身无所不在的监视者，感知这个世界。

    一颗一颗流星划过天境，无尽的繁星们疯狂的闪烁，所有智慧生物都感觉自己仿若被什么注视。

    随着神力扩散的，是无数细细的丝线，而那些灰色的星粉却落在了森林外面正在进入这个位面的亡灵身上，一颗颗代表灾难的红色凶星徽记被印在他们的身上，而在主位面的居民的额尖，则是正相反的蓝色星光徽记。

    这命运之力和月光构成的星光之网是永续型禁咒，从今以后，所有进出这个位面都会触及星光之网，而不经允许的进入位面的存在都会被印上凶星徽印，它并没有什么攻击能力，唯一的作用，大概就是标示那些非秩序侧的住民，并在各大教会的圣殿的地图上指示出来。

    在两位真神付出巨大的代价后，星光之网被构建完毕，从此，他们的牧师和圣殿骑士，将获得通过星光之网使用各类指引、探索类神术的能力，在无所不在的星光之网下，没有谁能够逃避侦查。

    这也是我在报告首页中所提及的要点。

    “首先，我们要做的，就是指出谁才是位面的敌人，谁才是位面的朋友。这是最先要做的，也是最重要的的一环。嗯，最好能够用神力禁咒予以永固，虽然或许付出会很多，但这样才能获得高的权威性的，也能够让人信任其中立性和真实性。对了，不要忘记留下操控的后门，等所有人都信任我们所指引的目标是真正的敌人的时候……..呵呵。”

    此时，就是艾耶本人也松了一口气，虽然星光之网的编织大概还要持续很多天，但能够阻止也就是当下了，很快星光之网就会成为这个时间基本规则的一部分，等那些主神醒悟后，除了击杀两位真神之外，已经无法阻止星光之网的存在。

    而想找两位将通过休眠恢复神力的真神的麻烦…….先问过他艾耶本人再说吧！从今天起，三位真神的神域将链接在一起，从上古时代开始，艾耶就已经不惧任何神明的挑战。

    而此时，在梦境之森之中，欣赏那璀璨的星光，看着艾耶给我准备的台词，我也差不多知道要做什么了，于是，我开口了。

    “吾乃律法和契约之神无眠者，律法的监督者，秩序的守护者，吾宣告，吾将向混沌侧乃至所有的异界入侵者宣战！”

    “吾乃工匠和侏儒的守护神贝尼，新生技艺的发现者和研究者，秩序的守护者，吾宣告，吾将向混沌侧乃至所有的异界入侵者宣战！”

    “吾乃财富女神贝雅娜，财富的持有者，秩序的守护者，吾宣告，吾将向混沌侧乃至所有的异界入侵者宣战！”

    一位主神，两位强大神力，三位中等神力，仅仅是现在艾耶拿出的实力就让任何势力不敢小瞧了，好吧，现在大概应该一位主神，五位中等神力了，但这绝对还不是艾耶势力的全部，恐怕谁都不知道艾耶手下还有什么。

    此时，我深吸一口气，我知道，当我念出接下来的宣言的时候，整个世界都会因此改变。

    “作为契约的制定者、监督者，所有期望为正义和秩序而战者都可以和我签约，成为守卫家园和秩序的勇士。不管你们的身份和地位过去如何，在签订勇者之约之后，你们将成为互相扶助的战友和兄弟，背叛位面和战友者将遭到契约的处罚，为位面而战的勇士，将根据契约获得神明的奖励。”

    当我念出自己的宣言后，工匠之神贝尼的嗓音又再度响起。

    “而为了让勇者之约顺利被执行，所有契约者可以从我这里领取‘勇者腕轮’，他会记录勇者的战绩和经历，并以此计算勇士因此获得的‘正义点数’。第一批十万只数量有限，先到先得！第二批……等我有空再说！”

    贝尼是侏儒族出生，他的声音带着侏儒特有的尖锐刺耳，语气也是如此，明明是真神，却依旧有侏儒的顽皮性子。

    “作为财富的看护者，而那代表荣誉和财富的正义点数，将在我这里换取诸神的珍宝！千万年的珍藏，我们的宝库可是很丰盛的，永生不死的不老泉、史诗神器、美酒美人、王国贵族、甚至连登上神位的机会，只要你们想到的奇珍异宝，我们这里都有!但是……不接受欠债！只收现金！”

    财富女神贝雅娜的宣言也很有个人色彩，但实际上却也是最重要的一环，没有报酬和回报，凭什么让别人为你卖命，而艾耶神系这次也下了血本，仅仅只是扫了一眼那兑换物品，我都心动的想去打劫了啊！

    好吧，当了这个层面，恐怕所有人都看出来了，别人是把游戏当做异界玩，我是把异界当做游戏玩，既然这世界没有那些开外挂的玩家，我就多制造一点吧。

    “龙傲天量产计划！这世界什么最可怕，就是主角气运的龙傲天啊，为什么那些传奇故事中都是不死小强！因为名不够硬的，都死在路途之中，自然就没有传奇故事了！这次，第一批十万只，我就看看其中会有多少主角命！波ss们，颤抖吧，脚男大军来了！”(未完待续请搜索飄天文學，小说更好更新更快!

    ps：今天还有一更，会晚一点。







  



