








    即使在封闭的基地，却依旧如白昼一般通明，照明设备全开，法师塔的光辉更是点亮了周遭的一切。

    但此时，在一片平地之上，却正在发生很不符合时代背景的事情。

    “手球，禁区内手球！裁判，这是点球，绝对是点球！靠，不理我，是不是又收了黑钱的！”

    迪尔满是怨念，这么明显的禁区内手球都不判，这球还怎么踢。

    “哈，居然敢对裁判咆哮，红手绢（找不到红牌的替代品）！”

    得意洋洋的银钩从口袋中掏出了一个被涂红的旧手绢，对着迪尔一挥。

    “友谊第一，胜利第二，多么卑劣的球员，这又是多么公正的裁决。赞美你，公正的地精裁判。”悲风一脸平静说道，但若不是他就是手球的犯规者，或许会更有说服力。

    迪尔当即就怒了，掏出法杖就打算开干，却被队友连忙拉住了。

    “地精收黑钱正常，不收黑钱才不正常，他收了黑钱我们才不担心再被敲诈。而且，银钩那小子有多黑你又不是不知道，他既然收了钱的，早就不要脸了，你和他闹，搞不好他会借机把我们更多的队员罚下去，那就真的输光光了。”

    “昨天不是说我们也要送钱的？怎么今天这哨更黑了。”

    “那啥，我们送了两千金币，绅士之国足球队（简称国足），直接送了六千啊，你说这见钱眼开的地精会怎么判？”

    “那大人为啥还要让那个地精当裁判？不换个公正的。”

    “我也问过大人。罗兰大人说地精们腿短，两边队伍都不要。踢不了球。想让他们不当裁判也行。只要当队员就够了，你愿意接受球比膝盖高的队员吗？”

    “…….其实可以试试当守门员的，噢，哪算了吧，会笑死人吧。”

    “呵呵，的确，那更离谱，跳起来都摸不到球吧。”

    反正只是娱乐。输了也只是帮忙做做家事，两人把自己说乐了，却也没有深究了，旁边的银钩、卡巴拉等地精倒是越听脸色越难看，越是恼火。

    “侮辱裁判！红手绢罚下！”

    “红手绢你妹啊，我们在场下！足球里面有可以把聊天的罚下去的吗？”

    恩，没有看错，这是一场足球赛，由律法、东岚公国联队（简称法国队），对自由绅士之国足球队（简称国足）。你也没有看错，这简称都是我起的。只是想借机发泄一下积郁在心底的多年怨念。

    被困守到基地已经一个月了，食物、食水什么都够，但漫长的室内拘禁式生活让习惯自由自在的众人很不习惯，士气和精神很是低落，这种时候，就轮到发泄多余精力的体育项目出场了。

    于是，我就“发明”了足球、篮球、羽毛球之类的球类运动，但可惜，唯一被众人接受只有这个难度最高的足球，毕竟篮球啥的，太依靠天赋，而各种族区别太大，根本玩不到一块去。

    比如说篮球必然会有冲撞，强悍的肉搏种族优势太大，还有巨人种族的低头灌篮，地精和矮人抬头都看不到篮筐之类的，足球能够被众人接受，只是大家都不习惯用脚踢脚，还处于同一水平线上。

    但即使如此，乱七八糟的情况也不断发生。

    “靠，悲风，你还玩上瘾了，你就不能乖乖用脚来踢球啊！”

    那个貌似忠厚的男人微笑道。

    “可规则上也允许手以外的其他的部位来踢球啊，尾巴完全符合规则啊，要不，你也用尾巴啊。”

    憨笑着悲风满脸惊讶，似乎还在奇怪对方为什么不用尾巴。

    “我是人类，有个x的尾巴啊！你这个#%……%￥%！”这是第几个被国足队长悲风.埃罗气的骂人的倒霉蛋？

    好吧，基本都是这样的节奏，照搬人类足球赛的规则显然不适合异族，各类bug层出不穷。

    但该说国足这简称用的好吗，悲风带领的国足简直找bug和漏洞的高手，乱七八糟的花招层出不穷，场上场下各种刷下限和耍无赖。

    两队累积交手三十多场，法国队才赢了五场，剩下的全部被国足阴了。

    当然，这场出现的bug下场前就会修正，但从某种意义上，却也是逼着人数太少的他们不断给规则找漏洞。

    “侮辱对手，红手绢！”

    收足了钱的地精果然尽职尽责，立马抓住机会就把对手罚下。

    好吧，至少他是今天第三个被罚下去的倒霉蛋，场面已经变成了11打19。

    啥，人数不对？开始还是11打11，但这群绅士实在太猛，不上两倍人数根本没法打，但即使如此，在各种乱七八糟的阴招之下，法国队也没赢上几场，国足持续大获全胜中。

    啥？咱这是在意.淫？这么多年了，每次看国足比赛就仿若遭罪，既然到了异界了，咱就意.淫两天行不。

    哎，那天国足才能再进次世界杯，也不指望能赢，平一场让我们高兴下吧。

    “想赢？下辈子吧！”

    正当我想入非非之境，矮人罗蒙斯粗豪的嗓音把我逼入残酷的现实。

    于是，我毫不犹豫的开始报复。

    “雪息冰壁！”

    冰剑猛地插入地面，正在带球奔跑罗蒙斯眼前就多出了一面冰墙，然后他就“乓噹”一声撞墙上了，龙皮球直接砸在脸上，然后狠狠的摔到了地上。

    我定的规则的确禁止了直接攻击和阻碍对手，但却没有禁止在地上铸墙啊，何况我收拾的根本不是对手。

    “混蛋罗兰，我们是一边的！你坑我做什么！”

    或许就是这矮人壮实的离谱的身躯，才让罗蒙斯能够跨越短腿一族的劣势。成为球队的一员。

    而他喊出的。却是最让我烦恼沮丧的。

    明明我想加入了是人类一方。为什么他们都说我非要加入绅士侧，而且，还为了限制我的行动，不让我用阴招，硬是逼我当了平时不能随便移动的门将，难道这是说我在是一个人类之前，更是一个绅士？

    “那个，不是我干的。你看对面球队的蕾妮也是极光骑士，肯定是对面干的。”

    “瞎扯什么！蕾妮丫头那有你这么阴险，还偏偏来了三段式墙壁，根据身高定制阶梯形状，先撞膝盖再撞肚子最后撞头！老夫的腰啊，痛的我是……”

    在国足队友直接争吵的时候，落下的球已经被作为前锋的卡西欧捡到，然后对面法国队球员一下子就脸色紧张了。

    最近的规则不断修订，各种超自然、种族天赋阴人手段都被限制住了，但有一点却是无法限制的。那就是四条腿的卡西欧。

    来去如风的半人马是真正的游骑兵和王牌射手，而之前为了限制他。又一次法国队真的上了骑士团，硬是把足球变成了马球。

    但显然这种违规战法对其他人太不公平，于是在一场胜利后，坐骑战术就惨遭规则和谐。

    但可惜的是，绝对平衡是永远不可能达到的，不管卡西欧是不是跑的比战马还快，我们都不可能要求半人马用两只后腿走路。

    此时的卡西欧尽展球王本色，眼看他只是侧身玩了几个假动作，就通过高速的挪移把对手的后卫完全甩开，马上就要进入射门范围，得分简直轻而易举。

    “啊啊啊啊！”

    年轻人就是不禁夸啊，刚刚说完，他就马失前蹄，摔到在地。

    “那个混蛋挖的陷马坑！居然还有绊马索，你们太恶毒了！你们以为这是战场吗？你们的体育精神了？”

    在球场后方观看的我摸了摸鼻子，没有说话，似乎是昨天晚上聊天的时候我想起来的，就说给蕾妮听了，没想到他们真照做了。

    “看来，第六百四十三条规则也确定了，事前不允许对球场动手脚。”

    每次比赛都可以出现四五十种新耍赖方式，如今的足球规则已经厚的的如电话簿了，我估计按他们这么胡来下去，等规则最终版被确定，光规则书就能放满一个书架。

    “呵，到时候新晋球员上场之前，先要笔试球场规则，不考六十分不许上场。考完了笔试考现场，带球、转弯、阴人，一项项的来，不考的你毛焦火辣想杀了教官就休想过。过了再给你发个c照，十年球赛不死再给你转b照，超级球星可以转a照。”

    我当然可以简单粗暴的规定只能做什么，但那样多没意思。反正是娱乐，这样热闹的胡来才有趣。

    而且，貌似他们早就把这足球比赛当做了脑力风暴，想方设法给对手下阴招才是取胜之道，原本纯粹的体力运动，变成了现在体力、脑力各占一半的综合竞赛，我已经迫不及待的看到这异世界的足球到会发展成什么样。

    “不管最后会变成什么样，至少比那个杀人网球、杀人兵兵好。”

    想起两个失败的尝试，我又是一脸冷汗，那两个项目我是规定只允许做什么，基本封死了大部分漏洞，但结果却是球赛向着另外一个诡异方面发展。

    “既然只能按照规则来发球，我就全力挥拍吧。看招，人宠合一，白鲸发球！”

    少女极光骑士的挥拍很优美，雪巨人变身后的挥拍很给力，但从那当场击碎钢铁的网球来看，若不是打偏，搞不好当场就会死人。

    “哼，是时间给你们看看真正的技术了，看招，消失的魔球！那千万只黄金球中只有一个是真实的！”

    黄金球你妹呀！明明是黄金弓加多重箭，什么叫这有一个是真的，那球落地后满地的网球是什么回事！卡西欧你给我弄消失啊！

    “看招，刀斩肉身，心斩灵魂，剃刀发球！”

    你以为你说剃刀就真是剃刀了！你的球了？我只看到剑气啊！你因为用球拍甩剑气别人就看不到了，这招我也会啊，看。旋风斩发球。看你死不死！

    也幸亏是龙皮特制的球。否则早就被他们的蛮力打的粉碎，很快，这两个游戏就没人玩了，毕竟除了那几个变.态，大家还是很珍惜自己的性命的。

    之后，我总结了教训，就是因为规定的态死，只能在发球和接球的时候加力。结果就是一球比一球重，一球比一球诡异，最后迟早弄出人命收场。

    “杀人网球啥的，还是算了吧。把注意力转移到如何作弊上来，应该对球员和观众们安全一点。”

    但事实已经证明了，安全只是相对的，逼急了，纳什之牙、马龙之肘之类不要太多，苏亚雷斯之牙、苏亚雷斯之手之类的，也屡见不止。但不管闹成什么样，至少大家都爱玩。就说明这诡异的足球比赛还是发展前途的。

    “说不定传到法师之国，会有一群法师骑着扫把在空中打球，然后还有专人负责击球把对手打下扫把。恩？印象里面好像有了，是个异世界巫师发明的，好像叫……就叫魁地奇！”

    我在搜罗过往记忆兼发愣的时候，捡到球的法国队已经把球传到了面前，然后一位骑士大力抽射。

    “雪息冰壁！”

    冰剑落下，整个球门前全部被冰墙挡住，对方的射门无法进入分毫，就直接被冰壁弹飞。

    很显然，这是在作弊，而这种在球门前堆满障碍物的做法，之后也必然被和谐，也只能够使用这一次。

    “第六百四十四条，比赛中也不许对球场地形动手脚。该死，又少了一个能钻的漏洞了。”

    而当我对着沮丧的敌人微笑打击他们的信心，而内心却为找漏洞越来越难而沮丧的时候，下一秒，突然一道雷暴就击碎了我所有的冰壁。

    “雪息冰壁是四环法术，他没有多少魔力，释放不了几次，大家不要气馁。继续！”

    科洛丝的喊声当即鼓舞了全队士气，但我却皱起了眉，当然不是因为眼前那群嗷嗷狂冲的年轻人。

    “第六百四十五条，场下人员不允许干涉场战况。啦啦队也来帮忙，太过分了。”

    短裙短衬的科洛丝被众人推上了啦啦队的位置，但其实她一直都想上场。

    此时，带球的蕾妮已经到了我的面前，但我依旧没有拦阻的意思。

    “哎呦！好痛！我的尾巴都撞弯了！”

    是的，我毫不担心的缘由，就是我的金牌后卫——悲风.埃罗。

    有他在，突破球门真心不是个体力活，你首先要对付的就是他厚脸皮下的无尽阴招。

    这次，仅仅只是踩到了他的尾巴（我发现他还是故意把尾巴伸到别人脚下），这厮就躺在球场上装样，然后……

    “红手绢！”早就被买通了银钩怎么会错过这个机会。一张红手绢，再度扭转了局势。

    这裁判的存在感实在太强，场边的嘘声响彻云霄。

    “第六百四十六条，若一方强烈反对，通过现场观众投票，允许更换裁判。”

    在双方你来我往的胡来之时，球赛已经临近尾声，国足三十比九领先（你没看错，这样胡来进球比正常球赛来的容易多了。）。

    “该结束了，再不结束，法国队的进球就达到两位数了。”

    “裁判，我们换人！我下，让格林上！”

    话音刚落，刚才还在沸腾的场上场下就变得静悄悄的，然后一个高大的身影刚刚踏入球场，周遭的观众就集体后退了几步。

    毕竟，一周前他登场那次，实在太过震撼人心。

    当时我高喊“关键时刻了，大家一起上，干掉他们”，那厮居然当即按下了一个按钮，然后“轰隆”“轰隆”，整个球场和其上全部球员一起飞掉了。

    虽然他控制火药做的很好，几乎没有人因此受伤，但其中的辣椒粉和胡椒可不好受。

    烟雾缭绕之后，不少人眼睛红肿咳嗽了两三天，当时格林就被打成了猪头，且一下子从主力沦为替补。

    事后我才知道，由于我提到球场如战场，要提前做好准备。这厮居然真的把它当做战场做准备了。

    他提前一晚就在球场挖坑。硬是在球场上均匀布置了九百个小威力地精暴雷。结果爆炸设计的很巧妙，虽然没有人受重伤，但集体休赛三天是少不了，甚至不少观众也被卷进去了。

    现在虽然明令禁止在球场上使用爆炸物，但看到他巍峨的身影，所有人都想起之前的惨剧，记忆犹新。

    “格林，交给你了。干掉他们！”

    格林换下的是我，庞大的身躯或许很适合门将的位置，但实际上格林根本就不会守门，找他上去也不是为了当守门员，此时，抓紧时间找个安全地点逃命的才是我的主要目的。

    “放心吧，大家，这次我没带爆炸物…….”

    “轰隆！”

    这谎话只持续了两秒，倒霉的迪尔就直接被炸飞了。

    “轰隆！”

    第二个炸弹居然在观众席上，这下。观众们哪还有看球的意思，当即四散而逃。而球员们也不会傻愣着，在轰隆轰隆的爆炸声之中，在球员的飞来飞去的背景之中，这场球赛一如既往的提前结束。

    第二天，这种戏剧化的球赛结局，还是所有人口中的热门话题，而那个倒吊在球场上的罪人身影，更是成为了众人唾弃的对象。

    “哼，都是那个混蛋，害的我输光光。”

    “你赌了啥？法国队赢？那你真是输了活该。”

    “没，国足太无耻了，只要还允许耍阴招，就算两倍人数法国队依旧不可能打赢，我只是赌最后法国队进球数会超过十个。”

    “我也是赌的那个啊，差点，就少一球，真是可惜啊！但貌似好多人都是选的那个，结果都气的要死。”

    “哼哼，你们就不懂了吧，我这次下注的目标是球赛被格林炸飞，结果就中了！10倍的赔率啊。”

    “靠，这都可以猜中，你太厉害了。不过，这样的赌注都可以下吗？”

    “别说，还有‘苏亚雷斯之牙’这个下注选项，只要球场上有人咬人就算赢。上次就有人压中了。这次我也压了那个，可惜没中。”

    “厉害，这么小的几率你都敢读，还赌中了。不过这次球场爆炸才十倍的回报，意外有点低啊。”

    “是啊，好像还有个家伙在这个选项下了大注，他好像还是‘法国队十球内加爆炸收场’一起压的，一个人席卷了九成赌金，才压低了赔率……”

    在某件漆黑的房屋之中，那个压中大注的幸运儿看着眼前的如山金币，正在得意的狂笑。

    “别笑了，该分赃了！”

    我环顾四周，作为合作伙伴，银钩、悲风、蕾妮都在，他们的眼神中也满是对收获的饥渴。

    “银钩，你的，五分之一，爆炸准备和裁判工作都辛苦了。”

    银钩乐哈哈的收起钱袋，对于地精来说，没有什么比数金币更让人愉快了。

    “下次继续合作，我们一起赚大钱！”

    我笑着点了点头，却没有打算告诉他，由于他上场实在做得太过分，裁判资格已经被剥夺，下次和我坐一起分赃的裁判，搞不好就是地精王子卡巴拉。

    “悲风，你的，五分之一，球场上做得好，不过……”

    “我知道的，我不会忘记背黑锅的格林兄弟的，做老大要讲义气，否则怎么能服众，剩下的就交给我，我会让格林兄弟满意的。”

    虽然不知道这位何时成了绅士们的公认老大，但我依旧点了点头，只要不触及自己的爱好，在某种意义上，狡猾至极的悲风办事牢靠，手腕灵活，比任何人都值得信任。

    “蕾妮，二十分之一，下次踢假球认真点，那个表情和动作都太假了，都差点穿帮知道不？”

    “罗兰大哥，不是说好了五分之一吗？”

    “笨，小孩子拿多了钱怎么好，我会帮你保管的，等你长大了再一起给你。”

    “哦，好的。”

    或许是公主殿下本就图个好玩，明显被黑金了却也一点都不生气。

    “对了，罗兰大哥，凯丽姐姐也和我打了一个赌？”

    “嗯？”

    “他赌你肯定会没收我的那部分，还是以小孩子不能乱花钱的名义来的。”

    “呵，太真了解我……哦，不对，你罗兰大哥是这样的人吗？”

    突然，我意识到有些情况不对了。

    “等下，凯丽怎么会知道？”

    “呵呵。你都说过了，表情太假，被发现了。”小手轻锤脑袋，蕾妮俏皮的吐了吐舌头。

    话音刚落，与此同时悲风和银钩就一把抓过自己金币打算开溜，但房间的门却自己打开了。

    “果然！大人，你，你真是让人不敢置信！我刚听到的时候，还以为有人污蔑你。你这样哪像一个公平公正的神使！”

    科洛丝那满脸沮丧和震惊，就仿若看到自己不争气的孩子走上歪路。

    “莫莫好不容易存的家当……又是你这个坏罗兰！但只要让莫莫今天晚上抱着小罗罗睡觉就算了。呵呵，莫莫保证什么都不做的。真的！最多只是脱下他的小衣服…….”

    那个莫莫，先擦擦口水再说这话吧，什么都不做？我信你我就真傻啊。

    “小罗兰，呵呵，居然敢带坏蕾妮，是不是忘记你凯丽姐的手段了。凯丽姐绝对不是为了自己输钱而迁怒啊。”

    那个，凯丽姐，你输了多少我补给你，别对着我这么笑，怪吓人的。

    “我的铠甲，差点死掉才弄到的米西洛尔秘银合金全身甲，大人，可以还给我吗？”

    门已经被愤怒的讨债者堵上了，熟悉的面庞上，也是熟悉的怒容，低着头思索了半天，我说了一句。

    “看球需理性，赌球毁一生，真心的！还有，人为财死鸟为食亡，想让我退钱退货，门都没有。对了，银钩和悲风就要跑了，请务必不要放过他们。最后，别打脸，明天我还要见人的。“

    【第六百四十七条，球赛有关人员不允许参与组织赌球；第六百四十八条，绝对不要让贪财的地精当裁判，更不要让狡猾的罗兰参与组织比赛！地精和罗兰永远不许参与赌赛！】(未完待续。。)







  



