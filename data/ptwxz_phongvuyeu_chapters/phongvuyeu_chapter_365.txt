








    有辛克作为内应，万亡会的人根本没想到这个交易者已经变成索命的敌人，有心算无心，当天晚上的收网行动简直毫无难度可言。

    在“老地方”等着他们的，虽然还是往日的交易对象，但效忠的对象却很有些不同，仅仅只是一个接触，就直接被黑暗游侠擒下了。

    好吧，我也觉得这个独眼大汉黑暗游侠很别扭，毕竟在某个世界，某位黑暗游侠女王更是人气天王，但既然那个世界发展到后期连人类都能够当猎人干游侠了，进入黑暗、死亡领域沦为黑暗游侠也很正常……虽然肌肉大汉黑暗游侠的确有些瞎眼，可惜我这里的娘溺泉没有剩…

    “对了，系统，你以前的娘溺泉什么的是真的吗？真的做得到完全转换吗？”。

    的确，和需要担心器官排除、感染、神经接触不良的现代器官移植不同，亡灵手术随意切割拼凑，什么不良反应都可以用魔力解决，技术也很成熟了。

    闻言，我一愣，别说，我的外科技术连脑袋和器官都可以随意更换，变性手术什么的不要太简单，但稍微想了一下，还是放弃了打造一个“正统画风”的黑暗游侠女王的打算。

    “……我觉得要立马道歉，否则会被干掉。对了，这么说，花木兰这么多年从军没有被发现，难道因为她长的和男人……我还是就此打住吧。”

    咳咳，这一不小心就走偏了，其实连变性手术都没啥难度，其实完全整容也不算什么，但一想到一个苗条美女里面是个粗豪大汉的灵魂。那一口清脆的黄鹂音原来却出于一个抠脚大汉，萌什么的没感觉。恶心到是不少。

    科洛丝？呵，科洛丝的性别就是科洛丝！汉子什么的？这个问题若是问硫磺山城那群混蛋，小心他们揍你，据说科洛丝追求者俱乐部的会员已经到了五位数，没有入会但心怀不轨者恐怕已经是六位数，就算她不当偶像，也成了新兴的邪教。

    对了，那家伙好像也离这里不远，或许，可以调过来养眼……帮帮忙。

    好吧，一不小心更偏了，看着那个正在树梢上遥望的黑色大汉，我还是放弃了给其改头换面的打算，他也恍然不知自己逃过了一截。

    言归正传，当肩上的重任放松了一下的时候，我也自由了不少，而此时，邪恶的亡灵法师也有好处的，至少可以不顾及手段，于是，在我面前，那个倒霉的万亡会祭祀把他几岁尿床都交代的一清二楚了。

    “……比想象的还要麻烦。”

    情报刚刚外泄的时候，万亡会就已经知道了那个遗迹的事情，但开始并不重视，只是为了活动经费而派出了一个有三五个高阶职业者组成的探险小队，对遗迹进行了探索。

    结果就是最后只有一个在外围活动的学徒活了下来，包括一个黄金大师的探险小队连敌人的情报都没有传达出来就全部挂掉了，本来探险就应该到此为止，但一个意外的消息，却让万亡会在萨沙公国的本地被被震惊了。

    “遗迹内部有一个地区的空间屏障极其薄弱，非常适合打开位面之门。一个普通的黄金阶亡灵法师，居然在这里召唤出了骨龙。”

    于是，新的探险团就上路了，虽然侦查小队连续被坑了三次，但这次的探索团不仅带着足够的祭品，还带着亡灵大帝赐下的圣物，组成成员都是精英，本身也是万亡会过半力量，不仅仅是一个圣阶。

    若这是坏消息的话，那么接下来却是一个好坏难分的新消息了。

    不知为何，进山的道路突然被官方封堵了，还是皇家卫队把守，除非和官方翻脸硬闯，所以这个现存战力最强的探索小队也算是被堵住了。

    不仅如此，由于藏宝图被无良奸商、情报贩子专卖，无数萨沙国内的冒险团、冒险者蜂拥而至，原本人烟稀少的小镇，已经变成了门庭若市的大市集，任何新到的外来人，就会成为众人眼中的竞争者。

    听到这个消息，一股强烈的坑爹感在我心口淤积，本来一个个好好的寻宝之旅，怎么变成了夺宝大挑战，说不准还要和官方对上，按照这个节奏，之后肯定还有更坑爹的事情等着我。

    “等下，那么，那些万亡会的祭祀也会在那个关卡小镇了？他们肯定会冲关的，或许，可以用他们借道……”

    看着眼前唯唯诺诺的万亡会信徒，一个已经用的很习惯的办法逐渐成型，或许，只要运用得当，那个战力很强的万亡会团队反而是好事。

    纯粹的死亡之力在掌心化作咆哮的骷髅头，死亡的标记已经证明了我的实力。

    “小家伙，本来你应该死路一条，成为我的实验品是你唯一的可能，但现在情况稍微有点变化。我这里却有一个提议。或许。你会‘自愿’帮我一把，当然，你也可以不愿意，我手头的实验材料还是很缺的。”

    虽然是万亡会的信徒，这个疯狂点头的中年祭祀，似乎同样畏惧死亡——

    在稍微做了一些布置，让其他人处置那些村民之后，我了解了手中的琐碎杂事。直接回到了扎营地的帐篷中休息。

    而即使已经入夜，营地中的人声却没有停歇，那些刚刚逃出生天的村民白天的时候还处于逃出苦海的兴奋之中，到了夜晚，静下来，想起那些已经不在的亲人和废墟一般的家园，不知道哪里首先响起的呜咽声，当这压抑着的悲伤传播开，整个营地就满是哭声了。

    老人抱着小孩低声抽泣，女人一边回忆自己的男人一边担忧着未来的嚎啕大哭。少年想起自己家人再也不见面，眼泪也不住的往下掉。

    在这个残酷世界。这样的事情已经太过常见，老油条们早就有所预期，要么躲在帐篷里塞住耳朵，要么就很抢在之前入睡了，而对于年轻的冒险者来说，这样的场景短时间很难适应。

    似乎想起了自己的遭遇，靠着自己的义父，卡特琳娜默默的抽泣起来，北地三人组反应不一，玛丽的脸色很不好看，黑暗精灵却已经习惯了别离，她脱下了沉重的铠甲，攀在营地的松树枝上，拿出五弦的木竖琴，轻轻的吟唱起精灵语的送魂曲。

    悠扬而平静的异国曲调在营地中回荡，稍带离别之意的曲子并不算悲伤，哭声却变大声了，而最年轻的审判者索尔却满脸茫然，这真实的冒险经历，和过去期盼中的史诗冒险相比，似乎很有些不同。

    按照传奇故事情节中的，获得救援的村民，不是应该奉献自己的鲜花和香吻吗，为什么获得自由的她们会如此伤心。

    精灵那送别的曲调还在耳边环绕，眼前的一幕在不断冲击他的认知，凡人的悲伤告诉他满是梦想的大冒险不是书本上那么简单，那些仿若传奇故事中的背景的村民，也有自己的生活、哀伤、幸福,或许他们的那点点滴幸福在贵族老爷面前不值一提，但却已经是他们的全部。

    家没人，等他们回家的人也没有了，连自己都差点变成了奴隶，哭一下，发泄一下，没错吧。

    华丽的修辞是没有必要的，失去亲人和家园的痛楚让良知者感同身受，在真挚的悲伤面前，地精平日显得滑稽的面庞不得不严肃起来，他犹豫了，似乎想做些什么，告诉他们，未来还有希望。

    “……让他们哭一下，你能做些什么？说些无关痛痒的安慰吗？别去打扰他们！”

    但他却被自己的同伴拉住了，同样满脸铁青的玛丽也很是不爽，这已经不是她第一次遇到的事情了，她知道，这个时候外人的安慰除了拉开伤口之外，没有意义。

    但索尔突然甩开了同伴的手，迈开小短腿跑了过去。

    “我，我，我不是来安慰你们的，我这里有一种力量，并不需要多好的天赋，普通人就能学，它，可，可以保护你们，让这样的事情不再发生。”

    地精一紧张，本来就有些结巴，却越发口舌不清了，但随着心底感情的投入，胆气越来越足，那不算大的声音却越发清晰大声。

    “是的，律法之力一定可以保护你们的！这就是那个大人创造它的目的。他不是为了那些强者提供一种新的欺凌弱者的工具，律法，还有律法之力，都是给与弱者的保护和救援！这是一种任何人都可以尝试掌握的力量，他是为了守护和惩戒罪徒而”

    索尔从脖子上取下锁链，那天平的徽记摆在手下，下一刻，银色的光辉在其上点燃，圣火缭绕之时，漆黑的乌云被打开了，一道银光落了下来，正好照在了地精的身上，天空之上，似乎有无形的眼睛在注视着这里。

    “几率微乎其微的神迹？不，冥府的律法之柱永无休眠，任何人都可以向其寻求公正和救援，我们不信奉盲目崇拜神明，但我们尊敬冥府诸神，因为他们给与弱者救赎和守护，给与不义者惩戒！”

    索尔打开背后的包裹，一股脑的把其中的律法典籍和学习笔记交给了那些有些茫然的村民。

    “先学《法论》，再从《律法和圣光的区别开始学》……”

    话语说到一半，他却想起了在硫磺山城看到的情景，这下他知道为什么书店之中这些最基本的典籍始终如此畅销，而那些购买者中不乏十数年的老法官，按理说他们早已经不需要这些入门书了，这下他知道，为什么那些刚刚外出公干的审判者和公正骑士，总是第一时间就是去补充书籍了。

    “你们的典籍遗失了吗？”。

    “没有，送人了。”

    “传播法律？太好了，又有了新的律法信徒？为什么不多带一点。”

    “多带？带再多也是不够的。呵，我和你说这些做什么，以后你会懂的。”

    过往的回忆在脑海中一闪而过，索尔满脸苦涩的笑了，这下他真懂了，但看着眼前的景色，却宁愿不懂，或许，当整个世界都是如此残酷的时候，带再多，都没有意义。

    “真的可以吗？我们这样的人也能够获得力量？”

    突然，茫然的人群中，传来了急促的询问声，那是一个年轻而瘦小的身影，衣不裹体的少年刚刚还抱着自己爷爷哭泣，此时，双瞳中却满是期望，是索尔展示的神迹和话语给了他希望，一个对于未来的希望。

    “啪。”

    一巴掌狠狠的打在自己的脸上，脸当即就红肿了，但索尔却全无感觉，他在为自己那一刻的茫然而感到羞耻。

    “对自己没有意义？呵，愚蠢的索尔，对他们有意义，就够了”

    刚刚走出校园的审判者深吸一口气，一边努力回忆导师是如何传授的，索尔一边开始向普通人讲述自己的律法之路，就如同他万千的前辈一般。

    “首先，你们要理解什么是律法之力和律法，这并不是什么攻击性很强的力量……“

    而在他背后，树木上的暗精灵悄悄的点头，她这才开始认可了这位同伴，而那送魂曲的声音却默默的停了下来，让地精那刺耳的宣讲声在夜里传得更远。

    而与之同时，在营地中最大、最豪华的那个帐篷中，那个应该已经入睡的人，却伸手拉下了窗帘，收回了自己的目光。

    他拿起摆在手边的一本手抄书，开始把自己今天的心得写入其中，这本书已经接近尾声快要完本。

    而这本手抄书的封面，却是《冥府之力在法咒术上的非常规用法——让低阶律法者在高阶职业者也有搏命的机会》。(未完待续……)

    ps：貌似已经恢复了一点状态，这两天应该可以加更一次补之前欠的

    那啥，名次好危险，继续求下保底月票。

    第四百九十九章传播：

    ...







  



