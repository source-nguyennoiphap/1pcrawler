








    埃索里维斯，亡灵语“埃索长眠之地”，这片区域作为开拓者的亡灵大帝埃索的陨落之地而举世闻名，八年之后，这里却有了另外一个名字亡灵之都。

    那一年，埃索果断的自我牺牲，这里就成了链接死亡位面之地，八年来，尸骨、冰寒、亵渎、幕血、黑绝、雾亡六大死亡位面不断传送援军抵达，逐渐将其建设成了世界规模最大的亡灵之都。

    整个区域都已经被死亡规则覆盖，数百米的骸骨巨人在平原上慢走，天空中徘徊的石像鬼和骨龙成打，但实际上，在世人眼中，这里却并不是最危险的生命禁区，甚至从某种意义上来说，成为了闻名遐迩的旅游胜地。

    那是因为在埃索里维斯肉眼所及的地方，就有那片翠绿的森林存在，那是如今主位面炙手可热的白银之子木灵一族的大本营。

    即使在森林外延，也有如山峦一般强壮的双头树人在来来回巡视，他们手持的大棒就是通天木，而天空之中，奇异风格的木质战舰群也在不断穿梭，

    从某种意义上，这些看似闲散的“野怪”，却都是大师、传说以上的战力，双方也打了这么多年，但这边的战场，一直以来却被戏称为新手的新人村，很少有资深的契约勇者选择在这里历练。

    原因？就是明明战斗的烈度实在不高，实在有些打假球的疑虑……实际上双方依旧敌对，只是无论如何都无法获得胜利的战争，投入再多的资源也是浪费，不如就这么保持默契。

    在之前那数次大战役之后，形势已经变得很多，木灵们已经成长起来，梦境之森变的根本不可能被攻破。而埃索里维斯作为亡灵六大位面的最重要入境口，塔力共和国的数位大佬会不惜一切代价保住它。

    不是没有勇者尝试过威胁埃索里维斯的安全，但真到危急的时候，海量的高阶亡灵和大法术都会从位面门中被丢出来，数位亡灵大帝们决心不惜代价保住这个常态传送门的时候。强行入侵被视作不可能。而就算清理到要塞和亡灵，以埃索生命换取的位面链接，也根本不知道该如何关闭。

    没有进展的战争自然谁都不想打，在真的命运之日前，塔力共和国的复苏是不可能达成的，亡灵方也在耐心等候。

    而他们的主要战力，也集中在拜尔交锋的雨夜要塞等主战场。毕竟。比起没有扩展性的木灵来说，主位面的人类才是它们的敌人。

    但随着拜尔国力的不断增强，尤其是龙族的加入，南方教派的崛起，缺乏顶尖战力的亡灵方居然有逐渐被压制的现象。

    拜尔帝国只是牢牢的卡主了那些要塞，阻止了亡灵们北进的道路，因为谁都知道，在亡灵大帝们真正降临之前。和亡灵死磕毫无意义，死亡位面的亡灵根本没有极限。

    但在另外一个方面。拜尔就很有些头痛了，在“第二战场”，亡灵们却取得了一系列的成就.

    万亡会等邪教徒如野火般的蔓延开来，“永生”的诱惑不仅对那些畏惧死亡的贵族有效，对那些因为战争、穷困等缘由痛苦、绝望的平民一样很有吸引力。

    “你畏惧死亡吗？你为生活痛苦吗？你为缺乏力量痛苦吗？加入我们吧，你讲获得改变命运的机会…….”

    万亡会的信徒大概也是牛皮癣和传销专精，如今，在拜尔的各大城市的小街小户之中，都可以看到这样的宣传。

    别说，比起某些神祗毫无真实度可言的传教，万亡会说的还真是实话。

    恐惧？亡灵不会恐惧，饥饿？亡灵不会饥饿，找不到老婆？亡灵不需要妻子……咳咳，最后一个不算，应该是心爱人已经死亡？换个方式让她在活一次。

    战乱时代，最凄惨的就是没有力量的平民了，当强者失去束缚的时候，当和平社会的基调被打破的时候，没有力量也成了一种原罪……受害者渴求复仇的力量，野心家渴望向上的力量，就是三观正常的普通人，也渴求保护自己力量。

    而想获得力量，却不是那么容易的。

    不提简直是烧钱的魔法修习，就是修行武技，也需要名师传授和足够的营养，而修习亡灵之道所需要的，大概只有作为材料的尸体和作为正常人的底限，而在战乱时期，恐怕这两者是最不值钱的。

    万亡会的传播会如此迅速，也是因为它的确给了那些渴望力量者迅速强大的机会，甚至在某些地方，使用亡灵、黑暗魔法的平民速成强者，也得到了和其他强者相同的待遇，入门资质、财富要求极低的亡灵之路，居然有了主流化的倾向。

    当然，事情发展到这一步，罗兰的锅也是逃不掉的。

    北地亡灵不遗余力的洗白和宣传，还有亡灵魔法的生活化、亡者和生者共存的现实，还有南方教派不遗余力给原圣光教义抹黑（这是敌视亡灵的根源之一），当然，还有塔力共和国和旗下万亡会的宣传攻势，瓦解了不少抵触心态。

    而另外一面，随着冥府的建立，新的四大元素水涨船高的遇到了大发展的机遇，寒冰魔法开始变异，寒冰的元素位面正在构成，而死亡之力，却也到了发展。

    总之……这或多或少都和罗兰有些关系。

    咳，言归正传，当魔潮降临的当天，空间对某些放逐户的限制被取消，拜尔帝国终于等到根除心腹大患的机会，或者，说是被“心腹大患”根除的机会也没错。

    “嗯？这就是故乡的味道吗，还真是……”

    发出感叹的，是刚刚走出传送门的白头小子，他看起来和人类的幼年时期有点像，却刚刚来到这个世界，就低头添了舔地面，然后一口咬掉了坚固的石板，然后口中传出了碎石搅拌般的脆响。

    “……让人作呕啊。到处都是生者的味道，浓烈的生命气息离得这么近，你们这些年到底在干什么。”

    一口吐掉所有的碎石，那个白毛小子带起头，白色毛发如褪色的霜雪般黯淡，双瞳中没有黑色，只有白色的十字星正在熊熊燃烧。

    即使这是在亡灵殿堂，即使房间中的迎宾都是强大的亡灵大君，依旧没有谁敢直视那致命的灵魂之火。

    “修普诺斯陛下，您的降临让……”

    一位血族亲王迎上去，但话语没有说完，却被急性子的雾亡位面的主宰打断。

    “其他家伙了，别告诉我都还没来。”

    考虑这位大佬臭名昭著的超级急脾气，血族亲王连忙做出引路的手势。

    “不，陛下们都到了，都在等候您的降临。”

    闻言，修普诺斯笑了，所有的亡灵大君都知道，雾亡位面的亡巫大帝最讨厌的就是等候和迟到，而最喜欢的，却是别人等他。

    而作为接应者的鲜血亲王心底却在暗自庆幸，幸好通知雾亡位面的集合时间被真正的集合时间的晚上一天，没有耐心习惯早到的修普诺斯才不用等人，作为接应者的自己才不会被这个脾气暴躁的亡灵大帝迁怒。

    而很快，在一间狭小的会议室之中，五个席位的桌子只剩下一个空席，随着修普诺斯的降临，死亡系的顶端，五位亡灵大帝在埃索里维斯汇聚一堂。

    一位昏昏欲睡的老者，一个看起来还在上小学的白毛小子，一个完全和普通骷髅兵看不出区别的白骨骷髅，一个正在小口小口享受红色液体的少女，一个看起来很是普通的人类。

    若一定要给亡灵大帝们找出些什么共同点，大概，就是他们看起来都很普通。(未完待续 。)







  



