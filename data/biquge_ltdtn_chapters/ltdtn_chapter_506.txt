    厉鬼也好，神明也罢，都是一股气息汇聚而成。
    这气息是魂魄，是阴气，是香火，是纯阳之气，而这也就导致了鬼神死后往往什么都不会留下，会消失的干干净净。
    而这鬼王居然留下了一颗脑袋。
    “带回去问问李忠，李忠现在做了鬼这么久，知道的事情比我多。”李修远暗道，却是立刻将这鬼王的脑袋装进了鬼王布袋之中。
    再次看了看这座阴山。
    阴山上下还是有无数的冤魂厉鬼存在，还有鬼王阴兵级别的窥视自己，不过眼神之中都是畏惧和害怕之色。
    鬼王被诛杀的这一幕他们可都看在眼中。
    这样的道行不是他们这些小鬼可以对抗的，再加上之前鬼王惹怒了李修远的缘故，这些冤魂厉鬼担心可能会被殃及，步鬼王的后尘，故此心中皆是一片害怕。
    李修远忽的指着一位有鬼将道行的厉鬼道：“你，过来。”
    那被点名的厉鬼浑身一颤，身子都吓的缩小了一圈，脑袋四处张望了一下，似乎还在看旁边有没有其他的鬼。
    “不用左摇右晃了，就是你，过来。”李修远道。
    “是，是。”
    那恶鬼吓的急忙连滚带爬的跑了来，一到面前磕头就拜：“高人饶命，高人饶命的，小的什么都不知道，这里发生了什么事情小的也一概不清楚，那鬼王小的不认识，小的是被他抓来的，和这鬼王有着夺妻杀子不共戴天之仇，多亏高人将这鬼王诛杀了，替小鬼报了仇，小鬼感激不尽。”
    “多谢恩人。”
    “多谢恩人。”
    这恶鬼口呼恩人，连连磕头跪拜。
    “......”李修远神色古怪的看着他。
    这样子还鬼将呢，一点节操都没有，如果之前那鬼王听见了只怕会气的把你给吞了。
    “你叫什么名字？”李修远道。
    “小鬼郑屠，生前是一个杀猪的屠夫。”这恶鬼颤颤巍巍的说道。
    李修远道：“吩咐你一件事，做好了可以活命，做不好，我砍了你的脑袋。”
    “啊~！”郑屠吓了一跳，浑身一哆嗦。
    “怎么，不愿意？不愿意的话我可以换过一个人，不，换过一只鬼。”李修远道。
    “愿意，愿意，还请高人吩咐，请高人吩咐。”
    郑屠又是接连跪拜，连忙应下来。
    李修远指着这阴山道：“将这左右两座阴山上所有的冤魂厉鬼集结到山脚下，记住是所有的鬼，少一只不可，若是到时候山上还有鬼，不但你要死，山上的鬼也要死，听清出了没有？”
    “小鬼听清楚了。”郑屠连忙道，心中却是松了口气。
    这事情不算难，感觉活命有希望了。
    李修远道；“给你半个时辰的时间，半个时辰到了若是还做不到可就别怪我不给你活命的机会了，而且也别试图逃走，鬼王我都斩了，想来也不缺一鬼将了。”
    “是，是，是。小鬼明白，小鬼明白。”郑屠磕头应道。
    “办事吧。”李修远挥了挥手。
    郑屠二话不说，当即化作一股阴风就冲进了阴山之中，随后却听到他的声音在阴山之中回荡：“所有的鬼都听到了没有，快离开阴山，不可逃走，都在山脚下带着，赶走，老子剁碎了你们。”
    “关着的鬼也他娘的全放出来，现在鬼王已经死了，我的话谁不听？”
    “你敢跑？真当老子只是喊喊的么，老子杀了你。”
    “啊~！”
    有厉鬼惨叫声传来，想来是被郑屠给杀死了。
    毕竟是一鬼将，别看在李修远面前磕头就拜，一副哆哆嗦嗦的小人样子，但真面对那些恶鬼的时候威势还是有的。
    再加上此刻郑屠颇有几分狐假虎威的意思，更加的肆无忌惮之前，其他的一些恶鬼更是被他毫不客气的指挥起来，但凡有不听话的恶鬼，不是打残，就是杀死。
    一时间阴神内外恶鬼哀嚎不断。
    “恶鬼还需恶鬼磨啊，这事情我只看结果，不看过程。”李修远骑着龙马在山脚下等着，对这些惨叫无动于衷。
    胆小温顺的冤魂自然会乖乖的来山脚下汇聚。
    只有那些作恶多端，心中畏惧的恶鬼才怕被连诛才会向着逃走，而对这些恶鬼他没有必要手下留情，若是对恶鬼都有同情心的话，那他就不是人了，就是佛陀了，只有佛陀才有这样的慈悲之心。
    还不到半个时辰的时间，阴山脚下就汇聚了密密麻麻一片的冤魂厉鬼。
    除去之前被消灭的一批，这些冤魂厉鬼的数量多的数以万计。
    “如此多的鬼生活在这山上，这山已经不下于一座鬼城的存在的。”李修远心中暗道。
    “高人，高人，小的已经把您吩咐的事情办好了，山上现在一只鬼都不剩下，而且小的适才在鬼王宫殿之中的时候还发现了一些好东西，小的不敢擅作主张，便给高人带来了。”这个时候郑屠谄笑的走了过来，脸上尽是巴结讨好的神色。
    “哦，什么好东西，这鬼王难道还留下了什么宝物不成？”李修远道。
    郑屠谄笑道：“高人你且看。”
    说着挥了挥手，示意一伙阴兵把东西带上来。
    李修远还带着几分好奇的看了一眼，可是随后却是微微一愣。
    却见一群阴兵驱赶着几十位女鬼往这边走来，这些女鬼一个个貌美不凡，有的楚楚可怜，惹人怜爱，有的成熟妩媚，让人心动，有的文雅恬静，使人悦目，总之燕瘦环肥应有尽有，仿佛天下的美人都汇聚在了这里一样。
    “艳鬼？你带这些艳鬼给我做什么？”李修远说道。
    郑屠讨好道：“高人，这些艳鬼可都是那鬼王几百年来收集的，个个都是人间的绝色，她们生前有些是皇帝的妃子，有些是青楼的头牌，有些是山野的美人，高人一个人在阴间身边怎么能没几个女鬼服侍呢，所以小的自作主张把这些女鬼带来了，高人放心，小的可没有强迫她们，她们都是十分愿意服侍高人的。”
    似乎为了应了他的话一样，一些貌美的女鬼已经在给李修远眉目传情了。
    虽然郑屠这做法有些不耻，但他的确是一个人才，见风使舵，巴结讨好的功夫一流。
    “我不需要，我是活人对女鬼不感兴趣，收起你这媚上的一套，不然下次诛了你。”李修远冷冷一哼。
    “小鬼知错了，小鬼知错了，还请高人恕罪。”郑屠吓的急忙跪地磕头。
    李修远喝道：“所有的鬼都跟我来，谁敢走脱，便诛了谁，郑屠你看着他们，出了问题，我先找你。”
    群鬼一颤，不敢任何的反抗。
    郑屠更是欲哭无泪，只得硬着头皮答应这门苦差事。
    末了，李修远再扫看了一眼那些艳鬼，发现这些艳鬼的姿色都不是变化出来的，是实打实的貌美女鬼，这个鬼王到是很懂得享受啊，竟然网罗这么多艳鬼。
    在阴间占山为王，不知道享了多少年的福。
    如今死了，倒也死的不冤枉，够本了。
    “好大，竟比春花的还大，明明那么大，为什么腰却那么细，腿也那么细长，这不科学啊，到底怎么长的。”
    李修远脑海之中回想着那群艳鬼之中一位衣衫半露的女鬼，心中暗暗吃惊，这样的美人若是在阳间的话，说不定他都会去追求，如果是被皇上看见的话一定会千方百计的夺取那个美人。
    美色，果然是天底下最厉害的东西，连他都守不住心神。
    幸亏自己是人间圣人，鬼魅不近身，不然以他这样的意志力肯定是会被诱惑的。
    所以说，要想做正人君子，就必须对自己狠一点，不给自己被诱惑的机会。
    坐怀不乱，要么是女的不够美艳，要么男的是宦官，反正李修远做不到这一点。
    “食色性也，孔子你说的很对啊，真不愧是圣人。”李修远暗道，心思转移，不去想那些艳鬼的事情。
    带着几万冤魂厉鬼，他原路返回，来到了鬼城。
    鬼城修建不够快，是因为这鬼王作乱的缘故，如今鬼王死了，他又带来了这么多鬼魂，相信足以加快鬼城的建设。
    “是大少爷回来了。”
    鬼城之外，李忠带着十几名鬼将，上千阴兵在城外候着的，当他见到了李修远骑着龙马返回的时候却是急忙迎了上去。
    “李忠，那鬼王我已经诛了，这是那鬼王的脑袋，以后挂在城门上，看看哪个鬼王敢闹事，身后的是那鬼王统治的冤魂厉鬼，你接收了，恶鬼让他们去建城，好鬼去送她们转世，那群艳鬼一只也别留，全部送她们转世去。”李修远道。
    说着把那青面獠牙的鬼王脑袋丢了出去。
    李忠下意识的接过，附近的其他鬼将见此脸上皆露出了震惊之色。
    一尊鬼王，说斩就斩，人间圣人的锋芒当真是鬼神难挡啊，比阎罗还可怕。
    “不过我很好奇，这鬼王脑袋为何没有消失，反而有血有肉。”李修远道。
    李忠急忙回道：“大少爷，这是夜叉的脑袋，那鬼王是一只断头鬼，他不知道是在那杀了一头夜叉，夺了他的脑袋接在自己头上的。”
    “是这样.....夜叉么？”李修远问道：“阴间有东西？”
    “回大少爷，夜叉在阴间也有，小的不知道听那只鬼提起过，阴间的某处被夜叉王占着呢，而且夜叉最为凶恶，吃人，吃鬼，寻常的鬼不敢招惹。”李忠道。
    李修远道：“我知道了，待我阳间的事情解决之后，我再来扫平阴间，省的一些牛鬼蛇神都躲在阴间作恶，对了，那些艳鬼记住了，可别留。”
    说完又叮嘱了一下。
    这些艳鬼的姿色太高，他怕这些李忠，以及其他的鬼将把持不住，到时候生出乱子来，索性眼不见为净。
    人喜欢貌美的女子，鬼也自然喜欢貌美的女鬼。
    “是，大少爷。”
    李忠好奇的看了一眼那些女鬼，为什么这些女鬼会被大少爷叮嘱两次呢，看了一眼之后却是暗暗吃惊。
    的确是美艳非凡，应该送去转世，省的在这里祸害阴间。
    “不够为了杜绝鬼王作乱这样的情况再次发生，我会考虑派遣一尊鬼王在这里镇守的，免得再来闹事。”李修远道；“这里的一切就交给你了，我回阳间去了，阳间的动乱还要我去平息呢。”
    “大少爷放心，小的在这里一定会管理好的。”李忠拱手道。
    李修远点了点头也不多言，骑马便走，他在这里李忠反而缩手缩脚，不好办事，他走了以李忠的能力这些事情不难处理。
    “这位大哥贵姓啊，小的郑屠，小的一见这位大哥就觉得亲切的很，不是兄弟，胜过兄弟，小的也是帮恩公办事的小鬼，以后还请多多关照啊。”等李修远走后，郑屠又屁颠屁颠的谄笑着问候李忠。
