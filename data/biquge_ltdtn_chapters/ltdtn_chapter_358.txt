    长须鬼王将扬州孙总兵的魂魄带走之后，约莫片刻之后，便有一股阴风自军营的一个方向腾起，迅速的想着金陵城而去。
    没有了魂魄的孙总兵此刻还睡在军帐之中，不过他的气息却变的微弱起来，整个人已经陷入昏迷之中。
    但在城外的另一处军营内。
    红目鬼王此刻亦是牵着那位赵总兵的魂魄开始离开军营。
    这种勾魂夺魄的事情是阴兵鬼才都会做的，就连寻常的小鬼，如果遇到病危的人，说不定也能以唤名字把病人的魂魄唤走。
    官至总兵的人虽然不同寻常人，可是作恶太多，害人恶念太深，再加上鬼王亲自出手哪会有失败的可能。
    军营之中。
    一队巡逻的甲士忽的眼睛余光一瞥，恍惚之间似乎看到了自己的总兵大人被一个人用稻草绳绑住了双手，像是看押一个凡人一样往前拉去。
    那个人影很是古怪，浑身漆黑，像是被一层黑屋笼罩，但一双眼睛却是冒着虹光。
    这个甲士忽的道：“头，刚才我好像看到了总兵大人被一个人影绑着手带走了。”
    那队长说道：“瞎扯什么呢，看错了吧，这天寒地冻的总兵大人早就在军帐之中休息了，怎么会出来。”
    “真的有看见，那一定是总兵大人没错的，我的眼睛在夜里好着呢。”
    那甲士说着指着一个方向道：“便是往哪去了。”
    可是众巡逻甲士一看，却是发现那里哪有什么人影，只有军帐的一角被风卷起来了，在那里摆动着，咋一看去像是一道人影一闪而过罢了。
    “下回看仔细一点，别拿总兵大人开玩笑，要是被总兵大人知道了小心你的脑袋。”队长瞪了一眼，有些恼怒道。
    那甲士甚是委屈，他觉得刚才走过的人影就是总兵，根本就不是营帐的厚布被风吹动的样子。
    虽有有些小小的耽搁。
    但是红目鬼王很快就牵着绑好的赵总兵离开了军营。
    军营之中虽不是鬼神所待的地方，但奈何这两位总兵的兵太过羸弱，连自己的主帅都护不住。
    倘若是一支铁血之军，那军中杀伐之气能凝聚刀锋，剑刃能刺穿，击伤任何一位靠近的鬼神。
    两位总兵的魂魄被带走，而身处于另外一座军营之中的钱总兵却还浑然不知。
    他并没有这么早入睡，因为孙总兵连夜派来了一千弓弩手，所以他需要处理，布置一番，而这样一来，便推迟了休息的时间。
    “呜呜~！”
    一阵阴风在军帐的上空盘绕飞舞，发出了呜呜的声音。
    军中的甲士没有丝毫的留意，只是当做是冬日里凛冽的寒风，根本不会意识到这会是一尊鬼王蛰伏在这附近，伺机勾魂夺魄。
    不过帅帐之中却是灯火通明，军中的偏将，参军皆在，不是下手的好时机。
    钱总兵此刻将命令一一发了下去，然后到：“明日就这般行事，只等那李修远到帅帐之外埋伏在左右的弓弩手就立刻万箭齐发，不过这李修远的武艺非凡，所以为了稳妥起见还需要布置刀斧手，万一射杀不
    死，刀斧手再上把他砍杀了。”
    “明日早上本总兵会书信一封引诱那个李修远前来，你们都先回去休息吧，明日做好准备便是。”
    “是。大人。”
    众偏将，参军一一退出军帐。
    蓦地，孙总兵想起了什么问道：“对了，那个投效本大人的书生朱尔旦还在军中么？”
    “回大人，还在军中。”一位亲兵道。
    “此人和李修远似乎有过节，明日找个人询问他一番，本总兵虽然想除去李修远，但亦是不想被人当枪使，若是这个朱尔旦真的和李修远有过节，想借本大人的手对付那李修远，你们就寻个由头把那朱尔旦
    给做了，天底下的书生多的是，本大人不需要一个居心不良的书生当幕僚。”
    孙总兵冷着脸示意了一下。
    那亲兵点了点头表示明白。
    “打水洗漱吧，本官要休息了。”孙总兵又道。
    而此时此刻，在某处的军帐之中，这里灯火通明。
    一个身材魁梧的汉子此刻在军帐之中被五花大绑，身上缠满了铁锁链，但便是这样，那铁锁链亦是哐哐作响，似乎要被崩断一样，时不时的被绷得紧紧。
    除此之外，这些锁链上还缠着黄色的符箓，上面画着不知名的符文。
    不知道什么样的人需要这样的手段来捆绑。
    “吴象，不用挣扎了，你是挣脱不了这锁链的，便是你有五头蛮像的力气也是白搭。”
    烛光摇曳，朱尔旦微微阴沉着脸坐在一旁，脸色有些难看。
    “若非你这斯坏事，当日金陵城之战绝不会输，我已经按照神君的吩咐去调遣三位总兵起来了，只等他们的军队一到，就有三位元帅附身其身体之内，随后带领三地大军，齐攻金陵城，李修远挡的了九山王
    ，挡的了杨彪，不可能还挡的了这三位总兵的兵锋。”
    “奈何人算不如天算，昨日夜里李修远竟率领鬼神打上天宫去了，将东岳神君的神权打落，十大元帅，再折损三人，以至于东岳大殿群龙无首，我等鬼神亦是一夜之间成了无根之萍，不知该如何是好。”
    “你这书生说什么，我一句也听不懂，放开我，有胆量就放开我。”
    吴象怒着说道，他挣扎摆动，可是身上的铁链却怎么也挣脱不开。
    朱尔旦站了起来：“你不需要听懂，你只需要知道你今日落到了本神的手中，虽然现在神君从天宫坠下，但亦是有东山再起的机会，毕竟神君是泰山神得道，是天生的神邸，即便是伤的再重也绝对不会灭亡
    ，今日纵然谋害不了李修远了，也要拿你这五头蛮像开刀。”
    “不然有你这厮在李修远的身边，天底下哪还有鬼神妖魔敢贴近他？”
    说完，他又从怀中取出了一张金纸，上面金光闪动，不知道是何物。
    朱尔旦缓缓的将金纸折叠起来，三两下就把一张金纸折叠成了一个钩子的形状。
    随后他把这金色的钩子贴上符箓。
    随着光芒一闪，金色的钩子变成了一支淡金色的铁钩，不似纸符之物，似乎精铁打造而成，但又并非寻常的俗物。
    这是神君书写神令的纸张，如凡间帝王的圣旨一样。
    “你的力气是大，可是再大力气的人也不能大的不过自己。”
    朱尔旦嘴角带着一丝冷笑，他从吴象身上的铁链上取了一端，然后绑上铁钩，随着往吴象的脑门一砸。
    难以置信的事情发生了，这个金色的铁钩竟然没入了吴象的脑子里，既没有伤到皮，又没有伤到骨。
    “啊~！”
    可是吴象却感觉脑门生疼万分，他奋力的挣扎可却挣扎就越痛，越痛就越挣扎。
    “喊吧，哀嚎吧，你体内的东西就要被你自己给拉出来了。”朱尔旦眼中冒出了兴奋之色，因为他知道自己这个办法奏效。
    翁~！
    随着吴象的挣扎，他的脑袋上冒出了一圈白光，光芒耀眼，非鬼神所不能见。
    “昂~！”
    白光之中似有一头巨兽在咆哮，震的附近嗡嗡作响。
    朱尔旦眯着眼睛，盯着那白光，他看到了一个虚影，一头白象的虚影。
    那是，传说之中菩萨的坐骑白象。
    “好痛。”
    吴象双臂，双腿用力，奋力挣扎。试图崩断铁链，可是用力越大，脑袋就越痛。
    但那金色的铁钩却在慢慢的从脑袋里被拉了出来。
    朱尔旦此法是接力使力，五头白象的力气是可怕不假，但若是用四头白象的力量对付一头，却是不难取胜。
    吴象身体之中的五头白象，其中四头分别藏在了四肢之中，最后一头则是藏在了脑海之中。
    脑中的白象代表着智慧。
    也就是说吴象是一个有智慧的人，只是见识不多，阅历少，所以智慧不显而已，若是跟着李修远历练一番，将来绝对是李修远麾下的一员主帅，在凡能争战，在天能统御鬼神，比十大元帅的威胁还大。
    “今日就破了你的五象之身，擒拿驯服你们这几头畜生。”朱尔旦心中冷笑不断。
    有擒拿白象的功劳，他觉得便是神君大人也不好责问自己办事不利了，倘若能驯服这五头白象的话，那他就拥有这吴象的力气，一人单枪匹马都可以去搏杀掉李修远。
    人间圣人又如何？
    敌得过五头巨象么？
    然而就在他这样思考的时候。
    突然。
    “啊~！”
    吴象的痛苦达到了极致，忽的扬起脑袋双目猩红的怒吼了一声。
    白光瞬间耀眼无比，从军帐之中冲天而起。
    “昂~！”
    一头巨大的白象被金色的铁钩勾住了鼻子，红着眼睛，从白光之中猛的冲了出来，它发出了愤怒的咆哮，巨大的象脚奔跑起来，宛如一座小山半的体型横冲直撞。
    “不好？”
    朱尔旦此刻眸子一缩，见到那被勾出来的白象红着眼睛扑向了自己。
    “哇~！”
    下一刻，他感觉自己的鬼神之躯被重重的撞击了一下，这蛮力及其可怕，撞的自己鬼神之躯龟裂，直接离开了肉身飞了出去。
    朱尔旦.....不，飞出去的那道红色的身影不是别人，而是那个身穿朱红色官服的陆判。
    陆判的身躯满是裂痕，香火和阴气溃散，倒在军帐一处的地面上，痛苦的低吟着。
    “昂~！”
    巨大的白象狂暴起来了，红着眼睛继续冲撞，一路无可阻挡，自南向北狂奔出去。
    所过之处，军帐就像是遇到了一股狂风一样瞬间就被掀飞。
    睡在军帐之中的甲士根本就不明白怎么回事，只能看见地面上有一个个大象的脚印路过。
    这菩萨的坐骑白象，凡人是看不见的，但饶是如此亦是能在凡间留下自己的痕迹，影响到周围的一切。
    “那是？”
    此刻，这一幕被天空之上的长舌鬼王瞧见了。
    他看见一座军帐之中白光冒出，冲天而起，刹那之间一头白象自白光之中奔出，仿佛失去了理智一样横冲直撞，一路向着北面而去。
    他虽为鬼王却不敢拦住这头白象。
    因为他在这白象的身上感觉到了菩萨的念力，这白象是非同小可的神物，他千年的道行亦是不敢招惹。
    “似乎看到一尊鬼神被这白象撞飞了？难道军中还有除了我之外的鬼神么？”长舌鬼王心中疑惑。
    这军中是鬼神的禁地，若非他受了命令，也不太愿意来军营。
    “且去看看。”
    长舌鬼王身子一动，化作阴风向那白象冲出的方向而去。
