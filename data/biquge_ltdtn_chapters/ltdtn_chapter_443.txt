    陆判以身犯险试出斩仙之法的克制手段，本欲逃走，再行谋划，然而李修远和鬼神斗法已经斗了不止一次两次了，现在已经很有很经验了。
    他不再会如以往那般大意，这次来的时候安排了四尊鬼王在这附近埋伏。
    不求他们诛杀陆判，只要拖住一二，等他赶到的时候这陆判就能伏诛。
    天空之中阴风呼啸，本来就是冬季，寒风凌冽，再加上阴风夹带其中，寻常的人只需被吹一下就要遍体冰凉，几欲冻僵，阳气稍弱的人怕是次日就要发烧生病。
    在这阴风之中，四尊鬼王的身影隐约呈现，他们互为犄角，拦住了陆判的去路。
    “你们这些鬼王背叛我的事情还未找你们算账，还敢拦我？当真以为我诛杀不了你们几位鬼王么？人间圣人的斩仙大刀可怕，我手中的这根笔又岂是等闲。”陆判喝到，伸手往宽大的衣袖一抓，一根足足有半人高的毛笔被取了出来。
    这毛笔和寻常的毛笔只是大了一些，并无奇特之处，只是笔尖的一抹朱砂赤红，宛如鲜血一般鲜艳，似乎能滴下血来。
    见到这笔，四尊鬼王脸色一变齐齐后退数丈。
    此笔是阴间的至宝，能改生死簿，判人生死，增人福寿。
    除此之外此物还有一能力，那便是能一笔诛杀任何冤魂厉鬼，对鬼王亦是克制万分。
    “陆判官，我等几位鬼王最先听命的是阎罗君，当初阎罗君任期已瞒，转世轮回去了，是你试图代替阎罗君，执掌阴间，违背了阎罗君留下的命令，致使阴间诸多鬼王反叛，导致阴间大乱，本以为你能知错能改，在阳间重建轮回，然而你今日的举措却是让我等寒心。”
    “与人间圣人为敌，便是与天下正道为敌，天下纷乱已经够久的了，我等弃暗投明追随人间圣人岂能算是反叛？”
    “不错，陆判，你的气数已尽了，敢算计人间圣人，今日岂能不亡？”
    四位鬼王虽忌惮陆判，但却一个言语凌厉，指着陆判的种种不对，并未露出退缩之色。
    这个时候人间圣人也在场，是站队撇清联系的最重要的时刻，此刻若是半分犹豫不决，想做那墙头草，必定会被秋后算账。
    陆判虽然道行高强，但人间圣人的刀也够锋利啊。
    “胡扯，某还不知道你们，是想助人间圣人成大道，好敕封成神，堂堂正正的成为阴间的鬼神。”陆判喝骂道：“给某把路让开。”
    他没有迟疑，当即举起手中的毛笔对着那阵阵阴风一划。
    一道红光从笔尖溢出，如一抹朱砂在半空之中划过，所过之处皆是红光凝聚不散，宛如一道霞光彩虹。
    红光所过之处，阴气瞬间为之一荡，拦路的鬼王当即脸色骤变迅速躲避。
    持剑鬼王一时闪避慢了一步，一条手臂被红光划过，宛如利器斩中，当即落在了地上，化作了一股阴气溃散，这鬼王断臂的疼痛让他忍不住惨叫了一声。
    “谁敢拦某，谁便死。”
    陆判化作狂风呼啸而过，借此机会冲出鬼王的阻拦。
    他不是斗不过这几尊鬼王，十王殿是他创出来的，岂能镇压不了这十尊厉鬼，他忌惮的是李修远。
    虽然他找到了克制之法，可是却他没有回去布置周全，正面斗法的话是定然会被诛杀的。
    “陆判，何必急着走，你的酒还未喝完呢，你不是寻到了克制我的斩仙大刀之法么，为何还要逃走？”李修远的声音突然响起。
    下一刻，一道紫光冲天而起，宛如华盖笼罩天空。
    紫光之中有龙凤的虚影穿梭游荡，发出了龙凤合鸣的声音，而在这紫气的深处，李修远却是大步走了出来，他手中提着一柄古朴的大刀，刀上刻着血淋淋的四个大字，车斤，人山。
    神魂出窍，祭出斩仙大刀，这是他动了杀意了。
    每一次出刀必能诛杀邪门。
    这一次定然也不例外。
    “是李公子来了，这下好了，陆判逃不了了。”长须鬼王见此顿时喜上眉梢，心中却是大松了一口气。
    其他三尊鬼王也是一阵轻松。
    今日被陆判逃了，他们可就要担心日后秋后算账了。
    这个陆判是很歹毒的，以前有阎罗君压着方才无事，现在无人压制，做出什么事情来都不奇怪。
    “李修远？”陆判此刻吓的阴气都溃散了不少，此刻二话不说转身就欲逃走。
    李修远道：“你们护住我的肉身，这陆判我来诛杀。”
    说完，他当即追了过去。
    四尊鬼王立刻应了声，让出了道路，不敢拦在李修远的面前，否则被那紫气之中的龙凤察觉，立刻就会被扑杀。
    “休走。”李修远追了过去，抬手便是一刀。
    一刀斩出，一股力量似白虹，如皓月，浩浩荡荡的奔涌而来，形成了一柄刀的形状。
    此刀能劈开昏暗的天空，斩下腐朽的仙佛，这是李修远道的象征，是他用来治理这天下最锋利的兵器。
    陆判回头看了一眼，吓的肝胆欲裂，这人间圣人果真再次出刀了。
    此刀万万不能挡，也没有鬼神能挡住。
    但却可以避。
    陆判深知这一点，迅速的躲避，不敢面对这可怕的一刀。
    “咻~！”一道白虹掠过苍穹，一闪而逝，但是却劈开了昏暗的天空，驱散了夜里的乌云，将一轮明月和漫天星辰显露了出来。
    一时间，苍穹之上星光璀璨，笼罩大地。
    附近的村镇之后，有老学究夜里未眠，偶见此等异景，不禁惊道：“这是白虹贯日的景象啊，人间有不平凡的事情发生了。”
    人间的确是有不平凡的事情发生。
    这是人间圣人在诛杀一尊恶神，为世间驱除一份邪恶。
    山林之中有精怪，鬼魅，但凡见到这白虹劈开夜空的一幕，皆是瑟瑟发抖，从内心深处的感到恐惧。此等恐惧尤胜天上的雷公诛妖荡魔。
    陆判所化的阴风被这一刀斩中，瞬间就消失了一大半，只留下了一小团阴风在半空之中飞舞盘绕，形成了一个漩涡。
    风漩之间陆判的脑袋凝聚成形，他眼中尽是惊恐之色；“这一刀，斩了我足足一千年的道行啊。”
    鬼神，精怪的道行全在身体上，被斩去一节无不是伤筋动骨，当日青娥被铁山打断了腿都伤了根基，天劫难渡。
    如今这陆判被一刀斩去大半个身子，现在的他道行已经不如鬼王了。
    “竟然还没死，你和乌江龙王一样，都很命硬。”李修远看见那夜空之中一团风漩凝聚不散当即冷冷道：“只是不知道下一刀您能否扛得住。”
    陆判急忙道：“李修远，你诛杀我没有任何的好处，我知道你想要什么，你无非是想得到阎罗君的生死簿和这支笔，此两物我交给你，你放我离开，如何？我被你一刀斩去了千年的道行以后连你麾下的一尊鬼王都斗不过，你何必赶尽杀绝。”
    说完，他身旁一股怪风卷起，一本古朴的书本，以及一根半人高的比墨笔一左一右的飞了出去。
    李修远此刻反而楞了一下。
    可是下一刻见到陆判再次掉头而走的时候，却是立刻明白了。
    这是在壮士断腕，弃卒保车啊。
    “是生死簿，和判官笔，他居然把这两件宝物直接丢了。”
    李修远看了一眼，是追上去诛杀还是这个陆判，还是先取生死簿和判官笔呢？
    此番若是不取的话，落在地上，再想去寻可就得费工夫了。
    这荒郊野岭，谁知道这玩意会落到什么地方去。
    要知道这阴间的宝物便是测算，推演之法也是算不出来方位的。
    “先取宝物，再追杀陆判。”
    李修远丝毫不迟疑，飞了出去，他先取了判官笔，后取了生死簿，接着再向着陆判逃走的方向追杀过去。
    只是一左一右的飞了两道，难免浪费了一点时间。
    而陆判却已经离的很远了，几乎都要失去了踪迹。
    都是神魂之体，速度都是差不多的。
    鬼神有御风之法，李修远有天地之力庇护，能乘龙驾凤而行。
