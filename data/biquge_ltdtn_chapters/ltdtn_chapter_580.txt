    李修远的气息只能影响三丈范围，这三丈范围差不多就是这个禅院吧。
    三丈范围之内能封锁一切道术，一切法力，让一切的妖魔失去引以为傲的道行，瞬间打回原形，只剩下了本体。
    若是寻常的妖怪还好，至少没有了法力还能跑动起来，如狐精，虎妖之类的，但是树妖姥姥身为千年榕树精，还没有修成人身，一旦没有了法力和道行的话这些根须全部都要趴在地上不得动，化作寻常的树根。
    它可比不上望川山的那只千年何首乌精。
    千年何首乌精是特殊，五百年就能跑动，人参娃娃也是一样，当然，这类仙草的劫难也容易被人觊觎，随时都会被人挖取，而榕树却一般不会被人乱砍伐掉，所以上天是公平的。
    “啊~！”
    千年树妖的树根被定住的时候，附近的院子里，一声惨叫声响起，却见聂小倩突然痛苦无比的飞了出去，浑身宛如被烈日照到了一样，痛苦不堪。
    “小倩姑娘，你没事吧。”宁采臣连忙追了过去。
    李修远看了一眼道：“聂小倩只是百年道行不到的鬼，受不了我的气息，她近不了我的九丈范围，带她离远一点就没事了。”
    聂小倩在气息的影响之下痛苦无比，这样下去的话很快就被被他的气息所杀死。
    宁采臣闻言，大惊失色，急忙抱起了聂小倩就往外跑去。
    这抱住之后，他觉得怀中的聂小倩一点重量都没有，浑身轻飘飘的像是要化作一阵风吹散一样。
    “呸，这个傻书生，知道她是女鬼还救，真是脑子摔坏了，刚才如果不是我们及时赶到的话他就已经被女鬼害死了。”燕赤霞呸了一口道。
    李修远道：“不用理会，她是一个很聪敏的女鬼，知道这个时候选择站在哪边，先对付这树妖再说。”
    “树妖在哪，在哪，我来了。”这个时候夏侯武手提宝剑赶了过来，却发现自己手中的道理之剑竟没有用武之地。
    院子里一院子的老树根，可是却不见妖怪的踪迹。
    “你树妖在后山方向。”李修远道。
    夏侯武愣了：“怎么又跑去哪里了？”
    “我去追杀它，你就在这里定住它的根须，这样它就逃不了了。”燕赤霞说道：“夏侯兄，我们一起去。”
    “好。”夏侯武毫不犹豫的提剑又向着后山的方向奔去。
    然而就在他们追杀树妖的时候。
    后山的密林之中，一位不男不女的中年人这个之后一张脸上布满了树皮一样的皱纹，它脸上露出了惊怒之色，因为他能感觉到燕赤霞还有另外一位剑客在向着这里赶来。
    “没办法了。”树妖姥姥不敢托大，毕竟李修远还在这里，只得一咬牙，壮士断腕了。
    “咔嚓，咔嚓，咔嚓。”
    一连串木头断裂的声音不断的响起，那些伸展进兰若寺禅院附近的根须立刻接二连三的被它给弄断了。
    没有根须的拖累，树妖姥姥当即恢复了行动，立刻化作了一棵大树的树心，轰的一声遁入了地下。
    “不好，那树妖逃遁走了，它弃了一部分的树根。”燕赤霞大嗓子从不远处传来。
    “嗯？”
    听到这个声音李修远当即皱了皱眉，他还准备神魂出窍去斩了那树妖的，结果这么快的功夫树妖就来了个壮士断腕跑走了，这树妖经过了上次的事情之后变的越发的胆小怕死了，像是一只老鼠一样的油滑，明明有着千年的道行却胆小怕事，不敢露面。
    系上玉带。
    果然，地上一根根粗壮的树根已经如同死物一样倒在那里一动不动。
    即便是没有了气息镇压，这些树根也失去了作恶能力。
    “这树妖上次弃了树皮就已经损失了根基，如今再自断树根，这道行至少只要折损一百年。”李修远骑着龙马道：“去后山看看，”
    虽说树妖可能逃遁了，但他既然是来诛要的，又怎么能让它就这样轻而易举的逃走呢。
    龙马立刻飞奔起来，越过了院墙，直奔后山而去。
    当他赶到的时候，燕赤霞已经出了法力封锁的范围，御剑飞行带着夏侯武飞进了后山的密林之中，已经失去了踪迹，他追了一段路之后便只能是放弃了去寻燕赤霞和夏侯武了。
    荒郊野外的他不敢随意的出窍，免得肉身便宜了山中的虎狼。
    “这次树妖过于谨慎，一击不中，不会逃窜到其他地方去吧。”他心中不禁有些担心起来。
    对付树妖到是不难，就怕这树妖吓的又跑了。
    这千年的恶妖，纵然是杀不死，也要打的它没有作恶的能力才行，眼下这种程度是远远不够的。
    “不，它应该不会轻易的离开，这种级别的妖怪都是有地盘的，它好不容易在这里站稳脚，不可能轻易的挪窝，它这种样子若是去了其他的地方，说不定碰到了本地的神明又或者本地的妖怪，反而死的更快，如果我没有猜错的话，这树妖一定会躲在地下不出来，等我们离开。”
    李修远心中想到。
    不过就在他思考的时候，忽的周围似乎刮起了一阵风。
    周围的树梢被吹的簌簌作响，发出了沙沙的声音，好像有人在窃窃私语一般，这股风吹来，说也奇怪，只是围绕在白马身边打转，形成了一个漩涡，可是却始终不敢接近白马的身边。
    坐下的龙马似乎有些脾气不好，扬起铁蹄，嘶鸣几声，不安分的在旁边奔跑了一圈，似乎在驱赶着什么东西。
    “怎么了？”
    李修远立刻回过神来，感受到龙马的不安，微微皱了皱眉。
    龙马是很有灵性的，周围有鬼魅妖邪出现都会有察觉，然后通过一些小动作提醒自己，眼下这种躁动不安肯定也是这个情况。
    “夫君~！”
    然而就在此刻，一个轻灵悦耳，带着万分娇羞的声音忽的响起。
    “嗯？”
    李修远神色一动，下意识的寻声看去。
    “铃铃~！”清脆的铃声响起。
    却见月光之下，一位清纯貌美的女子，整个时候正站在一处凸起的土丘之上，这女子身材娇小，皮肤白皙如玉，带着一股缥缈出尘的气息，仿佛随时都要随风而去一般。
    而在这女子的细嫩的颈部下，一副象征着吉祥如意的，金锁儿正挂在胸前，上面有鸳鸯戏水，龙凤交颈的图案，在月光下闪烁着金光。
    “青梅......”李修远顿时愣住了。
