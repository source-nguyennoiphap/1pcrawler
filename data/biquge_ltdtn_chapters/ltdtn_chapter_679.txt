    听着这些书生发癫发狂的乱叫，李修远非但没有觉得瘆得慌，反而心中有几分痛快。
    他们不是说十娘是吃人的厉鬼么，今日便让他们看看什么是厉鬼，也让他们尝尝十娘刚才经历的绝望和无阻。
    而且这样的惩法比起他们的所作所为却是轻得多，他们只不过是被吓唬而已，没有性命之忧，如果你的胆色够强大的话不受惊吓，那么李修远的手段自然是无用，而刚才的十娘却是有被群而害死的风险。
    “公，公子，奴家不想待在这里了，公子你是正人君子，是一位真正的好人，如果公子不嫌弃的话还请带奴家离开这里，奴家愿意一辈子跟随公子左右，服侍公子，虽然奴家这般模样不让人待见，但奴家忠贞，女德却不必其他女子差，还请公子放心。”
    这个时候，身旁的十娘却是乘此机会轻声的开口道，她虽然声音含糊不清，可却能听出她的真情实意。
    说着娇软的身子却是主动的依偎在李修远的旁边，一双细手在昏暗的琴阁内抓住他的手不放。
    十娘觉得李公子是值得托付的人，从之前在华县第一次相遇的时候她就觉得这个男子和寻常的男子不一样，今日再次遇到更是肯定了自己的想法。
    她相信这个男人值得让自己送出芳心。
    “十娘，好端端的你为何说出这话？”李修远有些疑惑起来。
    十娘催泪道：“公子也是嫌弃奴家这般模样么？”
    李修远道；“不是这个原因，你的病症可以医治，而你的美貌更是不俗，能得如此一位佳人倾心，那必定是前世修来的福分，只是十娘你我相见不过两面，为何愿托付我？”
    他对十娘还很陌生。
    虽然做鬼的时候是一个美艳的女鬼，做人的时候一个绝色的艺女，只是说实话他对十娘真没什么影响。
    如果不是十娘提起，他都险些忘记了十娘这个画中女鬼。
    “对公子而言相遇不过两次，但是对奴家而言，公子两次出现却是让奴家毕生难忘。”十娘诚恳的说道：“还请公子莫要辜负十娘的一片心意，否则，奴家也唯有舍弃了这重新做人的机会，化作女鬼继续去山野游荡，也好过被这些丑恶之人玷污。”
    李修远说道：“你的心意我不好辜负，不过我是有妻妾的人了，而且十娘你现在也许只是一时冲动才说出这样的话来，你应该多考虑考虑，不应该随随便便的决定下来。”
    他没有立刻答应，也没有聚集。
    只是因为他觉得和十娘算是第一次相见，这就要以身相许，有点接受不了。
    十娘苦涩道：“奴家的心意公子已经知晓，如果公子不相信的话，过几日再来探望奴家，奴家还是今日这话，绝无更改之意，只希望公子别让奴家等太久。”
    说着那抓着李修远的手却是恋恋不舍的松开了，然后轻轻推开了男人，自顾自的离去了，很快便消失在了不远处走廊的撞角处。
    李修远看着她离去，心中若有所思，不知道是该挽留还是该拒绝。
    最后矗立了一会儿之后，他便轻轻一叹立刻转身立去。
    很快便大步融入了街道之中，消失在了昏暗的夜空之中。
    然而就在他离开之后。
    约莫几个时辰过去了，琴阁之内却是一下灯火通明，挤满了不少看热闹的人。
    “发生什么事情了，这里怎么大呼小叫的。”
    “这群书生好像是饮酒发酒疯了，有些昏死过去了，有些做噩梦大叫有鬼，吵的我等都做不了生意。”
    “掌柜的，赶紧派人通知这些书生的家眷，让他们把人接回去。”
    这里的动静到底是吸引了别人的注意，被鬼神纠缠的这些书生只当做是耍酒疯一般，天亮之前就被人接送走了，余下的几个没有家眷的在本地的书生也被掌柜的安顿好了，不过说也奇怪，唯独有一个叫朱尔旦的书生消失不见了，并没有在这些书生当众。
    朱尔旦被厉鬼纠缠，他胆子的确是被寻常的书生大不少，尽管被厉鬼所吓，可是却一路慌不择路的逃窜，早就从琴阁的楼上跳了下来，一圈一拐的向着其他地方逃去了。
    可事无论他怎么跑，却依然有厉鬼尾随，吓的他叫天不应，叫地不灵。
    路上的行人更是空无一人，无论他怎么走都始终遇不到一个。
    不是他遇不到，而是他被李修远安排的鬼神迷惑了，见到了人也只会把那人当做石头，树木，根本就不是一个活人，所以自然就感觉无论怎么走都始终是自己一个人的缘故了。
    然而当朱尔旦有惊慌失措，哭喊着逃出鬼怪的纠缠时，却是忽的在眼前的街道上看到了一个人，
    一个身穿官服，此人脸色发绿，胡须是红色，相貌尤为丑恶凶恶。
    “又是鬼。”朱尔旦脸色一白，急忙改变方向逃去。
    “呵呵，我不是鬼，我是神仙，之前你们玩扶乩游戏的时候把我请来了，我是那个姓陆的乩仙啊，你贵人多忘事居然把我忘记了。”这姓陆的乩仙笑着说道。
    朱尔旦却是根本没有理会，只是逃的更快了。
    “你现在被鬼神纠缠，惊吓过度，看来得冷静一下才能听我说哈。”姓陆的神仙说道，随后他随后一挥，一股暖风拂面。
    朱尔旦一个激灵，浑身的寒气被驱散了，整个人暖和了起来，同时一瞬间周围追赶着自己鬼怪一下子消失不见了，之前一直是走在无人的街道上四处乱串，说也奇怪，这一瞬间的功夫街道上却又有了人影。
    虽是晚归的寥寥行人，但至少是有人，让他恐惧得到了缓解。
    “这个姓朱的书生怎么一下子气焰如此强盛了，我们迷惑人的手段对他没有用了。”
    “一定暗中有其他鬼神帮助他，道行很高，我们发现不了。”
    “我们是奉命惩戒这个朱尔旦，他有谋害人的恶心，受这惩罚是应该的，这次被其他的鬼神干预了我们却是不好向人间圣人复命了。”
    附近几个窃窃私语的声音响起。
    一股股阴风围绕在朱尔旦身边旋转，卷起一个个漩涡，但却始终没办法靠他太近，一靠近便感觉有一个火炉在炙烤自己。
    这热气既像是阳气，有像是血气，但又不是，不过却能退避寻常的鬼神。
