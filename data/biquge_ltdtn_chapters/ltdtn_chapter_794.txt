    因为昨晚在兰若寺里晃了一圈的缘故，李修远入睡较晚，到了后半夜才沉沉睡去。八一中文 Ｗ≠Ｗ≈Ｗ≥．≠８＝１≤Ｚ≥Ｗ≥．＝Ｃ≤Ｏ≥Ｍ
    而在隔壁的屋子里，身为李家护院的铁山，虽然睡的比较谨慎，可是此刻却也进入了梦乡，他做了一个非常真实，却又非常奇怪的梦。
    在梦中，铁山就在这件屋子里睡觉，忽的一阵凉风从窗外吹来，让他感觉了有点寒意，然后起床站了起来，准备去关一下窗户，然后回来继续睡觉。
    可是当铁山走到窗户前的时候却看到乐窗户外站着一位身穿红色官服，头戴官帽的中年男子，这个中年男子似乎漂浮在半空之中，身后还跟着几个凶神恶煞的恶鬼，这几个恶鬼穿着衙役的衣服，似乎是这个中年男子的手下。
    “可是郭北县李家的护院铁山？我是郭北城的城隍，现在托梦与你，你我正在梦中相见。”这个官员自称为郭北城城隍。
    铁山心中不疑，有些吃惊道：“小的李家的护院铁山，见过城隍大人。”
    “本城隍有要事需要通知李公子，奈何李公子命格极贵，我等鬼神无法近身，只能劳烦你通传一声。”城隍开口道。
    “不知道有什么事情需要小的传给大少爷，还请城隍大人吩咐。”铁山拱手道。
    城隍说道；“在兰若寺内有一女子，名为青梅，她明日阳寿将尽，还请李公子做好准备，料理后事。”
    铁山诧异道：“敢问城隍大人，这个叫青梅的女子和我家大少爷有什么瓜葛，竟要劳烦城隍大人亲自前来通告。”
    “这是天机，不可泄露。”城隍说道：“今日时候已经不早了，本城隍还要回郭北城处理政务，就不久留了，我们走。”
    这个城隍说完便挥了挥手，示意了一下，当即便和身边的几个恶鬼差役化作一股凉风消失不见了。
    而伴随着这股凉风一吹，铁山突然就一个激灵清醒了过来。
    他当即睁大了眼睛，却现自己正躺在木榻之上，被子盖的好好的，根本就没有起来过，而看了一眼房间的窗子，却也是关上了，没有打开过。
    回想梦中的一切，之前睡梦之中的时候尚且不知道害怕，但是这清醒过来之后却是感到无比的后怕，心中亦是有些恐惧。
    可是约莫片刻之后，铁山却是猛地想到了什么，急忙翻身起来，大步冲出了屋子。
    “大少爷，大少爷，大事不好了。”他来到隔壁不断的拍着房门。
    李修远躺在木榻之上，还在熟睡之中，可是房间外的噪声却是把他硬生生的吵醒了。
    稍微赖了一下床之后，敲门声还在继续，吵的他实在是没有办法安睡，便不得打了个哈欠，迷迷糊糊的清醒过来。
    “铁山，这一大早的你什么病，本少爷还在睡觉呢。”李修远看了看外面的天色，忍不住骂道。
    铁山在门外继续道：“大少爷，小的也不想打搅大少爷睡觉，可是今天出要出大事了。”
    “要出大事？出什么大事，进来说吧。”李修远微微清醒了过来，立刻道。
    出于对这个铁山的了解，这个护卫耿直，忠诚，一般没有什么特别的大事不会这样冒失打搅自己的。
    铁山推门而入，急忙道：“大少爷不好了，今天有一个叫青梅的女子要死了。”
    “什么？”李修远听到这句话先是楞了一下，随后诧异道；“你说今天那个青梅姑娘会死？”
    “是啊，今天青梅就会死。”铁山连连点头道。
    李修远说道：“这到底是怎么回事，你且详细说来。”
    “是这样的，小的之前在睡觉的时候晚上突然做了一个梦。”铁山说道。
    “既是做梦，那就是无中生有的事情，区区一个梦境你这般小题大做干什么，打搅了本少爷的好觉，出去，我要继续入睡了。”李修远有些气恼道。
    铁山立刻辩解道：“小的这个梦不是寻常的梦，小的梦见了郭北城的城隍，城隍托梦给小的，让小的转告给大少爷，他说今日一个叫青梅的女子阳寿将尽，小的不敢大意，所以醒来之后就立刻前来通知大少爷了。”
    “什么，你梦到了郭北城的城隍？”李修远顿时脸色一变，当即就站了起来。
    若是其他的梦他肯定不会当真，可是梦到了城隍那就非同小可了，这可是聊斋世界，城隍可是货真价实的鬼神，掌管一城一地的神明，一般人做梦只要是梦到了城隍那么十有**就是真的。
    因为寻常的鬼魅托梦是绝对不敢带着城隍的名号的。
    “大少爷，小的真的梦见了城隍，城隍本来是要托梦给大少爷的，可是城隍却说大少爷命格极贵，鬼神没有办法近身，所以才托梦给了小的，让小的转告给大少爷。”铁山说道。
    听这么一说，李修远当即就信了。
    他身怀七窍玲珑心，的确是鬼神不近，别说托梦了，便是走到自己身边也是一件难以办到的事情。
    “城隍托梦，青梅将死。”李修远目光微动，脑海之中想起了昨日那个俏丽女子。
    很难相信，那个健康无比的青梅仅仅隔了一天就会死去。
    一个人若是无病无灾，怎么可能一日之间死去呢？
    除非是......意外。
    一念至此。
    他当即迅的更衣，连洗漱都不用便立刻大步走出了屋子，然后奔跑着离开了。
    “大少爷，你去哪？”铁山连忙追了出去。
    李修远这个时候还能去哪，自然是去那个青梅姑娘所在的禅院里，看一看这个女子到底怎么回事，是不是出什么意外了？
    如果真是意外的话，只要自己能赶到的话就能救下青梅，至于阳寿将尽，那是一个笑话。
    便宜师傅瞎道人说了，自己天生圣人，蕴含大气运，鬼神不近，阴阳不管，可帮人逆天改命，渡厄渡劫，天生便是救济苍生之人。
    只要他能帮青梅渡过这劫难，那么青梅就不会有阳寿将尽这一回事。
    可是当他刚刚冲出院子的时候，却见到一个清秀的丫鬟一边带着哭腔，一边浑身染着鲜血的往这边跑了过来，因为跑的过急的缘故，在路上还摔了一跤，摔的鼻血都流出来了，可她还是立刻翻身起来，继续向着这边跑过来，眼中只有焦急和慌张。
    小蝶~！
    李修远见此顿时脚步一停，脸色当即就变了。
    难道意外已经生了。
    小蝶见到李修远之后却仿佛见到了救星一样，一边哭着，一边抹着鼻血道：“李公子，不好了，小姐，小姐她出事了，求求李公子，你救救小姐吧，你快救救小姐吧。”
    说着便跪在了地上，抱着李修远的腿，生怕他不相救一样。
    “青梅姑娘在哪？”李修远问道，此刻他也不问生了什么事情。
    不过看着样子，必定是急事无意，问了也是浪费时间。
    “小姐，小姐她现在在那神树旁边......”小蝶带着哭腔说道。
    可是她的话才刚刚说完，李修远便挣脱开了她，立刻便冲向了后院，头也不回的说道：“铁山，照顾一下她。”
    跟过来的铁山本来想要追过去的，可是听这么一吩咐当即就留了下来。
    “李家护卫都死哪去了，还不出来，给你们十息时间，再不出来的话，回去一人五十棍。”铁山见到这女子浑身是血，也知道是出了人命关天的大事，当即怒喝道。
    “在，在此。”
    “铁山队长。”
    当即三四个精壮护卫连衣服都来不及穿便冲了出屋子，齐刷刷的来到了铁山的面前。
    “留下一个人照顾这个姑娘，其他人跟我去保护大少爷，今日大少爷如果出现了一丁点的损失，拿你们试问。”铁山喝道。
    “是。”其他几人心中一凛，急忙道。
    立刻，铁山带着两个护卫追了过去。
