    看见这口空荡荡的大箱子，李修远就知道这绝对不是钱管家拿走里面的银两，绝对是妖魔鬼怪一类。??八?一中文网?  Ｗ≤Ｗ≠Ｗ≈．≥８≥１ＺＷ．ＣＯＭ
    当然，也有可能是一些修道之人施展了手段。
    不过是修道之人的可能性很少，修道之人最忌讳的就是因果，似这般不问自取，拿走了白银一万两，其因果之中，足以毁了他的修道之路，所以绝对不是修道之人为之。
    “四周看看再说。”李修远举着灯笼，在库房四周巡视起来。
    看看能不能找到什么蛛丝马迹。
    “少爷，这里阴森森的，奴婢心中有些害怕，还是赶快出去吧。”小蝶有些害怕道。
    李修远说道：“等等再离开，容我先查探一番。”
    他在这里转了一圈之后，并没有现什么端倪，一切都很正常，也没有找到什么蛛丝马迹。
    如此看来这一万两白银丢失很有可能是鬼魅所为。
    只有鬼魅才能穿墙自如，可以轻而易举的带走银两。
    “看来还得去找师傅问问了。”
    李修远微微摇头到，打算抽空去一趟下河村，看看自己师傅能不能算出什么东西来。
    “走吧，我们离开这里。”
    正当他带着小蝶准备离开的时候，忽的身后的库房之中传来了一声声吱吱的声音。
    是一只老鼠。
    库房之中会有老鼠。
    要知道李家的钱库建造的时候可是格外的坚硬，四周的墙壁都是用大青石砌成，而用来砌墙的泥土也是用黏土混合糯米，鱼胶等物调成的，一旦砌成其坚固无比，足以比得上京城的城墙那般坚固了，根本就不可能有老鼠能够打洞钻进来。
    不过反常必有妖。
    李修远当即转身又返回了库房一看。
    这个时候他瞧见一只白毛老鼠，真个时候正爬到了一口箱子上，捧起一锭白银就往嘴巴里塞。
    足足有成人巴掌大的银锭，居然被这只老鼠硬生生的塞进了肚子里，而看着老鼠的肚子也没有丝毫鼓起的迹象，依然非常扁平，这老鼠似乎还没有过瘾，又继续捧起银锭吃下去，吃了足足三锭之后方才有些警觉起来，脑袋一转看到了李修远大步走来。
    “吱~！”
    白色的老鼠当即一叫，立刻簌簌的从箱子上跃了下来，然后迅的就沿着墙角跑走了，最后低着脑袋往一条只有手指粗细的石缝之中钻去。
    那么小的缝隙居然被这白老鼠硬生生的钻了进去，很快就没影了。
    当李修远赶到的时候却是正好看见了这老鼠的一根尾巴消失在了石缝之中，并没有能逮住这只老鼠。
    “这只老鼠成精了。”
    他最先关注的不是自家丢失了三锭银子，而是更在意这只白色老鼠自身。
    很显然，能一口气吃下和自己体型一般大的银锭，还能钻进这么小的石缝之中，绝对不是寻常的老鼠能做到的，必定是有了道行的妖物。
    “这白鼠至少也要一两百年的道行了，不过即便是有了道行，白鼠没有化成人形，应该不知道银子的用处，对它来说银子就是石头，不能吃，也不能用的垃圾，所以它偷我家的银子必定不是为了自己用，是给别人偷的，只有人才知道金银的好处。”
    李修远沉吟了起来。
    觉得这白鼠背后还有人，或者是受什么人指点了。
    不过偷窃这种事情，修道之人是会犯因果的，必定不是修道之人做的。
    那么这白鼠是谁指使的？
    想了一下，李修远决定还是去好好调查一番，不过这一番查探却是将凶手给锁定了。
    不是钱管家，而是那只白鼠。
    老鼠最善打洞，这有了道行的老鼠更胜，青石缝隙那么一点点都能钻开，也的确只有异类能够办到了。
    “我们离开这里。”
    李修远吩咐一声，带着小蝶迅离开了库房。
    回去之后，他将这样的事情大致和自己的父亲说了一遍。
    李大福顿时睁大了眼睛：“竟有此事，我李家的上万两白银居然是被一只白鼠给盗走的。”
    “是的父亲，孩儿亲眼所见，那白鼠连吞三锭白银，腹部不胀，还能钻进石缝之中逃脱。”李修远说道：“如此能力毫无疑问，那老鼠一只有了道行的精怪，被这样的精怪盯上了钱库，别说上万两了，比那时我们李家钱库之中的所有钱财也要被搬空，所以钱管家是无辜的。”
    “那，现在怎么办？”李大富一时间有些拿不定注意了：“既是精怪作乱，还是去请道长下山抓妖吧。”
    提起自己的师傅瞎道人，李修远的嘴角又是一抽。
    他说道：“师傅在修大道，只怕没有时间，现在当务之急还是去把库房之中的那石缝封死，免得白银再被丢失，至于钱管家的话，还是放他出去，给他一笔银两让他会去养老吧。”
    “石缝好办，不过钱管家既是无辜，为父当去赔礼道歉，再请他继续管理钱库，为何要辞退钱管家。”李大富疑惑道。
    李修远说道：“钱管家经过此事，忠心必定已经动摇了，再让他掌管钱库只怕已经不安全了，而且钱管家岁数也不小了，让他告老回乡，安享晚年也是应当。”
    “呵呵，吾儿真不愧是七窍玲珑心，心思细腻，洞察人心，处理事情来如此的老练，为父却是大为欣慰啊......”李大富说着，可随后看见了后面的小蝶，不禁当即又止住了。
    他险些忘记了，自己孩子拥有七窍玲珑心的消息不能泄露出去，否则一生不得安宁。
    “咳咳，钱管家的事情既已经解决，可是钱库的事情为父还是不放心啊，那石缝封死好办，可若那白鼠再来如何是好？”
    “家里应该有养猫吧，若是有一只猫在库房之中巡视，那便可以高枕无忧了。”李修远说道。
    李大富当即眼睛一亮：“有理，有理，老鼠最怕猫了。”
    “不过等闲的猫是治不住有了道行的老鼠，孩儿需要去准备一番。”李修远拱手道，然后又离开了。
    “少爷，等等奴婢。”小蝶像是一根小尾巴一样跟在李修远的身后。
    李修远来到后院之后，吩咐一位婢女道；“后院的那只黑猫去哪了。”
    “大，大少爷。”那个婢女急忙施了一礼，然后道：“那黑猫估摸着在花园里打盹呢。”
    “去把它抓来，我有急用。”李修远说道。
    “是。”婢女应了声便很快小跑离开了。
    不一会儿功夫，婢女就抱着一只黑猫返回来了。
    黑猫是家养的，并不怕生，老老实实的被人抱着。
    “小蝶，带黑猫去我的书房。”李修远说道。
    小蝶从那婢女手中取过黑猫之后便跟他去了书房。
    “我想想，我想想，那启灵符怎么画的。”
    李修远磨墨，取笔，铺了一张宣纸，脑海之中回想着自己那个便宜师傅以前教过自己的一点东西。
    启灵符！
    顾名思义，是给生灵开启灵智的，这是一门比较高深的符。
    修道之人没有一定的道行是施展不出来的。
    但是李修远是一个另类。
    七窍玲珑心的他有着过目不忘的本事，当即脑海之中就回想起了启灵符的画法。
    学着以前瞎道人的样子，李修远笔走龙蛇，很快一副连他都看不懂的道符就画好了。
