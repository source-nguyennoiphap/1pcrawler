    十日时间转眼就过去了。
    随着赵景登基称帝，先皇罪己诏，退位诏，传位诏三份圣旨传遍京城，传遍天下，有人震惊，有人诧异，更有人愤怒。
    因为只是宫变而已，再加上赵景不留余力的连连颁发圣旨安抚各方官员，各地总兵，大宋过整体上是处于一个还算是稳定的情况之下，没有人想要起兵造反，也没有人跳出来反对赵景称帝。
    有资格范围赵景的，只有京城的那几位皇子，可是随着赵景坐镇皇宫，掌握禁军，那几位皇子早就被软禁起来了。
    只等李修远调韩世忠两万铁骑入京，一切大势都可安定。
    但，这也只是权利接替而已，出了京城，天下的百姓依然凋敝，贪官污吏依然横行，只是鬼神精怪的事情比之前少了很多，很多，再也不会听到哪个村子闹妖，闹鬼死了几十人，上百人这样非常严重的事情
    ，也听不到什么白莲教，弥勒教招募信徒。
    若是赵景登基肯施仁政，治贪腐的话，大宋国还有中兴的机会，倘若只是另外一个赵官家的话，这大宋国的国运很快就会消耗一空、
    改朝换代也就不远了。
    这一切都和李修远没有关系，没有长盛不衰的王朝，一个王朝兴盛衰败本来就是一种规律。
    十日期限一到，李修远便点起了兵马，带上了所有人，便连战死的甲士的棺材也托人依次运出京城了。
    “沙金，你是跟我去扬州，还是留在京城继续做你的镖头？”镖局内，李修远看着众人整装待发，忽的问道。
    沙金道：“大少爷若是不嫌弃的话小的愿意跟大少爷去扬州。”
    李修远点了点头：“那好，那今日就随我等一起出发吧，镖局就交给其他信得过的人打理，京城的镖局还是不能废弃的。”
    “是，大少爷，小的会安排好的。”
    沙金面带喜色，他留在京城只是一个镖头而已，去了扬州怎么也能混个不低的武职。
    一番准备之后，李修远等人很快浩浩荡荡的离开了京城。
    他带着圣旨和任命，准备前往扬州上任。
    计划正常的话，李修远将在扬州治理几十年，直到辞官归乡养老.......当然，若是闲暇之余还得抽空解决另外几件事情。
    就在他刚刚走出京城城门的时候，路旁却是突然看见了一位邋遢老道。
    这老道衣衫敞开，露出胸膛，躺在一处人家屋顶上酣睡着。
    似乎李修远等人的路过惊扰了他，让他不由睁开眼睛，悠悠的睁开眼醒来，他看了一眼李修远笑道：“圣人就要走了么？要不要老道送你一程？”
    他这话似乎只对李修远说，旁人并未听见。
    李修远看了这老道一眼，并不认识他，只是拱手施了一礼：“不敢劳烦道长。”
    老道挠了挠脖子笑道：“贫道看你此番南下路上并不会太平，还是当心一点比较好，如果路上有人喊你的名字，你切莫回头，兴许能够逃过一劫。”
    “嗯？”
    李修远心中产生了疑惑，难道自己路上会遇到什么危险么？
    可是这老道便是修行众人，懂得趋吉避凶之法也应当算不出自己的任何事情才对。
    “道长能否详说？”李修远问道。
    那老道摇了摇头，只是笑着挥了挥手目送他离去。
    此刻战马走过，李修远却是和这老道渐行渐远了，最后便只看见他往后一倒，继续在屋顶上晒着太阳酣睡起来。
    既已走远，见其也没有相告的意思，李修远也就没有多问。
    随后，李修远吩咐道：“吴象你领兵在前，沙金你带队人马在后，路上注意安全，另外每隔二里放出一个斥候，我们只有两千不到的人马，路山需要小心一点。”
    吴象和沙金领命，立刻带着人马一前一后的护送。
    李修远则是带着伤兵以及一队亲兵走在军队中间，确保万无一失。
    “公子，韩世忠的两万骑兵已经在来的路上了，只需不到半日的功夫就能在路上相遇，难道还怕会遇到什么山贼，强盗，拦路打劫不成？需要这么小心应对。”一旁，胡三姐骑着一匹枣红马，跟在左右，她
    丢了个媚眼，娇声笑道。
    “万事听人劝，有一老道说我路上有劫难，我看还是小心一点比较好。”李修远道。
    “是么？”胡三姐眼睛一转：“那老道肯定是信口开河，谁能算到你的劫难啊。”
    “可我也看不透那老道的底细。”李修远道：“那老道看似凡人却拥有凡人没有的气质和神态，看似仙人，却是太过深入红尘，已经有了凡人的俗气，若说是正在修行之中的道人，可也没有修行之人该有的
    气息......”
    “反正路上也没几个时辰，我替你到四周去查看查看。”
    胡三姐美眸一转，却是骑马离开队伍，走到路旁突然化作了一只红狐奔入山林，随后一股妖运腾起，一只笼罩在队伍的头顶，随着队伍徐徐往前飘荡。
    却是她在天上警惕四周。
    而与此同时。
    一处靠近官道附近的小镇子里。
    今日，镇内来了一位身穿白色道袍，一尘不染，仙风道骨的老者，这老者自称是山中仙人，今日特意出山，只为点化一位好友重返天宫，荣登仙境。
    这样的趣闻一下子就将小镇上的很多人吸引了过来。
    “本仙人是三百年前唐朝时期的修道之人，不日方才得道成仙，记得三百年前和本仙人一起修行的还有我一位挚友，只可惜本仙人的那位挚友贪图人间富贵，跑去人间做了官，耽误了修行，最后修行不足不
    得不坠入轮回受投胎转世之苦，今日本仙人已经成仙，所以特来点化本仙人的那位好友转世，与之一起飞登仙宫。”
    “适才本仙人查探得知，我那位好友转世就在此镇，故此今日特来查探。”
    听到这个仙风道骨，自称是山中仙人的老人这么说，看热闹的人顿时有些激动起来。
    “仙人，你看我可是你的那位好友转世之身？”人群之中有一闲汉厚着脸皮问道。
    山中仙人摇头道：“你并不是。”
    “那我呢，我是么？”一位农夫问道。
    “可惜，你也不是。”山中仙人又道。
    接着又有好些个人询问，可是山中仙人皆摇头表示不是，最后叹息道：“看来今日我那位好友又要错失仙缘了，也罢，也罢，本仙人只好孤身一人飞去仙宫了。”
    说完转身便又准备离去。
    可是他没走多远，却又突然轻咦一声，抓住一位路过书生的手，惊喜道：“好友，我找你好些时日了，真是太好了，没想到竟在关键时刻遇到了你，现在不要多问了，赶紧随我去仙宫之中吧，我接引你成仙
    得道。”
    这个书生叫顾生，他此刻正与妻子买菜而归，见到这自称仙人的老者一下子冲过来说出这么一番话，他当即有些摸不着头脑了。
    “道长认错人了吧，我并不是认识你啊。”顾生道。
    “你今生不认得，前世却是和我相交甚密，我们互为知己啊，今日我修行三百年终于得道成仙了，就要登上仙宫了，逍遥快活了，朋友你可愿意随我一同前去？”山中仙人道。
    顾生急忙摇头道：“不，不，不，晚生家有父母，室有妻儿，怎么能抛弃父母妻子跟道长去修行呢，道长还是请回吧，仙宫道长一个人去就可以了。”
    山中仙人闻言长叹道：“唉，当年你就是凡尘之心不死，才错失了成仙的机会，没想到转世之后你还是这样，也罢，也罢，那我也就不勉强你了，既然今日就要分别了，我也没什么东西可以送你的，这是我
    随身的匕首，也算是一件仙家宝物了，就送给你防身吧。”
    “以后遇到厉鬼，精怪，只要念动咒语，这匕首就会飞出，将其刺杀。”
    说完念念有词，手中的匕首立刻闪出一道耀眼的霞光，如一道长虹一样飞了出去，竟将前面路旁的一颗大树给贯穿了一个窟窿。
    “啊。真是仙家宝物啊，顾书生你发达了。”
    “天啊，这么大的树都能穿出一个窟窿，这要是打在人身上哪里还有命在啊。”
    “真是了不得宝物。”
    路旁的行人见此睁大了眼睛，惊呼不已。
    “这是使唤这匕首的咒语。”
    山中仙人对着顾生念出了咒语，然后将匕首收了回来塞到了他的手中，然后便长叹一声，晃晃悠悠的离开了。
    回到家中，顾生看着桌子上的这亮晶晶的匕首，不由笑了：“真没想到，我前世还认识一位神仙，若不是我有父母妻子，我说不定在街上的时候就跟他走了。”
    妻子江蓉看着桌上的匕首，回想了那山中仙人的话，却又不禁咬了咬牙道；“夫君，有一件事情我没有告诉你，以前不说是因为此时机密，如果传出去的话会引来杀身之祸，但是今日我却忍不住想要告诉你
    了。”
    “是什么事情？”顾生问妻子道。
    江蓉道：“我不是本县的人夫君你是知道的，以前我借口说自己带着老母亲是逃难来这里的其实不是，我本是金陵城一位副总兵之女，因为有人杀死了我的父亲，抄了我的家产，我气不过，为父报仇便去行
    刺仇家，结果失败了，后来怕祸及家人就带着仅剩的老母逃难到了这里，之前我苦练武艺，赡养老母，只为有一日能够报仇雪恨。”
    “但我的武艺还不算高强，仇人的势力也越来越强大，我已经感觉报仇无望了，所以嫁给了夫君，替夫君生育孩子，侍奉公婆，如今这位仙人的法宝让我看到了报仇的希望，还请夫君准许我带着这匕首去为
    父报仇。”
    说哇，她咬着嘴唇跪在了顾生的面前。
    顾生愣住了，他不知道如何回答。
    “孩子已经能够说话走路了，母亲也今年因病去世了，家中的事务我都打理妥当了，我现在心中已经没有了什么牵挂，若是我能成功报仇雪耻，再来和夫君团聚，若是不能那么我一定是被仇家所杀，还请夫
    君另娶贤妻，将我忘记。”
    说安，江蓉不等顾生答应拿起了那仙人给的匕首就跑出了屋子，然后步伐矫健的消失在了小镇之中，浑然不像是一个柔弱的女子。
    顾生想要喊住他，急忙追了出去，可是却也已经完了。
    此刻管道之上。
    李修远的军队一路平安无事，所过之处便是山贼，强盗也望风而逃，不出几个时辰，他就已经和韩世忠带来的那两万铁骑的斥候碰面了。
    “将军，韩总兵的人马就在前面五里之外。”伺候抱拳道。
    “来了么？”
    李修远坐在马上，眯着眼睛眺望了一下远处，的确是听到了阵阵战马的轰鸣声传来。
    “很好，一切如计划所料的一样，只要这两万铁骑入驻禁军大营，京城的局势就算是彻底稳定下来了，那个赵景也不用担心被人一脚从龙椅上踢下来了。”
    “加速前进。”
    不多时。
    韩世忠领两万铁骑出现在了眼前，浩浩荡荡，黑压压的一片，旌旗林立，杀气腾腾，这样一支由李修远耗巨资打造出来的铁骑，不但装备精良，而且凶猛敢战，是大宋国第一劲旅，无出其右。
    就这样一队人马，便是面对二十万大军也有一战的资本，而且胜算还比较大。
    “大少爷，我们来了。”这个时候韩世忠让大军停下，自己带着一队人马直奔而来，前来迎接李修远等人。
    李修远见到韩世忠骑马奔马，笑了笑，也准备迎上去，而这个时候，官道之上却传来了一个声音。
    “你可是金陵城李修远？”
    “嗯？”
    李修远骑马路过，等听到的时候声音已经在后面了，他下意识的回头一看。
    却见官道旁边有一位盘着头发，衣着朴素的妇人，正看着自己，看其样子应当是附近村镇路过的百姓。
    但是这妇人却突然从怀中摸出了一柄亮晶晶的匕首，然后嘴中念念有词起来。
    “刺客？”
    李修远瞬间反应了过来，与此同时天上留意的胡三姐也立刻卷起一阵妖风吹卷而下，周围的甲士也瞬间脸色一变，怒视了起来。
    “有刺客，保护将军。”
    “该死的，那下这妇人。”
    一声声大喝响起。
    但随后那妇人手中的匕首却化作一道霞光直奔李修远而来。
    “飞剑？”李修远眸子一缩，几乎本能的反应伸手试图拦下。
    以他的武艺，三石已下的箭矢都能抓住，但这匕首飞来却奇快无比，几乎在他手掌还未抬起的瞬间就已经飞了过来。
    “噗嗤~！”
    匕首瞬间没入了他的胸膛，李修远身躯一颤，坐在马背上的他立刻跌落了下来。
    “成功了。”江蓉见此心中狂喜。
    但随后一股妖风吹来，直接将其卷起然后抛飞到了空中，接着重重摔下，立刻就摔的浑身疼痛万分，当场吐血。
    “妖妇，老子杀了你。”一位麾下甲士牙呲欲裂，目中满是悲怒之色，他骑马而至，拔刀就斩，欲取其头颅。
    “住，住手，别杀她。”李修远那有些虚弱的声音忽的响起。
    这甲士闻声一惊，下意识的收回长刀，一刀斩在一旁的土地上，画出了一条狰狞的口子。
    虽不杀，但随后附近如狼似虎的甲士却顷刻之间涌来，刀枪齐用，瞬间就将倒在地上吐血的江蓉给死死的摁在了地上。
    “公子。”
    胡三姐立刻显化出来，她跑了过去，将倒在地上的李修远急忙扶坐了起来。
    李修远脸色苍白，嘴中咳血，亦是有些难以置信的看着胸口这柄深入血肉的匕首，鲜血汩汩不断的冒了出来。
    胡三姐见此立刻惊慌失措起来，她伸手捂住伤口，却发现伤口怎么都捂不住：“公子，快，快止血，服用千年何首乌......没事的，公子你一定会没事的。”
    说着，说着，她竟不断的低头垂泪起来。
    她是千年狐精，哪里不知道眼下这种情况意味着什么，这匕首什么位置不刺，却刺他的胸膛，要知道上次诛杀鬼工头的时候，李修远身上的这铠甲就被那鬼工头打出了一个窟窿，虽有修补，但已不是龙鳞，
    没有无坚不摧的防御。
    而这匕首却正好顺着那缺口刺入，看这位置似乎已经刺到了心脏......
    李修远虚弱一笑，他抓着三姐的手道：“没用的，何首乌精早就用完了，上次给三姐你服用的是最后一块了，其他的都给我父亲煲汤了，咳咳，看来时候到了，我的恶报来了......那老道人提醒的很对，我
    路上的确是会遭遇劫难，回头必死。”
    “胡说，你怎么会死，你可是人间圣人啊。要死也应当是老死才对。”胡三姐流泪道。
    “三姐你很清楚，我三丈之内之内法术无效，但这飞剑却恰恰无视了我气息的影响......咳咳。”李修远感觉胸口一阵疼痛，浑身上下的力气仿佛都被抽空了一样，竟提不起一丝。
    胡三姐正欲安慰，这个时候韩世忠，沙金，吴象等人已经齐齐赶来，他们跪在地上，又急又怒，看着李修远不知道该如何是好。
    “少，大少爷......”韩世忠声音在颤抖，他看那柄刺入胸膛的匕首，竟也忍不住流泪起来。
    “去，把那行刺我的女子带来。”李修远靠在狐三姐怀中，有气无力道。
    很快，江蓉灰头土脸，狼狈不堪，口角流血，被三五个大汉死死的摁住抬了过来，同时两柄利刀更是架在她的脖子上，只要她有异动就能割下她的脑袋。
    “你是何人，为何刺我？”李修远眼皮一抬，看向她道。
    “金陵城江副总兵是我父亲。”
    江蓉恨恨道：“你杀我父亲，抄我家产，让我还有我母亲没有安身之所，我每日都恨不得杀你报仇，今日老天有眼，我总算是办到了，便是死也算是值了。”
    “为父报仇么？咳咳。”李修远又咳出几缕鲜血，他道：“为父报仇的确是名正言顺，看来有人让我应劫在你的手中，你走吧。今日我不杀你。”
    “什么？”江蓉睁大了眼睛。
    她已经做好了处死的准备，没想到李修远竟放过自己。
    “你们放了她。”李修远挥了挥手道。
    “这妖妇行刺大少爷，放不得，小的建议将其严刑拷打，追问幕后主使，然后凌迟处死。”韩世忠咬着牙道。
    “没这个必要，放她走，怎么，难道我受了伤命令已经不管用了么？”李修远盯着他道。
    “大少爷......”韩世忠跪在地上，磕头道；“事关大少爷的生死啊。”
    “韩猛，执行命令。”李修远道。
    韩世忠只得咬牙喊道；“尊大少爷令，放，放了这妖妇。”
    很快，江蓉被放开了，但附近的甲士却依然恨不得将其剥皮饮血，一个个满是杀意的盯着她。
