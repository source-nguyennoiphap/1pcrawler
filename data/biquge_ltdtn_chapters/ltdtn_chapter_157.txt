    “回公子，小女子本是南方某县士绅家的女儿，只因那一日偶然外出，见到了街上卖货的货郎，本想买几件女儿家的用物回去，却被那货郎以取货为由骗进了一偏僻小巷，那货郎见到四处无人却是不知道从
    哪里取出了一张纸符贴在了身上，待小女子反应过来的时候却已经不再是女儿身了，而是变成了一头山羊。”
    说到这里秋容又忍不住伤心难过起来：“小女子变成羊之后被那货郎一路驱赶，甚至还见到他以同样的方法诱骗其他的女子，待那货郎将小女子赶到京城的时候，一路所诱骗的女子已有十几位之多，那货郎
    将其他女子变成的山羊卖给了某些贩子，而且价格都出的极高，都是几十，上百两银子，已经超过了一头羊的价格。”
    “小女子想来，那些买羊的人定然是知道这些羊都是人变的，故此舍得花钱购买，今日那货郎准备卖掉小女子的时候，小女子趁那货郎谈价之际跑了出来，偶尔在路上见到这里有一座寺庙，便想着向高僧求
    救方才跑到了相国寺内。”
    “之后幸亏遇到了公子，方才被公子搭救得以恢复人身。”
    秋容一五一十将自己发生的经历说了一遍。
    那羊贩就是货郎。
    以卖女儿货物为由，游走在南方各城镇之间，再施以符咒，将颇有姿色的女子变成山羊，然后驱赶到京城，远离女子家乡的地方贩卖。
    其实事情并不复杂，只是那羊贩掌握了旁门左道之术，以法术拐卖有姿色的女子而已。
    李修远听完之后认真的说道：“自古以来，历朝历代都有拐卖妇女孩童的人贩，但历朝历代都制以严刑，布以厉法，对于这样的人都是严惩不贷的，若是逮住，连报官都不需要，打死也不犯罪，大宋国也是
    如此，秋容姑娘你的遭遇值得同情，可是受害的女子还有，此事既然是我遇到了不会不管的。”
    “我已经遣派鬼神前去抓拿那羊贩了，不出所料的话不日就会有消息传来，在那之前希望秋容姑娘能在京城暂留几日，等此事结束之后，我会派顺风镖局的人送你回家去，有镖局的人护送，姑娘你路上不会
    出什么意外的。”
    秋容感激不尽道：“小女子在京城人生地不熟，无所依靠，承蒙公子搭救，一切尽数听从公子的安排。”
    李修远点头道：“秋容姑娘你且放心，回头你可以书信一封，我遣人给你送回家去先报个平安，亦或者可以让你的家人来京城接你，一切皆有你做主，如今这天色也不早了，秋容姑娘奔波劳累许多日，怕是
    身心乏累了，还是早点休息吧。”
    秋容点了点头。
    李修远看了看院子，却才想起来了这后院就只有两间卧房，一间是他的，一间是女鬼小谢的。
    “不过有件事情却是有些抱歉了，这镖局的情况秋容姑娘你也看到了，前院都是镖局的人住的地方，后院还算是清净几分，那边是一个叫小谢姑娘的厢房，也是暂住在这里的，如果秋容姑娘不介意的话能否
    和这位小谢姑娘共处一室？当然，若是秋容姑娘介意的话我可以派人送姑娘去最近的客栈歇息。”
    “多谢公子关系，小女子愿意和那位小谢姑娘共处一室。”秋容想也不想就道。
    她变成羊之后见到了太多人心险恶了，好不容易得救，哪里还敢胡乱走动。
    李修远点了点头：“那我去问一问那位小谢姑娘的意见，还请秋容姑娘稍等片刻。”
    他说完便走到对面屋子敲响了门。
    “小谢姑娘可在屋内？是在下，李修远。”
    屋内幽静无声，门窗久闭，仿佛许久都没有人居住。
    可是片刻之后屋内突然亮起了烛光，一道影子投射在了门窗上，从影子的形状可以判断，这是一个女子的身影。
    “是李公子么？”小谢的声音响起，影子动了动，盈盈施了一礼：“小女子有礼了，不知李公子这么早敲门拜访所为何事？”
    李修远将那位秋容姑娘的事情说了一遍，然后道：“如果不打搅的话，能否让这位秋容姑娘借住几宿？”
    小谢听了秋容的遭遇却是幽幽一叹：“没想到这位姑娘却是这么可怜，幸亏得遇李公子，要不然的话还不知道会流落什么样的地方，只是我的情况李公子想来也知晓，身份的差别若是和我住在一起的话总归
    不是一件好事。”
    “只是暂住几日而已，我很快就会将其送回家乡去，不会有问题的。”李修远道。
    他知道人鬼有别。
    人鬼不能相处在一起，否则很容易生病。
    可只是几日的话却是没有关系，而且这秋容还进过相国寺，沾染过佛门气息，抵挡几日小谢身上的阴气还是不成问题的。
    “那就请这位秋容姑娘进来吧。”
    忽的，一阵凉风吹起，却见房门后的门栓落下，木门无人自开。
    房门打开之后却见一位阴柔，貌美的女子立在门内，一双美丽的眸子幽幽的看着李修远。
    “秋容姑娘进去休息吧，在下就先回屋休息去了，不打搅你们了。”李修远施了一礼，便立刻转身离去。
    小谢却是开口道：“李公子这刚来就要走了么？难道我的身份就这么让李公子你感到害怕么？”
    李修远楞了一下，随后笑道：“我并不感到害怕，只是小谢姑娘的有些执念也该放下了，我麾下的鬼神已经开始入驻京城了，之前的牛马二神小谢姑娘应该也已经看到了，他们的职责就是抓捕逗留在人间的
    冤魂厉鬼，所以等京城的事情平息之后，我想留给小谢姑娘你的时间可不多了。”
    说完他便回房休息去了。
    小谢见到李修远回房，又是幽幽一叹：“为什么世上会有你这样的男子，若是你是一个寻常的书生那该多好啊，偏偏又不知道从哪里学了一声抓鬼除妖的本事。”
    随后她有看向了秋容道；“如果不是看在李公子亲口恳求的份上，我是不会允许你和我同住的。”
    秋容低头垂目，没有反驳。
    “唉，是我任性了，我又怎么能刁难你呢，快些进来吧，夜里外面有些凉。”
    “多谢小谢姑娘。”秋容施了一礼道。
    “不用客气，这里本来就是李公子的府邸而已，我也只是借住的，你要谢就谢李公子吧，我并不值得你感激。”小谢道。
    当秋容走进卧房的时候，四处打量了一眼。
    却见和屋内的家具，布置都很陈旧，好似都好像放置了几十年一样，一些地方的木漆都斑驳掉落，如果不是这里还算干净，一尘不染，她都怀疑自己走进了废弃了几十年的宅子一样。
    “你就去一旁的偏房歇息吧，这里没有伺候人的下人，有什么事情就自己解决吧。”小谢道。
    不过在这个时候。
    李修远已经入睡了。
    但是这一日他又做了一个梦。
    距离上次做梦已经有一段时间了。
    还是接着上次的那个梦，梦境之中他身处于一片桃林，这里屋舍俨然，百姓安居乐业，一切既熟悉又感觉很陌生。
    李修远还是如上次一样站在了一间铁匠铺前。
    一位老铁匠正在修补打造一柄断裂的大刀，一锤一锤的敲敲打打，然后又放进了火炉之中煅烧。
    火炉无烈火，只有一小撮火苗。
    温度无法烧红刀身，铁锤敲打也无妨让这刀身变形。
    “老人家你这样敲打刀身是修补不好的，你要增加炉火的温度，还要添加其他的镔铁融入刀身之中去，才能重新将这刀锻造好。”李修远开口提醒道。
    老铁匠笑道：“我当然知道，可是这里就这么一件铁匠铺，铁料已经用完了，再也寻不到了。”
    李修远说道：“别的地方也寻不到么？”
    “别的地方寻不到，到是你手中却是有一块。”老铁匠道。
    李修远闻言，蓦地低头一看，却见自己的手中不知道什么时候握着一块石头，金灿灿的，分外醒目。
    还未来得及说话，却听那老铁匠继续叹道：“便是你这一块也不够啊，还得再寻一块才行。”
    “那我应该到什么地方去寻呢？”李修远正欲开口询问。
    忽的，耳旁响起了一声金鸡啼鸣之声。
    瞬间，他眼睛睁开从梦境之中清醒了过来。
    “又做梦了，还是同一个梦。”李修远皱起了眉头，揉了揉脑袋：“不过再想回忆却又记不起梦境之中发生了什么，古怪，古怪，改天是不是要去找师傅问一问？”
    “咯，咯咯~！”
    忽的，这个时候门外突然响起了雷公的叫声，不知道什么原因这只红色的大公鸡啪嗒撞击着自己的大门，显得很是急躁，仿佛有什么事情发生了一样。
    “大清早的雷公你吵什么？”李修远没办法只得起身开门。
    自己在门外施了画地成牢的法术，这雷公便是再有灵性也闯不进来。
    大门一开，他立刻就闻到了一股腥味，像是羊膻味。
    这股味道很浓烈，让李修远立刻就皱起了眉头。
    不对。
    这是水鬼留下的味道。
    他大步走了出去，刚出门的时候就看见自己房门的台阶旁边留下了一个个湿漉漉的脚印，似乎昨晚有人在自己门口踱步走了好几圈。
    再看院子里，竟是密密麻麻的湿脚印，而且庭院的角落里还散落着一些灰烬。
    像是纸灰燃烧之后留下的。
    “昨日夜里有群鬼入门。”李修远神色微变。
    他昨夜熟睡了，而且还在做梦，根本就不知道外面发生了什么事情。
