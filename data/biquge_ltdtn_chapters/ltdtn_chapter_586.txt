    聂小倩~！
    这个白衣女子出现之后，李修远立刻就注意到了她的存在。
    如此无缘无故出现一位绝世美人，唤作是谁都不可能不留意，而且确切说来，李修远今日才算是正式见到这个传说之中的女鬼。
    不得不说，这个聂小倩也的确是很特殊的存在，仅这一副容貌身姿，就足以让无数人为之心动了，若是这样的美人再主动的投怀送抱的话，怕是天底下正常的男人，能不被她诱惑的怕是连手指都掐算了过来。
    别的不说，李修远就知道这兰若寺一百年后，会出一个叫小卓的女鬼，而且和聂小倩容貌一模一样。
    但是这女鬼小卓却硬是将一个不懂感情，从小生活在寺庙之中的小和尚十方在几天时间之内就动了凡心，爱的那个小卓死去活来。
    一个从小出家之人尚且如此，更何况是世俗之人了。
    “小倩。”宁采臣见到院子门口的聂小倩，顿时惊喜若狂，一下子就有些激动起来。
    聂小倩螓首微催，有些犹豫不决的徘徊在院子外，不敢进来。
    “小倩，你赶紧过来吧，李公子他是一个好人，他不会把你当成恶鬼消灭的，你尽管放心。”宁采臣说道。
    “这位公子想必就是李修远，李公子吧，我以前听青梅姑娘提起过，青梅姑娘便是死后变成了女鬼，也一直对李公子念念不忘，说李公子是天底下最好的男子，今日一见，李公子的气度和的确是让鬼神都折服。”聂小倩对着李修远施了一礼。
    青梅闻言顿时露出了几分羞意。
    到是李修远冷静非常道：“听你这话的意思似乎是专门在找我的？”
    “李公子心思灵活，一眼就看出了我的意图，我这次的确是专门来找李公子的，还请李公子帮我一个忙。”说完，聂小倩忽的跪了下来，一副楚楚可怜的样子说道：“如果李公子不帮，我今日只怕是必死无疑了。”
    “小倩，你这是做什么，你快起来，李公子肯定会帮你忙的，别看李公子一副冷淡的样子，实际上他的心肠可是很好的，之前在郭北城的时候他出钱出力，不知道救了多少人，这点小事他肯定不会放在心上的。”
    宁采臣急忙道。
    李修远打断了他的话道：“那得看什么事情才行，这里鬼怪的事情怎么能和郭北城的救灾相比，这是个人的私事，而且还不知道好坏呢，若是好事我自然会考虑一二，若是坏事，那我坚决不会答应的。”
    “李公子你放心，小倩是心地良善的鬼，所求之事绝”
    宁采臣被迷的神魂颠倒，他可没有，还不知道什么事情哪里会立刻答应下来。
    君子一诺千金，要么不答应，要么答应了的事情就一定要做到。
    “聂小倩，我不知道你来这里有什么目的，打算做什么，但是在这之前我已经帮了你一个忙，将你的骨灰坛从那后山的榕树之下带了回来，你现在已经不受那树妖的控制了，我想，你现在应该算是已经自由了。”李修远道。
    聂小倩顿时楞了一下，随后有摇头道：“李公子的一番好意，我铭记在心，纵然我因为李公子的帮助得到了自有，那也是片刻的自由，只要姥姥还在，它一定会来寻我回去的，即便是我逃到天涯海角也逃不过它的手心，到时候我若再被姥姥寻到的话，那边是必死无疑。”
    说完又有一些悲凉起来。
    树妖控制骨灰坛只是方便操纵自己的手段而已，但是千年树妖真正依仗的却不是这种不上台面的小道，而是那近千年的道行。
    这样的妖怪要去抓捕一只女鬼，身为女鬼的聂小倩又能逃到哪里去呢？
    李修远闻言，略有所思，觉得这话是对的。
    记忆的故事之中聂小倩真的没有逃走的机会么？
    并不是，她有很多次机会，只是没有做罢了，而这根本原因不是因为骨灰坛，而是因为处于对千年树妖的畏惧。
    “那么你今日来找我的目的是？”李修远又道。
    聂小倩跪在地上道：“恳求李公子灭了树妖姥姥，救我出苦海。”
    “灭树妖是我正在做是的，不需要你恳求我也会去办，你这一次却是多余了。”李修远道。
    “事情并非如此，而是今日姥姥吩咐我勾引李公子，务必将李公子带到后山的山林之中，在那里，姥姥已经准备好了陷阱等待着李公子的到来，除此之外姥姥还去请了一位非常厉害的妖怪来当帮手，一起联手对付李公子。”聂小倩道：“这次姥姥铁了心是要杀死李公子。”
    “天底下想杀我的千年大妖多的去了，这棵千年榕树在我眼中算是威胁最小的。”李修远道：“不过我想知道树妖去请的帮手是谁？你知道么？”
    “我也不知道，我只知道姥姥称呼它为黑山老妖，是一个道行非常高的妖怪，可能实力都超过了姥姥。”聂小倩说道。
    李修远听到这里却也算是明白了事情的始末。
    “如此说来，你的任务就是勾引我去后山，那树妖和黑山老妖已经在那里做好了准备，想要将我杀死？”李修远道。
    “是的。”
    李修远说道：“既然如此，那你为何又要将此事告诉我，如此重要的事情若是泄露出去被树妖发现的话，你可是必死无疑。”
    聂小倩凄凉一笑：“我没有办法，只能是冒险一试了，继续待在姥姥的身边只有死路一条，而且如果不将此事告诉李公子的话，以我的手段怎么能将李公子引到后山里去？”
    “毕竟我和青梅一样，连接近李公子的能力都没有。”
    她有着近百年的道行，可是却还是和青梅一样没办法近的了李修远的身。
    之前李修远的气息散发出来，九丈之内鬼神不近的景象她可是看的一清二楚，而且被气息影响之后的感觉无比痛苦，比死还难受。
    这样的人，她便是有着再高明的手段也不可能将其诱惑。
    总不能凭三言两语就能把事情给办了吧。
    而且连姥姥都深深忌惮，甚至是畏惧的人自然是非比寻常，岂是聂小倩这个小小女鬼敢招惹的。
    “留在姥姥那里必死无疑，与其如此，倒不如那和姥姥拼了，所以今日还请李公子出手诛杀姥姥。”聂小倩道。
    将消息泄露给李修远，就是想让他将计就计，今夜就去除去树妖，如此也就两不耽误，既把人吸引过去了，完成了树妖的命令，又两边不得罪。
    李修远笑道：“你背叛了树妖，眼下似乎并不是一个很明确的做法，若是树妖真的找来了黑山老妖，你或许效忠它的好处或许更多一些，而且你却选择站在我这边，说实话，我们的虽然看上去人多势众，但实际却是实力非常寻常，所有人加在一起都比不上树妖的那千年道行。”
    “可是这里一切都有李公子，只要李公子还在，那树妖永远都会对李公子畏惧。”聂小倩说道。
    “呵呵，畏惧来源于死亡，树妖它很清楚在，这次很有可能会死在我的手中，所以才对我敬畏无比。”李修远道。
    听这话的意思，怕是上次伐了树妖的树皮之后让这树妖已经彻底畏惧起了他。
    这是纯粹被打怕的。
    “李公子，你就帮一帮小倩吧，她也是一个可怜的女子，被那妖怪控制了，身不由己，不得不听从那树妖的吩咐。”宁采臣此刻替聂小倩求情道。
    李修远还是没有答应宁采臣。
    他太年轻了，想的事情还不过全面。
    李修远却是颇有一些老谋深算的意思，对于鬼神的话从未立刻就相信，而且这聂小倩的话是真是假，也得不到验证。
    难保，这又不会是聂小倩的计谋之一。
