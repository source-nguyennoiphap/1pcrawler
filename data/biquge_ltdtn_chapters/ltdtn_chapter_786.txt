    李修远的房间床榻摆放的位置极为讲究和巧妙，到了早上辰时时分，窗外的第一缕阳光照射进来，正好落在了床头之上，可以悄无声息的叫醒正在熟睡之中的人。八一?中?文 Ｗ≥Ｗ≠Ｗ≤．≥８≤１＝Ｚ＝Ｗ．ＣＯＭ
    此刻，被阳光一照射，李修远眼皮微微一动，意识渐渐清醒过来，按照平日里的习惯定然是要起来洗漱的，不过却因为这几日没有好好休息的缘故，今日却是有点想要赖床了，便脑袋往下埋了埋想要避开这烦人的阳光，继续熟睡一番。
    可是当李修远脑袋一低的时候，却现自己被一头秀的螓给抵住了。
    “嗯？”有些迷迷糊糊的伸手摸了摸，大手所过之处只觉一片娇软细腻，同时一股幽香从锦被之中飘了出来。
    下意识的抓了抓，却听见怀中一个娇羞的声音细细响起：“少爷，轻点。”
    听到这个声音之后，李修远当即双眸一睁，惊醒过来了。
    此刻，他却瞧见一个双眸明亮，清秀可人的丫鬟，此刻正红着脸缩在自己的怀中，微微抬起眸子看着自己，有几分紧张和羞涩。
    “小，小蝶。”李修远这才想起来了，昨晚丫鬟小蝶要给自己暖脚，自己不让便相拥而眠。
    虽说昨晚不觉得有什么，可是睡了一觉清醒了不少之后，却觉得有些尴尬起来。
    “小蝶，你这么早就醒了？”李修远不好意思的问道。
    “奴婢昨晚睡的香甜，所以很早就醒了。”小蝶红着脸细声细语道，一双眸子时不时的偷偷看着李修远。
    “既然已经醒了，那就起来吧，我今天也应该去求学了。”李修远说道，便欲起身。
    “少爷，先别。”小蝶忙抓住锦被试图制止。
    可是李修远已经坐起来了，锦被被掀开，顿时躺在旁边的小蝶立刻暴露出了无限春光，看的他眼睛都直了。
    小蝶娇羞无比，细手想要遮挡，可是看见少爷看的入神却又不敢遮遮掩掩的，怕扰了少爷的兴致，一时间动作僵住了，有些不知所措起来，只觉自己的芳心狂跳不止，浑身有些滚烫起来。
    “咳，咳咳。”李修远急忙将锦被盖在了小蝶的身上，尴尬无比道：“你的衣服怎么没了。”
    小蝶知道自己浑身上下被少爷看了个遍，羞的埋在锦被里，小声回道：“奴婢昨晚被少爷搂在怀中，睡的有些热，不知不觉便把衣服给脱了。”
    “原来是这样。”李修远看了看旁边，却见软塌之下散落着好几件女子的衣衫，显然是小蝶夜里不知不觉脱下的。
    他翻身起来，却是将地上的衣衫捡起，丢到了软塌上。
    “穿好衣服起来吧，别那般害羞了，你现在可是我的贴身婢女，早晚都是我的人，用不着那般避讳。”李修远说道。
    “是，少爷。”小蝶红着脸，伸出一只纤细的玉臂，将软塌上几件贴身的衣服抓了进去，然后在锦被里穿戴起来。
    不过就在这会儿功夫，李修远已经走出了屋子，准备洗漱起来。
    见到李修远屋子门打开了，立刻便有婢女端着热水，送上洗漱工具走来。
    “大少爷今日起的可真晚。”婢女抿嘴笑道：“是不是那位妹妹的缘故，听说大少爷昨日带回来一个好女子，收做了贴身婢女。”
    “巧云，你到是消息灵通，连这事情都知道。”李修远在这个巧云丫鬟的伺候下，洗漱起来。
    “奴婢哪能不知道，今儿早上府上的姐妹们都议论着呢，大少爷也真是的，若是缺贴身婢女何不到府上的姐妹之中选一个，也好全了我们的心愿，平日里几个姐妹大胆，都摸上了大少爷的榻，还被大少爷赶了出来，弄得那几个姐妹伤心死了。”巧云说道。
    “她们都是良人，将来都是要嫁人成家的，我若祸害了，以后她们既嫁不了人，也在李府得不到名分，时间久了，岂不是白白蹉跎一生。”李修远拿着猪毛刷，沾着青盐一边漱口，一边说道。
    巧云幽幽道：“便是一辈子伺候大少爷又如何，奴婢这些个姐妹也心甘情愿，再说了，奴婢们日后就算是嫁了人，哪有比得上大少爷分毫的，不过是一些粗鄙，懒散的丑汉罢了。”
    “你还小，以后就知道了，再说了，我武艺没有成，近不得女色，总不能让你们这些姐妹们干等吧，到时候青春不在，那不是被我误了一生。”李修远说道。
    府上的丫鬟，婢女的确是颇有姿色。
    只是奈何他被自己的师傅瞎道人坑的练了什么童子功。
    以后是要以武入道的，再加上又知道了自己来到了是聊斋世界之中，更是不敢松懈了，怎么样也得修炼有成之后再说。
    不过李修远虽然克己修身，可是因为平日里待人和善，出手大方，再加上心底善良，又年轻英俊的缘故，府上的丫鬟进来一个，便掳走了一个丫鬟的芳心，似今日这般，和丫鬟巧云聊起来，也丝毫没有摆大少爷的架子，而是平等对之。
    于是在这个尊卑严重的世界里，李修远便很容易显得鹤立鸡群，与众不同，自然也能让女子为之趋之若赴。
    “大少爷什么都好，就是心太善了，奴婢几个福分浅薄，得不到大少爷的青睐。”巧云有些幽怨道。
    李修远笑了笑却是没有多说什么。
    他不能因为这些丫鬟婢女的爱慕就把她们全部收纳了，这以后李府岂不是要乱套了，而且她们大部分也不是因为真的爱慕，只是想要找一个好归宿而已。
    这是这个世界绝大多数女子的想法，没有那么多情情爱爱在里面，有些女子婚配，甚至连男人的面都没有见过一回。
    这对穿越过来的李修远是没有办法接受的。
    “少爷。”
    就在李修远刚刚洗完脸之后，却见到小蝶俏脸微红，秀有些凌乱的从屋内走了出来。
    李修远点了点头：“过来洗漱吧，待会儿我带你去见我父亲。”
    “是，少爷。”小蝶说道。
    旁边的巧云见到小蝶从大少爷的屋内走出来，眼中流露出了无比羡慕的神色，她知道大少爷是不可能留女子到自己屋内休息的，可这个小蝶昨晚却留了下来，这意味着这个小蝶日后一定是大少爷的人，虽说这金银锁儿不一定能够享到一副，可是枕边人却是少不了的。
    片刻之后。
    李修远带着小蝶吃了早餐，方才去拜见自己的父亲李大富，将小蝶的来历身份说一下，同时也好确定府上的身份地位。
    既是贴身丫鬟，自然是要查明身世，家底，李家虽然只是商贾之家，但多少还是比较注重门风的。
    当李修远来到大堂的时候，却是瞧见自己的父亲李大富正在对着钱管家火，怒骂，显得十分的愤怒。
    钱管家跪在地上却是低着头，面如死灰，似乎犯下了什么不可饶恕的大过错。
    门外候着的丫鬟们更是战战兢兢，不敢靠近。
    “父亲，大清早的为何事如此动怒，若是气坏了身子可就不好了。”李修远说道。
    “吾儿来了。”李大富见到李修远走来，脸上的怒意当即收敛了很多，随后又道：“还能为什么事情，这钱管家吃里扒外，我李家钱库重地，竟在一月之间丢失了足足白银一万两，你说为父应不应该动怒。”
    “什么？”
    李修远也是一惊。
