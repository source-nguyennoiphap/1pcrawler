    天色已经黑了，只能勉强看见地上的路面。
    两匹快马从郭北县的方向奔来，在夜色的笼罩之下向着兰若寺的方向飞奔而去，速度很快，可以说是马不停蹄。
    这两个人是李修远派去兰若寺取金身的护卫。
    一人叫马东，一人叫牛二，都是跟谁李修远去过鬼市，进过楚氏陵园，到过华县，遇到过鬼娶亲的人。
    可以说两个人的胆子是练出来了，对于鬼怪并未畏惧之心。
    而没有心中不畏惧，这寻常的鬼魅就已经得退避三舍了，再加上他们腰间时时刻刻揣着一根柳木棍，这是打鬼的利器，一棍子招呼下去能打的冤魂厉鬼哀叫连连。
    胆量加上棍棒，不出意外，能拦住两个人的鬼魅已经不到了。
    “马东，兰若寺不是也闹鬼么，听说还有恶鬼吃人，寺庙之中的和尚都因为那些恶鬼而弃庙逃跑了，我们去兰若寺会不会有危险啊。”牛二忽的道。
    “大少爷既然让我们去，那就说明不会有事的，倘若真的有危险，大少爷又岂会不说，别想这个了，赶路要紧，大少爷虽然没有说兰若寺内有危险，但是却说了路上可能有危险。”
    马东很是凝重道：“上次我们去华县中了几个妇人的算计，脸面都丢尽了，这回可得把事情办好，因为这次可比上次的事情严重多了，没看到大少爷的眉头都皱起了么？”
    “说的也是。”牛二点了点头。
    两个人继续赶路。
    忽的，路旁的草丛之中有一道金光闪动，却是一锭金子藏在草丛之中，待人捡取。
    “嘿，又是金子，这把戏对我们没有用，之前在城里的时候大少爷就给我们看了，所谓的金子不过是骷髅骨变化出来的，若是捡走带回家，必定会引来一只恶鬼进屋，到时候苦不堪言。”马东看了一眼戏虐一笑，却是不理会那草丛之中的金子。
    “说的是，不过大少爷的话灵验了，我们正在被鬼魅盯上，它们想要迷惑我们两个。”
    “只要我们不动邪念就行了。”马东又道。
    两个人又骑马赶了一段路，却又看见前面的路上有一匹小毛驴出现，小毛炉上坐着一位成熟美艳的妇人，这美艳的妇人忽的回头看见两人，忙道：“两位好汉，我是某家的娘子，因为去县里省亲耽误了时辰，眼下天黑了，两位好汉能否帮忙送我一程，我必定感激不尽。”
    说完微微弯腰施礼，可以看见她敞开的衣襟那一片雪白的肌肤，分外诱人。
    “没空，你去找别人吧。”牛二瞪了一眼，喝道。
    说完，反而加快的马速，超过了这个骑驴的妇人，
    “那妇人定然是鬼，这样的手段我们已经不是第一次见到了，大少爷的那些朋友，就有被女鬼迷惑的，看上去女鬼个个貌美无比，实际上本来面目可能是黄脸健妇，也有可能是男鬼，甚是都有可能是一条狗。”马东说道。
    牛二点头表示同意。
    又行了一段路之后，忽的，在两个人的前面出现了一堵墙壁拦住了他们的去路。
    两次见此齐齐笑道：“这鬼魅的手段用尽了，变化一堵墙就能拦住我们么，大少爷还等着兰若寺的了空大师的金身去去镇压鬼魅呢，我们若是回去晚了，以后鬼魅祸乱百姓，我们怎么好面对他们？”
    “说的是，区区一面墙壁就想挡住我们，看我们将他撞碎。”
    两个人浑然不惧，直接骑着马撞了过去。
    说也奇怪，当两个人撞过去的时候还未靠近墙壁，那面突然出现墙壁却又忽的消失了，前面再次显露出道路来。
    等他们两个人走远之后，两只恶鬼却是从路边显现出了身影。
    “这两个人我们害不了，他们不贪念，也不好色，心中没有邪恶的念头，我们的手段没有办法迷惑他们。”
    “何止是这样，他们的念头太过刚硬了，连我们的鬼墙都能撞破，从这可以看的出来，他们此行的目的非常的重要，是为了一件大功德的事情，那善念萌发出来，比墙壁还要坚硬，若是撞在我们身上，肯定要把撞伤我们，若是我们强行要找他们两个人当替身的话，一定会遭到很严重报应的。”
    “说的是，我们得找两个贪婪好色之徒当替身，只有这样的人才容易上钩。”
    两只恶鬼窃窃私语了一番，便化作了阴风消散了。
    而它们的退缩，附近的好几股阴风也都四处散开去，都认为马东和牛二两人迷惑不了，所以也都选择放弃了。
    此时此刻。
    李修远骑着龙马一跃坠入深坑。
    伴随着好几个呼吸的时间下坠，他总算是落到了地面上。
    在这地面的前面有一个巨大的大洞。
    阵阵阴风就是从这大洞之中吹出来的，而在大洞的另外一面，却似乎连接着一个世界，里面有宫殿的废墟，还有各种坟间祭祀的物品，以及远处倒塌的城墙和屋舍。
    看样子里面是一座废弃的城池。
    “这是阴间的一座鬼城，没想到这个缺口开的如此的准确，居然打通了一座鬼城。”李修远神色一动，下了骏马，
    阴间他去过，而且不止一处，知道阴间如凡间一样，很是辽阔，而在阴间之中，虽然冤魂厉鬼无数，但都是汇聚在鬼城之中。
    就如同凡人生活在城池之内一样。
    而这一座废墟般的城池之中，李修远看见了一个个脸色苍白的鬼影向着这里汇聚过来，除此之外还有不少高大凶恶的猛鬼，他们显然都是知道了这里有一处通往阴间的缺口，所以就齐齐向着这里赶来，准备通过这个缺口回到阳间去。
    “你们通往阳间的路，到此为止了，这里由我来镇守，尔等冤魂厉鬼还不速速退去。”
    李修远大步走来，进入阴间，站在了这缺口前面大声的喝道。
    他的气息散发出去，附近鬼城的废墟，建筑立刻就化作了灰飞，变成了坟间祭祀用的纸屋。
    这是被他驱散了鬼魅附着在上面的力量，直接打回原形的缘故。
    随着他的话音一出，至少数千往这里汇聚来的冤魂厉鬼，齐刷刷的止住了脚步。
    走在前面的厉鬼似乎不相信李修远的话，继续往前走了几步，一接触了李修远散发出来的气息就像是遇到了滚油一样，惨叫一声又迅速的缩了回去。
