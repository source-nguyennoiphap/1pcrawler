    见到这纸团之后，李修远第一时间便是警惕和疑惑起来。?八一中文网  Ｗ㈧Ｗ㈧Ｗ㈧．?８?１?Ｚ㈠Ｗ㈧．㈠Ｃ?Ｏ?Ｍ
    “这么晚了，那个姑娘找我不知道所为何事？莫不是昨日的误会太深，今日想要报复回来？想要戏弄自己一番。”
    若是如此的话，李修远反而是不担心了。
    他怕就怕这事情是不是和这寺庙有关。
    要知道，这可是聊斋世界，妖魔鬼怪不知道有多少，尤其是这深夜外出，遇鬼遇狐的事情不知道有多少，虽说他是七窍玲珑心可以看破一切虚幻，避一切鬼神，可到底也是**凡胎，还是存在这一定的危险性。
    就在李修远犹豫不定的时候，另外一边，之前那个丢下纸团却又匆匆离开的丫鬟小蝶却是提着衣裙一路小跑回来。
    “怎么样，书信送到了么？”青梅站在禅院前，带着几分娇态忙问道。
    小蝶匀了匀气，笑着说道：“小姐放心吧，奴婢已经将书信丢到了李公子的脚下了，相信李公子这个时候正在看小姐的书信呢，估摸着再过片刻李公子就要赴约了。”
    青梅美眸一亮，忙道：“若是如此的话我这就去大雄宝殿之前等候着他，免得失了礼俗。”
    说完又急忙整理了一下衣衫，秀。
    “小蝶，你看我这样子可以么？有没有什么地方没有打理好的。”青梅问道。
    小蝶笑道；“小姐今日格外漂亮，相信李公子待会儿肯定会被小姐迷的神魂颠倒的。”
    “胡说什么呢，你且赶快禅房准备好酒水，瓜果，待会儿好招待李公子。”青梅又催促道：“千万别手忙脚乱的误了事情，若是今日误了小姐我的好事，我日后可不饶你。”
    说着又有一些严厉起来。
    “奴婢知道了，小姐请放心吧，奴婢一定会被一切都准备妥当。”小蝶说道。
    青梅不放心又叮嘱了一番，然后方才急匆匆的离去。
    今夜天空有些昏暗，虽有明月，但却一片云朵遮住，使得周围并不光亮。
    青梅趁着夜色，急急忙忙的赶到大雄宝殿之前的时候，一颗放心却已紧张不已，噗通，噗通的直跳，让她忍不住微微喘息了起来，不过她的俏脸之上依然带着几分激动之色，一双明亮的眸子四处打量着，
    似乎是在寻找李修远的身影。
    “李公子还未来么？”青梅心中有些羞意的想道。
    她平复了一下心情，准备来到大雄宝殿前面静候李修远赴约，可是当她走到大雄宝殿大门府旁边的时候，却隐约看见在大雄宝殿之内一位男子却早已经矗立在此，虽侧身对着自己，但是依然可以看见这是一位男子的身影。
    不过在昏暗的大雄宝殿之内看的并不清楚。
    “莫不是李公子已经提前来了？”
    青梅芳心又不争气的砰砰直跳，她轻轻的步入大殿之中，犹豫了一下，最后鼓起几分勇气轻声问道：“是李公子么？”
    那矗立在大大雄宝殿之内的男子一动不动，似乎并没有听到青梅的声音。
    青梅以为自己声音小没有说清楚，便又往前走了几步开口道：“奴家青梅见过李公子。”
    声音提高了不少，清脆悦耳，但这矗立在大殿之内的男子依然不动。
    青梅这个时候有些疑惑和好奇，轻轻的走了过去，然后抬目一看。
    眼前这个站在大殿之内的男子并非自己要等待的李修远，而是之前借宿在兰若寺的穷书生，好像是叫什么叶怀安，虽说和这个书生有过一面之缘，打过一个招呼，但是她却对这个书生很是陌生，也并不了解。
    书生叶怀安此刻站在原地，脸上露出痴迷之相，呆呆的看着眼前的这幅壁画。
    如果李修远在这里的话就会知道，此刻书生叶怀安所看见的这壁画正是白天他险些入迷了的那一副。
    不过这叶怀安显然已经沉迷到了这壁画之中去了，在此不知道矗立了多久，整个人仿佛已经被迷的神魂颠倒了，不可自拔，时不时的还吃吃一笑，便是口水流出来了也没有察觉。
    “叶公子，你无恙吧。”青梅见到他这一番痴迷的样子，出于好心轻轻晃了晃他。
    此时此刻，沉迷于壁画之中的叶怀安此刻正与一位天宫之中的绝色仙女，互拥在了一起，双双倒在了仙女闺房的软塌之上，准备彼此欢好一番。
    可就是在此刻，叶怀安感觉软塌猛地震动了一番，随后神情一恍惚，只觉自己被那震动震飞了出去，离开了软塌，飞出了仙宫。
    神情一恍惚，叶怀安这才渐渐的回过神来。
    当他看清楚眼前的景象时，却现不知道什么时候自己的身旁正站着一位娇柔动人的俏丽佳人，这俏丽佳人的模样岂不是和那位欲和自己恩爱的仙女模样一般无二么？
    “没想到仙女也随我一起离开了仙宫，太好了，我叶怀安誓，今生今世必定只爱仙子一人，仙子，来，我们继续刚才的事情。”
    叶怀安说着抓住青梅的手腕，欲将其拉进自己的怀中。
    青梅吓了一跳，连忙挣扎道：“放来我，我不是什么仙子，你认错人了。”
    “仙子不用怕，让我好好疼爱你。”叶怀安带着几分兴奋之色，此刻便欲强行霸占这个娇柔的俏丽佳人。
    青梅此刻又惊又慌，奋力挣扎，伴随着一声裂锦声响起，她衣袖被撕扯了下来，露出了一节白皙如玉的纤细玉臂。
    “仙子真是漂亮。”叶怀安看在眼中，双目喷火，恨不得立刻就得到这个俏丽佳人。
    “救命，救命啊，放开我，你这登徒子放开我。”青梅奋力挣扎，同时大声呼救起来。
    可是这个叶怀安也不知从哪里来的一股力气，抓住青梅的双手，让她挣扎不脱，就是想要得到眼前这个仙女，任凭青梅如何挣扎他就不放开。
    只等仙女挣扎的精疲力尽了，自己便趁势而为，将其得到。
    青梅一边呼救一边挣扎，身为一个弱女子的她很快力气就耗尽了，此刻忍不住哭泣起来。
    “仙子别哭，我这就好好疼爱仙子。”叶怀安觉得机会来了，欲解衣宽带强行和这仙女成了好事。
    不过就在这个时候，忽的一声呼啸声响起。
    “啊~！”
    一块石子从大雄宝殿之外飞来，重重的击在了叶怀安的脑袋上，只见他脑袋立刻就被打破了，鲜血流出，整个人一个踉跄险些栽倒在地。
    “大雄宝殿，佛主眼前，这位兄台欺凌一位弱女子，这可是会遭天谴的。”一个冷淡的声音从外面传来。
    青梅抓住这个机会急忙挣脱离去，带着几分泪花直往大殿之外跑去。
    此刻，天空之上的云朵散开，皎洁的月光洒落下来，一位年轻英俊，身姿挺拔，身穿锦袍的公子沐浴在月光之中，浑身上下似乎都散着淡淡的光晕，让人咋看之下还以为是天上的嫡仙下凡，不似凡尘中人。
    而青梅瞧见了这个年轻公子之后却顿时看到了希望一般，忙呼道：“李公子救我，有人想要非礼我。”
    “姑娘放心，有我在，没有人可以非礼你。”李修远平静的说道。
    声音之中仿佛带着几分神奇的力量，能立刻让人镇定下来。
    能有如此语气说出这般话的人必定是值得依靠和信任的。
    “多谢李公子。”青梅欣喜道，一双美眸停留在李修远的身上便只觉在也移不开了。
    “姑娘别走，此人昨日还轻薄过姑娘，是十足的纨绔弟子，莫要错姓了坏人。”叶怀安捂住流血的脑袋，急急忙忙的追了过来道。
    青梅吓了一跳，当即就躲在了李修远的身后。
    李修远淡淡一笑：“这位兄台，适才你非礼这位姑娘的事情我可都瞧见了，你是现在立刻向这位姑娘赔礼道歉呢，还是让我抓你去官府报案呢？”
