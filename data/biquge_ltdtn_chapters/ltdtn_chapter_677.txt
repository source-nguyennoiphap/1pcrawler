    “大少爷到了，前面就是楚氏陵园了。”一个护卫开口道。
    “看到了。”
    李修远骑马而至，抬头一看，却见不远处一处陵园前立着一座牌坊，牌坊上挂着一块石制的牌匾，上面写着：楚氏陵园，四个大字。
    晚上来这里的时候，这并不是牌坊，而是楚府的府门。
    但是白天，这里的鬼怪施展不了变化之术，一切都会显露出原型。
    “咦，那棺材板还在这里。”
    李修远骑马过一条小溪的时候，却看见小溪的旁边倒放着一块棺材板。
    这棺材板已经有些年月了，上面的棺漆剥落，已有多出地方腐烂，上面还沾染着一层厚厚的黑泥，像是从地上刚刚挖出来没有多久。
    “果然是一块棺材板，大少爷上次没有说错。”护卫惊奇不已的说道。
    上次来的时候，是一个老者站在这溪水旁边和他们打招呼，要他们帮忙把那老者背过小溪去。
    还好被李修远提醒了，两个护卫方才没有被迷惑，不然就是把这块棺材板背在身上了。
    李修远并没有理会这块棺材板，而是越过小溪，进入了楚氏陵园之中。
    楚氏陵园之内，坟丘一排排屹立在旁边，都是楚家奴仆，下人的坟丘，往前继续走，坟丘的规格却是提高了不少，能享用一块青石墓碑，上面都是楚家亲戚的坟丘，再往前的坟丘却都是用石料围了起来，修葺的整整齐齐，那是楚氏子孙的坟丘。
    不过还有的坟丘，奢华精美，不但是汉白玉建造而成的，附近两旁还有石狮子坐落在旁边，这些坟丘的位置都是在楚氏陵园之中所谓的风水宝地上。
    那些都是楚氏先祖的坟丘。
    楚氏一族上上下下，好几代人都埋葬在这里。
    而在这楚氏陵园的其中一处地方，还有一处祭祀用的祠堂，祠堂里面摆放着楚氏一族祖祖辈辈的排位。
    这里香火鼎盛，每日都有楚家专门的老仆打理，供奉，确保香火不断。
    “老老实实的待在祖坟之中享用香火不好么，为什么要来祸害人间。”李修远来到楚氏的祠堂前面，看着明亮的祠堂里面那一排排密密麻麻的排位，不禁喃喃自语道。
    从这祠堂的规模，可以看的出来这楚氏在本地的势力到底有多强大。
    只是这样的强大没有给楚氏一族带来福德，却带来了灭亡。
    “这位公子是来上香的么？香在那边，公子可以去取。”
    看守祠堂的老仆见到祠堂门口的李修远，立刻迎了出来，恭恭敬敬的施了一礼，然后开口道。
    他把李修远当初的楚家中的某个远方亲戚了。
    李修远笑道：“不，我不是来上香的，楚家的这些祖先享用的香火已经足够多了，我是来给他们送一篇祭文的。”
    “焚烧祭文的话，请到那边香炉之中焚烧。”祠堂内的老仆指了指一口巨大的铜炉说道。
    李修远点了点头：“多谢提醒，不过这片祭文应该楚家的这些祖先享用的最后一道香火了。”
    说完，他招呼了一下，带着护卫进了祠堂。
    看守祠堂的老仆有些疑惑，觉得这个公子有些古怪，言语之中对祖先们并没有敬畏之心，相反还有一些侮辱的意思在里面。
    李修远倒不是侮辱楚家的先祖们，而是抱着敌意来的，自然不会那么多恭敬。
    从怀中取出了早就准备好了的祭文，用旁边的灯火点燃，丢进了铜炉之中。
    祭文很快燃烧了起来，迅速的化作纸灰，但隐约可以看见祭文上为首的几个大字：请神祭。
    这不是一片祭祀祖先的祭文，可是一篇请天上雷神诛灭妖邪的祭文。
    祭文很快燃烧的干干净净，却见一股青烟有违常理，笔直的向着上空飘去，似乎成一条笔直的线，要连同天地，这和寻常的拜祭用的香上面飘出来的烟完全不同。
    李修远看着这股青烟腾空之上，欲冲上云霄，不禁带着几分期待之色。
    “呼呼~！”
    然而就在这个时候，祠堂之内无风自起，一股阴风从四面八方飘荡而来，向着这股冲上云霄的青烟吹去，似乎想要吹散这股青烟，让这股青烟消亡在半空之中。
    “这里好端端的怎么会起风呢？”护卫感觉很不可思议。
    那风吹到身上他感觉到了一股寒意，浑身上下鸡皮疙瘩都起来了。
    李修远平静的说道：“我们在别人的祠堂李烧请神祭，这里的鬼怪立刻就能知道，他们刮起阴风是想要吹散这股青烟，让我的请神祭到不了苍穹，这样一来天上的神明就不会知道这里的事情了。”
    “不过这是无用之举，我的请神祭这些鬼怪是阻止不了的。”
    说完，他看着那股青烟直通苍穹而去，便是周围阴风阵阵，可是却丝毫不影响那股青烟，这青烟之中仿佛有一股神秘的力量阻挡了这些鬼怪的力量。
    “呼~！”
    祠堂的天井之上忽的刮起了一道黑风，浓郁而又阴暗，带着一股腥臭的味道，像是什么东西腐烂了一样。
    这黑风隐约化作了一张人脸，张嘴一吸，想要将那飞出去的青烟吸到嘴巴里面。
    “啊~！”
    可是下一刻，一缕青烟飘荡过来，那股黑气之中似乎响起了一声惨叫声，紧接着黑气一颤，竟有种溃散的趋势。
    “我的这片祭文，你们这些老鬼拦的下来么？”李修远平静的说道。
    声音回荡在祠堂之内，激起了暗处阵阵阴风肆虐。
    “李修远，你一片请神祭送上天去，难道真的想要请来天上的神明，将我楚家的祖坟尽数荡平么？”
    之前那个看守祠堂的老仆走了过来，他双目迷茫，开口说话的时候却是楚侍郎的声音。
    李修远看了一眼，就知道那老仆是被楚侍郎附身了。
    “楚侍郎，我来这里不是和你斗法的，而是来和你讲道理的，你别误会。”李修远淡淡的说道。
    “讲道理？你这是要坏我楚家的祖坟。”
    楚侍郎附身的老仆勃然大怒道：“亏你还是人间的圣人，居然做的出来坏人祖坟的事情，当真是罪大恶极。”
    “祖坟的确是值得尊重的，毕竟人死为大，可是恶鬼的坟墓又如何呢？”
    李修远目光陡然变的凌厉起来：“你们还有脸做祖宗，在我看来你们这些家伙就是一群有了道行的恶鬼盘踞在人间，作威作福，死不足惜，其他的罪行我就不多数了，也数不清，就凭今日的事情我就应该荡平你们这群恶鬼。”
    楚侍郎又惊又怒，他知道此刻李修远来到这里就说明自己交代孙子楚天的事情失败了。
    借打猎之名，并没有能够猎杀了这个李修远。
    人间圣人当真是有大福泽，那样的谋害都能无恙，也不知道其中发生了什么曲折的事情。
    “你不必那样看着我，人杀人尚且要接受朝廷的审判，或下狱，或斩首，你们这些鬼怪迷惑凡人，害人性命，本地的城隍不管，我李修远来管，今日我就判你们这群恶鬼死刑，以前的种种一切恶行，今日一并清算。”
    李修远声音一冷说道。
    “李修远，你当真一点机会都不给？”
    楚侍郎附身的那个老仆浑身哆嗦，不知道是气的还是害怕的。
    李修远摇头一笑：“和你们鬼怪说道理永远说不通，你的理是理，别人的理就不是理了，机会我早就给过你们了，是你们自己不要，现在还要我来给机会？”
    “如果你们真的要我给机会的话就不会去先拦我的请神祭了，后面拦不下来之后才开始服软，希望我饶恕你们，我真的从未见过如此厚颜无耻之辈，亏你前世官拜侍郎，做鬼这么多年，礼义廉耻只怕早就忘的一干二净了吧。”
    楚侍郎这个时候咬牙切齿的说道；“李修远，今日你若真要和我楚家斗个不死不休的话，我楚家日后即便是有一人尚在，都会去寻你李家报仇，让你子子孙孙不得安生。”
    李修远浑然不惧道：“我连千年大妖都杀了，何惧你们这些小鬼作祟，若是我的后代真的被你们楚家的人找上了的话，那也是我的后代德行亏欠了，福德浅薄，便是死在了你们这些鬼怪的手中也是他咎由自取，你放心，我不会怪你的，相反我还会感谢你，因为你让我后代明白了，福德的重要性，日后说不定我的后代在你的影响下人人向善，广积阴德。”
    “......”楚侍郎无话可说。
    他读书这么多年，如今李修远的一番话方才让他明白什么是堂堂正正，什么是光明正大。
    一言不发的楚侍郎化作一道阴风离开了，事情已经没有了回转的余地，他只得去准备逃难。
    楚氏陵园不能待了。
    “轰隆~！”
    就在他化作阴风前脚一走，后脚天空之上便响起了一道晴天霹雳。
    这个惊雷震的楚家的祠堂微微一颤，灰土簌簌的从屋顶之上落了下来，同时那楚家祠堂之上摆放的牌位，竟随着这一声惊雷的响起，开始哗啦啦的跌落下来，有些摔在了地上直接摔了个四分五裂，有些倒在了木架上，再也立不起来，还有一些直接金漆剥落，上面的名字黯淡无光，隐约有了裂纹。
    “这，这发生了什么事情了？”
    恢复神智的老仆见此一幕顿时大惊失色。
    李修远说道：“老人家，这是不祥的征兆，或许你应该离开这楚氏陵园了。”
    说着他挥了挥手，示意了众护卫离开。
    是时候出去和天上的雷公们打一个招呼了。
