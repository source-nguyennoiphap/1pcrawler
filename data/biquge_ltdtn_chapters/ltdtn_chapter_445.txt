    若要人不知除非己莫为。
    这话当真没有说错，李修远本来是来朱尔旦家中寻陆判的，结果却听见了陆判和李梁金之间的对话，知道了陆判的歹毒用心，这个堂堂的阴间判官，竟也在暗中算计自己，用欺骗的方法来利用李梁金对付自己。
    “我和陆判之间并无恩怨纠葛，他竟想要谋害我的性命，这是我不能理解的。”李修远目光微动，看着不远处的朱尔旦家。
    “难道是因为十王殿的五尊鬼王被我诛杀了，所以他要报复？还是说他想要谋取我的七窍玲珑心？但不管如何，我深信，善念招来福报，恶念招来祸端，人不可能有害人之心，鬼神亦是不能有害人之心，他想要谋害我，这会儿就被我无意间听到了，岂不是把祸端招来了么？”
    李修远看着李梁金等人离开的方向，又暗道：“既然都已经唆使李梁金对我动手，那这恶事已成，我对这素未谋面的陆判也就不需要犹豫那么多了，今日......便诛了他。”
    当即，他吩咐了一声；“长舌鬼王，你去传个信给傅天仇身边的那个护身神将，让他告知傅天仇，说李梁金带兵欲行不轨之事，让傅天仇防备，他若肯动兵最好，若是不肯动兵的话你就通知我府上的李林甫准备避难，以你们这鬼王的手段，躲避这次的危险应当是不成问题的。”
    稳妥起见，他做了两手准备。
    而随着他的话音一落，附近的天空之上便卷起了一股阴风，长须鬼王的身形忽的显现了出现，对着李修远拱手应了声便有立刻化作了阴气溃散，然后向着衙门的方向迅速吹去。
    “其他四位鬼王随我去拜会拜会这个陆判。”李修远说道。
    当即，他骑着龙马一跃而下，从屋顶上落到了地面上，然后向着朱尔旦的家中走去，在他的身后四股阴风盘旋跟随，隐约之间有一个人的轮廓浮现，只是在黑夜之中这里的异样并不会被人看见。
    当李修远走到门口的时候却见到朱妻浑身冻的哆哆嗦嗦，正欲关门。
    “是朱夫人么？还请晚点关门。”李修远翻身下马，大步走来。
    朱妻见到李修远骑着一匹龙驹从天落下，心中惊疑不定，有些惊吓道：“你，你是什么人？”
    李修远道；“朱夫人无需害怕，我是城里的秀才，姓李，因为有点私事所以想要拜访拜访朱尔旦，若是有打搅之处还请见谅。”
    朱妻却是不相信李修远，她道：“我夫君说了闭门谢客，谁都不见，这位公子如果有什么事情明日再来找我夫君吧。”说完便欲关门。
    “朱夫人难道不知道你的夫君朱尔旦已经危在旦夕了么？”李修远说道。
    朱妻是妇道人家，听这一说当即吓了一跳：“这位公子你为何怎么说？”
    李修远道：“因为朱尔旦请来了一尊恶神，现在正在屋内和朱尔旦聚会，你若容许我进去的话便替你诛了那尊恶神，你若阻拦我的话我只能硬闯进去，之所以愿意在这里和朱夫人客气一番，是因为我是一个读书人，礼节还是要遵守的。”
    说完，他神色微动，因为屋内的说话声已经停止了。
    “鬼神已经察觉到我了，我不能浪费时间了。”当即，李修远大步走进了院子。
    看来是自己和朱妻说话的声音已经惊动了陆判，不过无所谓了，附近四尊鬼王在，这个陆判是逃不了的。
    朱妻心中犹豫，但还是没有阻拦李修远，因为她已经明白自从那神像出现在家中之中，夫君的性情大变，已经和以前大不一样了，就像是换过了一个人一样。
    若真被鬼神所害的话，倒不如相信这个李公子的话，将那恶神诛杀了。
    李修远还未进屋，便听见屋内传来了一个粗犷的大笑声：“哈哈哈，能避开我的耳目来到屋外而不被发现，这天下只有你这人间圣人了，你的出现是无法被推算来，便是我等鬼神也不能知道你会出现在哪里，既然来了，何不请进来一坐，喝杯酒水暖暖身子？”
    “不打算逃么？”
    李修远听到这话，神色反而一凝。
    若是闻声而逃，他反而不惧这个陆判，因为逃就代表着害怕，害怕就代表着威胁不大，自己诛杀他不难。
    之前自己故意这朱妻说话也是有意弄出一点动静来让陆判知晓自己的到来。
    这是一种试探和无形的威吓。
    如此手段是他当初从黑山老妖手中学来的。
    只可惜，这陆判知晓自己的到来并没有逃走，反而大大方方的邀请自己喝酒。
    当李修远走近屋内的时候，却看见书房之中一个身穿官服，绿面红须，相貌丑恶的鬼神正坐在桌子前，拿着酒杯微微晃了晃，然后鼻子一吸，将一股酒气吸进了腹内，脸上顿时露出了满足之色。
    “好酒，这凡间的酒的确与众不同，有股俗气。”陆判笑着说道，又给自己斟了一杯。
    李修远扫看了一眼屋内，却见到朱尔旦脸色平静，目光闪动不定的站在一旁，一言不发。
    当他见到李修远到来的时候露出了几分诧异的神色。
    “你便是陆判？阴间执掌生死簿的那位？”李修远道。
    陆判笑道：“不错，便是某，我对你不陌生，你的事迹我也略有耳闻，听说你在郭北县搏杀了一头千年黑虎，在城内斩过一条乌江龙王，在华县灭过一尊假观音......便是前不久，某的十王殿之中的五尊阎罗被你灭了五尊，收服了五尊，你这人间圣人当真是与众不同，不去考取功名利禄，不去赚取权势，不去纳妾成群，逍遥快活，却是专和我们这些鬼神过不去，李公子，你此举似乎略有不妥吧。”
    “因为有些鬼神不安分，想要谋害我的性命。”李修远淡淡道。
    “某的十王殿和李公子井水不犯河水，李公子不也打上门来了么？长须鬼王被李公子收服了，某的十王殿岂能不有点表示，不然十殿阎罗的威名何存？如何能震慑十方鬼怪，如何能维持轮回运转，某听说你人间圣人最重一个理字，此事你到是看看，是你有理，还是你无理。”陆判说道。
    李修远道：“此事自然是我有理，长须鬼王占据民居，成为凶地，影响城内百姓生活，这便是鬼神干预凡人，此乃一大错，我将其镇住，收为己用，这是一件功德良善的失去，厉鬼不可怕，可怕的是无人约束的厉鬼，我约束了长须鬼王就等于保了金陵城的太平，是你贼心不死，欲布局害我，现在还敢和我谈理字。”
    “除此之外，你之前更是唆使了李梁金谋害我，这样歹毒的用心，便是你未害我，害的是别人我也要将你诛杀，”李修远冷冷的说道。
    陆判哈哈一笑，又是到了一杯酒：“说的好，你的道义的确是堂堂正正，被你诛杀的鬼神足以让任何人信服，果然不愧是人间圣人，和那些欺名盗世之徒不一样，这酒当饮。”
    说完又是鼻子一闻，吸了一口酒气，然后将毫无酒气的酒水倒掉。
    李修远皱起了眉头，不明白这个陆判是要耍什么手段。
    他为何如此笃定？仿佛胸有成竹一般。
    “陆判，今日我要诛杀你。”李修远直接开门见山的说道。
    陆判咧嘴笑道：“你凭什么可以诛杀我？”
    “我有一刀，缺一头颅待斩。”李修远缓缓的开口道。
    “你的刀虽利，可上诛仙佛，下诛鬼怪，但却不是无敌，上天既然让你习得了斩仙之法，那自有相克之法，很不巧，某暗中观察你多时，却有一法可挡你斩仙大刀。”陆判酒杯一放，笑着说道。
    李修远冷冷道：“我不信。”
    “既然不信，请人间圣人出刀，某愿以身试刀。”陆判站了起来，对着李修远施了一礼。
    丑陋的脸上露出了几分诡异的笑容，绿色的脸庞越发显得诡异阴沉了。
    “是陷阱么？”李修远神色微微一动。
    如上次一般，等自己出窍，好趁机谋害自己的肉身？
    不，这可能。
    上次自己是因为一时不察，被鬼神埋伏了，所以才中了计，而同样的方法不陆判不可能使用两次。
    不过也不能保证陆判这再来一次设伏。
    又或者之前李梁金走的时候留下了高手，就蛰伏在这屋子内？
    一时间，李修远神色变化不定，反而有几分迟疑了起来。
    “要么是有埋伏，要么就是这个陆判真的有本事挡住我的斩仙大刀......只是我的斩仙大刀被挡住这是从未有过的事情，即便是再厉害的鬼怪都会被我这一刀诛杀，难道这陆判是在给我唱空城计？知道自己走脱不掉，所以故作轻松？”李修远心中暗暗想道。
    只是犹豫归犹豫，他脸上却是没有半分的迟疑之色，他当即道：“既然陆判有如此自信，那便请陆判试刀。”
    说完，他周身紫光大冒，似有冲天而起的趋势，隐约还有龙凤合鸣的声音传来。
    紫气盖顶，龙凤呈祥，
    这是人间圣人神魂要出窍，即将显现神异的缘故。
