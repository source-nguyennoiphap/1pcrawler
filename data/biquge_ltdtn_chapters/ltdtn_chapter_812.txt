    “李公子上次一别已有好些时日，不曾想到李公子竟会北上入京，上次天姥山一别，贫僧还以为无缘再见了。”
    庭院里的一处凉亭内。
    白云为李修远和释空大师沏茶，两人对坐，饮茶对谈，颇有几分访僧问道的雅致之情。
    李修远笑道：“天下这么大，我总得四处走走吧，一直待在南方的话这北方的鬼神未免生活的有些过于滋润了，听说现在的南方那些恶鬼恶神，闻我之名无不望风而逃，南方的鬼神虽然治理，平定了，可是
    这北方呢？面对大师我不想说一些敷衍的假话，我此番入京是来整治北方鬼神的。”
    “大师在相国寺修行，对京城的局势应该知晓的很清楚，可愿意为在下一解心中疑惑？”
    释空大师双手合十道：“阿弥陀佛，贫僧真是惭愧，惭愧，修行近百年，除了一些浅薄的本事之外，对于京城的这些鬼神却也拿其没有办法，若非相国寺得到了佛法的加持，只怕早就被那些妖魔鬼怪给谋去
    了，李公子有意整顿京城鬼神实乃百姓之福，万灵之福。”
    “只是贫僧久居相国寺，极少外出，对于京城的鬼神势力知晓的也不多。”
    李修远道：“大师在此地修行超过一甲子难道就一点都不知道？是大师不方便说，还是心中有忌讳？以大师的修心，我想应该不会在意那些东西吧。”
    释空大师微微笑道：“贫僧驻世的大限要到了，今日是贫僧最后一次开坛讲经，七日之后就要坐缸了，世俗的事情我早已经放下了，李公子心系苍生这是一件善意的好事，只是贫僧真的是爱莫能助了，不过
    本寺之中却有一位外来的修行僧人，他是十几年前来相国寺的，见到京城有妖魔作祟，所以一只在京城内外游历，降妖除魔，驱除邪祟，深知京城情况，也深得百姓爱戴。”
    “相国寺能有今日，也是多亏了他，李公子若是有什么问题的话可以去询问他，他的理念和李公子的理念是一样的，相信在京城能帮助李公子不少的忙。”释空大师说道。
    “什么？大师要死了？”李修远却是听的一惊。
    很难想象这看上去精气十足的释空大师竟然不日就要坐化了。
    释空大师笑道：“寿元是天定的，既然上天规定的时间已经到了贫僧又为什么不会死呢？正是因为驻世的时间已经到了，才不能助李公子在京城走一遭，白云，去，把迦叶僧人唤来，说是南方圣人正在此处
    喝茶，请他前来一见。”
    “是，师傅。”白云应了声便转身离开了。
    李修远道：“真是没有想到大师的寿元已经走到头了，似大师这样的得道高僧逝去真是人间的损失啊。”
    “人间有李公子你就足够了，贫僧此世修行已经圆满了，该皈依我佛了，而且贫僧此刻的坐化是一种幸福的事情啊，反而李公子还要在人间驻世至少几十年，为这天下芸芸众生操心，李公子的慈悲和仁德，
    已经胜过罗汉，堪比菩萨了，阿弥陀佛。”释空大师笑着说道。
    “如此说来，大师此刻走到是轻松了？不用为凡尘俗世操心？到时我还得在人间继续操劳下去。”李修远摇头一笑：“不过大师这样的话也是有道理的，只是人活一世总得为这世道做一点什么吧。”
    “吃喝玩乐，享乐一声，死后黄土一捧，这不是我应该追求的，我追求的是心中理念的达成啊，为此再辛苦也是值得了。”
    “阿弥陀佛，善哉善哉。”释空大师唱了声佛号。
    他亦是为李修远这种信念所折服。
    也许就是因为这个缘故，别人不能成为天生圣人，他能成为的缘故吧。
    上天安排他来到这个世界上不是没有原因的。
    “师傅，迦叶大师来了。”这个时候白云不知道什么之后走了过来，施了一礼道。
    此刻他的身边跟着一位僧人，这位僧人光着脑袋，身穿僧衣，相貌平平，是一个很寻常的中年男子，放在任何一个寺庙之中都是和普通的存在。
    唯一让李修远在意的是这个僧人的眼睛。
    那是一双什么样的眼睛啊，慈悲之中透露出一种异常的锐利，婉如一头苍鹰，又似一头猛虎，让人不敢与之对视。
    “这是一位僧人该有的神态么？”李修远心中暗暗惊道。
    而且看着僧人的双手，满是老茧，宽阔是蒲扇一样，厚重有力，而且膀大腰圆，仿佛有万夫不当之勇，若是脱掉僧衣的话，更像是一位武将，不像是一位和尚。
    “这位施主有礼了，贫僧迦叶。”迦叶僧人施了一礼道。
    释空大师说道：“迦叶，你来到相国寺已经十几年了，你是一位十分有智慧又有法力的僧人，在佛门之中的修行连贫僧都自愧不如，贫僧如今不日就要坐化了，这位李公子是南方的圣人，他来京城是要行济
    世救民，无量功德之大事，你可愿意助他行事？”
    他没有说那么多理由，只是问迦叶愿不愿意。
    迦叶僧人看了一眼李修远道；“他是贫僧一直等的人，贫僧愿意帮助他。”
    释空大师笑道；“如此我就放心了，李公子，京城的事情你可去问他，他知道的比任何人都清楚，有了他的帮助你在京城不管做什么都容易的多，现在差不多时间要开坛讲经了，贫僧去了，李公子你还请自
    便吧。”
    交代了一下事情之后，他施了一礼，起身便缓缓离去。
    一旁的弟子白云急忙搀扶，跟随他一并离开。
    李修远站了起来目送释空大师离去，今日这一别只怕再也见不到这位高僧了。
    “施主可知释空大师的寿元并不是近日终结的？真要算起来的话他还能活半年呢。”
    忽的，一旁的迦叶僧人双手合十微微低头缓缓的说道。
    “若是如此的话，为什么释空大师要说讲经完了之后就要坐化？”李修远皱起了眉头，看向了这位魁梧的僧人。
    迦叶僧人说道：“那是马上就要到上元节了，比起中元节，这是京城鬼怪闹的最凶的一段时间，往年释空大师守住相国寺的山门就已经很不错了，所以释空大师知道自己不能死，一旦死了，相国寺这最后一
    处佛门净地也将被妖邪占据，届时京城的百姓就更苦了。”
    “如今这最后一次讲经，释空大师势必要给每一位听经的百姓用佛法加持一番，在上元节这一次护住他们的安全，但这会耗尽他所有的念力，不过释空大师之所以会这么做是因为他信任李施主你啊。”
    “信任我？”李修远沉默了一下。
    “真是，正是因为信任李施主你的本事，释空大师才愿意将毕生的修为都赠送出去，为李公子在京城行事尽一份绵薄之力。”迦叶僧人道。
    李修远闻言立刻肃然起敬。
    现在他明白了为什么释空大师要带着白云厚着脸皮去向那些仙人讨要法器了，是因为他在为身后事做准备，想让白云成为一位降妖除魔的高僧，撑住相国寺。
    但只怕释空大师自己也没有想到他在天姥山碰到了自己。
    或许正是因为从自己身上看到了希望，释空大师才有了今日这舍身布施的一幕吧。
    李修远没有去阻止，也没有去赞许，只是忍不住的感慨道：“佛门说所的大无畏精神，或许指的就是释空大师吧，我不如他啊。”
    “既然是释空大师最后一次讲经，我又怎么能错过呢。”
    当即，他邀请了之前的好友，张邦昌，钱钧，高藩，朱昱四人，急忙前去相国寺的宝殿前听经。
    果然。
    此刻的相国寺山门前人山人海，百姓涌动，不知道有多少的人赶来听释空大师讲经。
    “我的天啊，这人也太多了吧，李兄，我们这怕是要被挤出山门外了。”见到如此多的人，张邦昌惊呼起来，有些都不敢往前了。
    李修远说道：“今日是释空大师最后一次讲经，我施法，带你们上天上去看。”
    “上天？李兄，你什么时候有这能耐了？”朱昱睁大了眼睛：“以前怎么没有见你施展我。”
    “新学的法术，以前还不会呢，便是现在施展也有些勉强，你们当心一点，”李修远先是施展了一个障眼法，免得引起人的注意，接着再施展了腾云的法术。
    立刻众人的脚底下涌出了一片白云，拖着他们就往半空之中飘去。
    这白云宛如实物，踩在脚下软绵绵的，不过因为他们身体内的浊气都太重了，饶是李修远施法也只能带他们飞到两丈不到的半空之中。
    再高就不行了。
    这和道行没有关系，是法术限制的缘故。
    飞天一类的法术是最难修行的，尤其是还是带人飞行。
    “真，真的飞起来了。”
    他们几个人睁大了眼睛看着下面的人群，虽然飞的不高，却已经让他们震惊无比了。
    “李，李兄，你难道已经是神仙人物不成？不但会做鬼除妖，连飞天遁地都能做到？”钱钧惊呼道。
    李修远摇头笑道：“我算什么神仙人物，神仙能腾云驾雾，朝游沧海暮苍梧，我这算是什么？一朵白云立地不过二丈，速度快不过牛车，离神仙还差了十万八千里呢。”
    “额......”
    听这么一说，众人觉得似乎是这么一回事啊。
    “释空大师开始讲经了，我们别出声，我施展了障眼法，其他的人看不到我们，但是却能听到我们的谈话，莫要引起被人的注意，不然被当成的妖怪打了下来可就不妙了。”李修远道。
    “李兄提醒的对。”朱昱连忙点头，不敢出声。
    此刻，相国寺的宝殿前摆起了一个法台，台上坐着一位身披袈裟，慈眉善目的老僧，他一句话没有多说，直接就念起了经文，似乎也不管众人声音嘈杂听不听的清楚，只是自顾自的念经。
    似乎并没有什么值得奇怪的地方，反而让人有些大失所望。
    但是李修远却看见这释空大师的身体之外冒出了金光，这金光从他嘴中吐出，在天空之中凝聚，化作一朵朵莲花从天坠下，没入了一位位香客的脑袋之中。
    “这是在给百姓加持佛法，而且念的是最常见的金刚经。”
    他虽然不懂佛法可也是清楚一二的。
    金刚经虽然常见但却是佛门之中最克妖魔鬼怪的经文。
    寻常百姓念此经文百遍甚至都十分有可能唤来金刚神庇护，为你驱赶精怪，擒拿妖邪。
    而由释空大师佛法加持的话，威力更大，这股念力留在每一位百姓的身体之中，只要碰到邪祟入侵亦或者是妖怪谋害的话，这念力激发，就会化作一尊金刚神显化出来为你抵挡灾难。
    如此不凡的经文，寻常人能听一次就是一件莫大的幸事。
    能无形之中为你消除灾难一次，怎么能不让人心动？
    只是被念力加持的百姓并不清楚这一点，他们得到了念力的加持并没有感觉到什么异样，反而因为人多嘈杂有些抱怨起来。
    释空大师依然念着金刚经，但是他的脸色已经有些苍白了，气息也萎靡了下去。
    这是加持的人太多消耗了自身的力量。
    可是这次释空大师似乎已经下定了决心要将一身的修为都布施出去，即便是加持人数过多，消耗巨大，依然没有停下念经。
    “难怪释空大师说今日结束之后他就要坐化了，这样的消耗根本就不是寿元已经自己坐化的，而是活活累死的啊。”李修远心中感到有些震撼。
    他见到了一位真正的佛门高僧在用自己的性命普渡众生。
    然而众生愚钝，并不知晓大师的用意。
    可释空大师依然信念如一，没有丝毫悔意。
    一遍遍念着金刚经。
    释空大师从之前的轻松自如，到后面几乎咬着牙念经，平日里倒背如流的经文此刻就像是一座沉重的大山一样压在身上，念出一段经文就是翻过一座大山。
    最致命的是，这大山连绵起伏，无穷无尽，没有尽头，只是为了消耗自己最后一丝力气，磨灭最后一丝信念。
    “释空大师以前讲经不是这样的，这次为什么只念金刚经，不解读其他经文？”张邦昌很疑问道。
    李修远道：“释空大师这是在舍身布施，普渡众生，不是在讲经，我们保持安静，这是对大师的尊重。”
    几人点了点，没有再说话。
    “咳咳。”
    当释空大师念完这一遍经文之后突然剧烈的咳嗽起来，鲜血吐出，落地竟是鲜红的血液之中夹带着缕缕金漆。
    “师傅，您怎么样了，您没事吧。”一旁的白云大惊，忙搀扶道。
    释空大师摇了摇头，继续念诵金刚经。
    “他这辈子的修行已经布施出去七七八八了。”李修远见到那金色的血液时心中一凛。
    这是佛门的高僧修行有成的象征。
    血液呈现金色，死后金身不朽，能得罗汉果位。
    也许释空大师此生的修行无法圆满，但是就这样把几十年，甚至是近百年的修行布施出去也绝非寻常的僧人能做到的。
