    诗文的好坏本来就是因人而异的，每个人的口味喜好不同，都会有失偏驳。
    所以在科举场上，很多考生就会打听审阅试卷的主考官的喜好，好迎奉考官的喜好，从而让自己更容易考取功名。
    眼下比试诗文，没有主考官，也没有评判，诗的好坏全凭两张嘴，自然是谁也不会承认自己的诗比别人差，故此为了以示公平，就得请来天上的神明评判了。
    李修远焚香供奉在文曲星的神位前，再焚香祷告，希望这股香火能连同天地，请来天上的神明评判。
    虽说当初和天宫的神明赌约，他们撤走了扬州地界的大小鬼神，但这请神的事情不在赌约范围之内。
    三炷香静静燃烧，化作了一股香火飞出了大殿之外，然后直奔云层而去，最后消失在了九天之上。
    这香，不是寻常的香，是李修远特制的。
    本是留给小梅享用，增加她的道行，让她早日摆脱女鬼的身份，可眼下小梅留在了郭北城和瞎道人修行仙家法门去了，这香火一时也用不上。
    “他真的能请来天上的神君么？”胡黑见到李修远你这举动，心生迟疑，但也尤为在意。
    若是他能请来天生的文曲星，那么同样他也能请来天上的其他神明。
    如此一来，这个人的威胁之大，将不言而喻。
    便是区区一个凡人，也是不能小觑的存在，更别说此人有一身鬼神莫测的神异本事。
    “我儿肉身被毁的仇只怕很难报了。”胡黑心中不禁这般遗憾的想到。
    但是他也不想想，胡汉有多少狐族子孙死在了这次的劫难之中，若提起报仇的话，胡汉的仇恨足以将胡黑一族杀的干干净净才能罢手。
    “已经过去片刻了，你请的文曲星在哪呢？”这个时候李梁金有些不耐烦了起来：“若是你没有请神的本事就别在这里丢人现眼，卖弄自己的一些神神鬼鬼的手段。”
    “我的祈祷随着香火上天，天上的神明若是接收少不了一时片刻的，如果你等的不耐烦了的话我们可以开始第二局的比试，到时候等天上的星君下凡之后再行评判也不迟。”李修远平静的说道。
    请神不是那么快的，随叫随到。
    传递消息还需要一些时间呢，神明显灵之前也得有空才行。
    李梁金说道：“别说本公子不给你机会，既然你想延迟第一场的评比，那本公子就依你，不过等第二场比试之后还没有神明显灵，那你可给本公子等着，本公子可不会轻饶你。”
    “说的似乎好像你一定能赢一样，第二局比试由我定，琴棋书画，诗词歌赋，我们比棋艺。”
    “棋艺一道不需要评判就能立分高下，这是最公平的文斗了。”李修远说道。
    “好，我就就陪你下一局棋。”李梁金立刻就答应了下来，显得很是自信。
    他闲暇之余在金陵城内参加各种文会，出入各种文雅场所的时候可没少下过棋，虽然棋艺不算是精湛，比不上金陵城内很多有棋艺高手，但自认为自己的棋艺也绝对不差，至少赢过这武夫是没有什么问题的，他不相信这个武夫还有这样的闲情逸致去下去。
    而且，有一句他也很赞同，棋艺是做不了弊的，谁输谁赢一目了然，没有任何诡辩的余地。
    “取棋盘来。”李梁金对着旁边的一个狐女说道。
    那狐女应了声，便盈盈退下下去，很快便从一处偏房之内端出了一个棋盘，又布置好了坐垫在旁边，就等两人入座。
    “请吧。”李梁金衣袖一挥，走上去，直接坐了下来。
    李修远笑了笑，也不客气当即坐下。
    棋艺这玩意他以前不精通，但是现在嘛......可不一定了。
    当初他为了救自己的父亲李大富，在望川山上和千年何首乌精下了足足三天的棋，那种鬼神一般的棋局他现在已经铭记于心了，虽说比不上何首乌精那个老头，但也学到了他五六分水准，最后言语撼动了他的心神方才赢了他一子。
    而千年何首乌精的棋艺有多高？
    简直就是难以想象。
    这老头在山上等成仙，无聊之余自己跟自己下棋几十年，棋艺怕是已经是人间无敌手了。
    能有他五六分的水准，李修远觉得可以横扫人间大部分的棋手了。
    “我执黑，我先下。”李梁金淡淡的说道，先抢先手，也不会和李修远去猜单双了。
    这个时候输赢很重要，不但是脸面的问题，还干系着诸多牵连，所以只能赢不能输。
    即便是上一局自己赢了，那这一局也必须要赢才行。
    李修远笑道：“无所谓，你先下便让你先下。”
    围棋可不是谁先下就先赢的，不过李梁金抢先，估计也有自己的下棋思路。
    如此正好，今日自己便当一回何首乌精，让他见识见识一下这只千年精怪的棋艺是何等的让人窒息。
    “啪~！”不假思索，李修远落下一子。
    李梁金脸色如常，亦是回了一子。
    李修远想都没有想，便再次落子，仿佛心中已进有了一副棋局，只要按照棋局上走就行了，根本就不需要多加思考。
    “这李修远怎么下棋的，步步抢快，简直就是乱下。”
    “这厮最喜用激将法，乱人心神，这估计是他的手段之一，以快棋给李兄施加压力，让他乱了心神，棋艺一道，棋局上的比拼虽然重要，但是棋外的比拼也不能忽视，下棋的时候往往可能因为一句显化，一时乱了心神，便走错一步，结果步步皆错，满盘皆输。”
    经过了之前的诗文比试之后，这两个书生不敢小觑李修远这个武夫了。
    虽然他们眼中不屑李修远这个一身武艺的莽夫，但也从他的诗文之中感受到了他的才情不下于李梁金。
    拥有这样才情的人，其他的造诣想来也不低。
    “啪~！”
    李修远等他落子之后迅速的接了一子，彼此相差不过半个呼吸的时间。
    李梁金脸色一冷，看着李修远：“你以为下棋下的快就能赢么？”
    “下的快不一定能赢，但你这片棋子却已经死了，有时候想的多反而错的多，犹豫是没有用的。”李修远笑了笑，直接捡走了他的一小片棋子。
    李梁金看了一眼棋盘顿时心中一阵憋屈。
    这个李修远到底是从哪学的棋艺，之前下棋落子奇快看上去像是羚羊挂角，实际上等回过神来的时候自己的棋子却被绞杀了一片，如此下去的话只怕情况不妙了。
    他不知道的是当初李修远就被何首乌精这一手快棋打的晕头转向。
    还没有明白怎么回事就已经输了。
    时间渐渐过去。
    李梁金落子的速度越来越慢，可是李修远落子的速度却还是那般凌厉迅猛，他的一子犹犹豫豫的落下，李修远就紧接着落下，接着再一看，却发现自己的子又被绞杀了一片。
    “年轻人下棋要有耐心，可别着急，想好再下。”李修远轻轻一笑，看着犹豫不决，皱眉苦思的李梁金笑道。
    李梁金听到这话本来就憋屈的他更是一肚子的火，有种想要掀飞棋盘的冲动。
    他的心情，李修远能理解，因为他也体会过和何首乌精下棋的憋屈。
    无情的碾压的确是难受，尤其是你这场胜负对你非常重要的情况之下，那心中更是难受的紧。
    “还要想么？你这一子已经想了很久了，莫不是想要拖延时间。”李修远道。
    再看棋盘，李梁金的黑子却是所剩无多，但凡会下棋的人都知道这已经是无力回天了，除非李修远突然变成了一个棋艺白痴，方才有赢的可能。
    “这一局你赢了，不下了。”李梁金恼怒的伸手一挥，将手中的棋子一丢，然后站了起来：“不过你别得意，我们的胜负还没有分呢，还有第三局没有比。”
    “第三局或许不用比了，难道你没有闻到一股味道么？”李修远笑了笑。
    李梁金鼻子动了动，却是闻到了一股浓郁的香火夹带着书墨味弥漫整座大殿。
    这香味不是之前的三柱香火能达到的。
    忽的，李梁金看见在主位身神位之后不知道什么时候坐着一个儒雅的中年男子，此人抚须微笑，身披大氅，衣着和常人不一样，有点像是寺庙之**奉的神明衣服样式，在看他的模样，却让他顿时大惊失色。
    居然是上一科状元郎的样子。
    “这，这.......你到底是人还是鬼啊。”李梁金顿时惊奇起来。
    要知道上一科的状元因为在朝廷之上得罪了小人，被发配到了偏远的县城里当县令去了，结果在半路上郁郁不欢已经病故了。
    但是他听到这个消息的时候还为此叹息不已，想当初状元郎未考取状元之前还来金陵城游玩过，自己还见过他，如今才几年过去了一代状元郎居然埋骨他乡。
    李修远却是脸色如常道，施了一礼道：“这位可是天上的文曲君星？”
    他对神明尊重，但却不卑微。
