# -*- coding: utf-8 -*-
from scrapy.http import Request
import scrapy


class PtwxzBigSpider(scrapy.Spider):
    name = 'ptwxz_big'
    #allowed_domains = ['https://www.ptwxz.com/html/5/5975/3235597.html']
    start_urls = ['https://www.ptwxz.com/html/5/5975/index.html']
    # type_of_url: chapter; Add here if need more
    need = 'chapter'

    def parse(self, response):
        if(self.need == 'chapter'):
            self.number_of_chapter = 0
            for chapter_url in self.get_chapter_urls(response):
                yield Request(chapter_url, callback=self.parse_chapter)

    def parse_chapter(self, response):
        self.number_of_chapter += 1
        lines = response.xpath('//body/text()').getall()
        file_name = "data/ptwxz_chapter/chapter_{}.txt".format(self.number_of_chapter)
        f = open(file_name, 'w')
        for line in lines:
            f.write(line)
        f.close()

    def get_chapter_urls(self, response):
        chapters = response.css('a::attr(href)').re(r'\d{7,}.html')
        chapter_urls = list()

        for chapter in chapters:
            chapter_url = response.url.replace("index.html",chapter)
            chapter_urls.append(chapter_url)

        return chapter_urls
