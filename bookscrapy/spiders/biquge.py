# -*- coding: utf-8 -*-
from scrapy.http import Request
import os.path
import scrapy
import re

class BiqugeSpider(scrapy.Spider):
    name = 'biquge'
    #allowed_domains = ['https://www.biquge.com.cn/book/28628/'] --> allow all
    start_urls = [
            'https://www.biquge.com.cn/book/28628/'
        ]
    host = 'https://www.biquge.com.cn/'
    # type_of_url: chapter; Add here if need more
    need = 'chapter'
    # abbreviation of book name for denominating file name
    abbreviation = 'ltdtn'

    def parse(self, response):
        if(self.need == 'chapter'):
            self.number_of_chapter = 0
            for chapter_url in self.get_chapter_urls(response):
                yield Request(chapter_url, callback=self.parse_chapter)

    def parse_chapter(self, response):
        self.number_of_chapter += 1
        lines = response.xpath('//div[@id="content"]/text()').getall()
        data_dir = "data/{}_{}_chapters/".format(self.name, self.abbreviation)
        
        if os.path.exists(data_dir):
            file_name = data_dir + "{}_chapter_{}.txt".format(self.abbreviation, self.number_of_chapter)
        else:
            if os.path.exists('data'):
                os.mkdir(data_dir)
            else:
                os.mkdir('data')
                os.mkdir(data_dir)
        
        f = open(file_name, 'w+') 
        for line in lines:
            f.write(line + "\n")
        f.close()

    def get_chapter_urls(self, response):
        chapters = response.css('a::attr(href)').re(r'\d{7,}.html')
        chapter_urls = list()
        
        for chapter in chapters:
            chapter_url = response.url + chapter
            chapter_urls.append(chapter_url)

        return chapter_urls
