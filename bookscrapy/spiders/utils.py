def get_chapter_urls(self, response):
        chapters = response.css('a::attr(href)').re(r'\d{7,}.html')
        chapter_urls = list()

        for chapter in chapters:
            chapter_url = response.url.replace("index.html",chapter)
            chapter_urls.append(chapter_url)

        return chapter_urls